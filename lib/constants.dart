import 'package:flutter/material.dart';
import 'package:flutter_imagenetwork/flutter_imagenetwork.dart';
// import 'package:flutter_cart/flutter_cart.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:rapidos_mobile/entities/adresse.dart';

import 'entities/Panier.dart';

//couleurs utilise pour l'application
final storage = FlutterSecureStorage();
const kPrimaryColor = Color.fromRGBO(246, 52, 54, 1);
const kTeXTColor = Color(0xFF3C4046);
const kPrimaryColorblack = Color.fromRGBO(103, 202, 218, 1);
const kBackgroundColor = Color(0xFFF9F8FD);
const kHeaderColor = Color(0xFFF5F5F5);
const ksecondaryColor = Color.fromRGBO(2, 44, 67, 1);
const double radiusBorder = 10.0;
const String google_api_key = "AIzaSyD6CxcFZY50mX4TNzELyTQAQzQ_4_7LB4g";
final FlutterCart cart = new FlutterCart();
final Adresse adresse = new Adresse();
IconThemeData themeIcon = new IconThemeData(color: Colors.black);
final ButtonStyle boutonRouge = TextButton.styleFrom(
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(20),
    ),
    backgroundColor: kPrimaryColor,
    textStyle: TextStyle(color: Colors.white));
final ButtonStyle boutonTransparant = TextButton.styleFrom(
  shape: RoundedRectangleBorder(
    borderRadius: BorderRadius.circular(20),
  ),
  backgroundColor: Colors.transparent,
);
// AIzaSyAH-hy85QjX7XSmqOmXANHeaZWyNDfjGLA
// adresse ip de cornexion au serveur
const SERVER_IP =
    'http://rapidoseat-env.eba-adp3mtww.us-east-1.elasticbeanstalk.com';
// const SERVER_IP = 'http://192.168.1.191:8083';

double latActuel = 4.0580037, lonActuel = 9.7260679;

const double kDefaultPadding = 20.0;

// nom de la police
const String font = 'DayRoman';

AjanuwImageErrorBuilder immageErreurChargement =
    (BuildContext context, error, stackTrace) {
  print('[AjanuwImageErrorBuilder] ' + error.toString());
  Color color = Theme.of(context).accentColor;
  return Padding(
    padding: const EdgeInsets.all(8.0),
    child: Wrap(
      children: <Widget>[
        Image.asset(
          "assets/images/donut.gif",
          width: 50,
          height: 50,
          // color: Colors.red,
        ),
      ],
    ),
  );
};

// polise et ecriture

// nb = noir et bold(gras)
// ===> texte en gras et de couleur noir
const TextStyle text1nb = TextStyle(
  fontWeight: FontWeight.bold,
  fontFamily: font,
  fontSize: 22,
);
const TextStyle text2nb = TextStyle(
  fontWeight: FontWeight.bold,
  fontFamily: font,
  fontSize: 20,
);
const TextStyle text3nb = TextStyle(
  fontWeight: FontWeight.bold,
  fontFamily: font,
  color: Colors.black,
  fontSize: 18,
);
const TextStyle text4nb = TextStyle(
  fontWeight: FontWeight.bold,
  fontFamily: font,
  fontSize: 16,
);
const TextStyle text5nb = TextStyle(
  fontWeight: FontWeight.bold,
  fontFamily: font,
  fontSize: 14,
);
const TextStyle text6nb = TextStyle(
  fontWeight: FontWeight.bold,
  fontFamily: font,
  fontSize: 12,
);
const TextStyle text7nb = TextStyle(
  fontWeight: FontWeight.bold,
  fontFamily: font,
  fontSize: 10,
);
const TextStyle text8nb = TextStyle(
  fontWeight: FontWeight.bold,
  fontFamily: font,
  fontSize: 12,
);

// ==> Text normal de couleur noir
const TextStyle text1nn = TextStyle(
  fontWeight: FontWeight.normal,
  fontFamily: font,
  fontSize: 22,
);
const TextStyle text2nn = TextStyle(
  fontWeight: FontWeight.normal,
  fontFamily: font,
  fontSize: 20,
);
const TextStyle text3nn = TextStyle(
  fontWeight: FontWeight.normal,
  fontFamily: font,
  fontSize: 18,
);
const TextStyle text4nn = TextStyle(
  fontWeight: FontWeight.normal,
  fontFamily: font,
  fontSize: 16,
);
const TextStyle text5nn = TextStyle(
  fontWeight: FontWeight.normal,
  fontFamily: font,
  fontSize: 14,
);
const TextStyle text6nn = TextStyle(
  fontWeight: FontWeight.normal,
  fontFamily: font,
  fontSize: 12,
);
const TextStyle text7nn = TextStyle(
  fontWeight: FontWeight.normal,
  fontFamily: font,
  fontSize: 10,
);
const TextStyle text8nn = TextStyle(
  fontWeight: FontWeight.normal,
  fontFamily: font,
  fontSize: 12,
);

// rb = rouge et bold(gras)
// ===> texte en gras et de couleur noir
const TextStyle text1rb = TextStyle(
    fontWeight: FontWeight.bold,
    fontFamily: font,
    fontSize: 22,
    color: kPrimaryColor);
const TextStyle text2rb = TextStyle(
    fontWeight: FontWeight.bold,
    fontFamily: font,
    fontSize: 20,
    color: kPrimaryColor);
const TextStyle text3rb = TextStyle(
    fontWeight: FontWeight.bold,
    fontFamily: font,
    fontSize: 18,
    color: kPrimaryColor);
const TextStyle text4rb = TextStyle(
    fontWeight: FontWeight.bold,
    fontFamily: font,
    fontSize: 16,
    color: kPrimaryColor);
const TextStyle text5rb = TextStyle(
    fontWeight: FontWeight.bold,
    fontFamily: font,
    fontSize: 14,
    color: kPrimaryColor);
const TextStyle text6rb = TextStyle(
    fontWeight: FontWeight.bold,
    fontFamily: font,
    fontSize: 12,
    color: kPrimaryColor);
const TextStyle text7rb = TextStyle(
    fontWeight: FontWeight.bold,
    fontFamily: font,
    fontSize: 10,
    color: kPrimaryColor);
const TextStyle text8rb = TextStyle(
    fontWeight: FontWeight.bold,
    fontFamily: font,
    fontSize: 12,
    color: kPrimaryColor);

// ==> Text normal de couleur rouge
const TextStyle text1rn = TextStyle(
    fontWeight: FontWeight.normal,
    fontFamily: font,
    fontSize: 22,
    color: kPrimaryColor);
const TextStyle text2rn = TextStyle(
    fontWeight: FontWeight.normal,
    fontFamily: font,
    fontSize: 20,
    color: kPrimaryColor);
const TextStyle text3rn = TextStyle(
    fontWeight: FontWeight.normal,
    fontFamily: font,
    fontSize: 18,
    color: kPrimaryColor);
const TextStyle text4rn = TextStyle(
    fontWeight: FontWeight.normal,
    fontFamily: font,
    fontSize: 16,
    color: kPrimaryColor);
const TextStyle text5rn = TextStyle(
    fontWeight: FontWeight.normal,
    fontFamily: font,
    fontSize: 14,
    color: kPrimaryColor);
const TextStyle text6rn = TextStyle(
    fontWeight: FontWeight.normal,
    fontFamily: font,
    fontSize: 12,
    color: kPrimaryColor);
const TextStyle text7rn = TextStyle(
    fontWeight: FontWeight.normal,
    fontFamily: font,
    fontSize: 10,
    color: kPrimaryColor);
const TextStyle text8rn = TextStyle(
    fontWeight: FontWeight.normal,
    fontFamily: font,
    fontSize: 12,
    color: kPrimaryColor);

// bb = blanche et bold(gras)
// ===> texte en gras et de couleur noir
const TextStyle text1bb = TextStyle(
    fontWeight: FontWeight.bold,
    fontFamily: font,
    fontSize: 22,
    color: Colors.white);
const TextStyle text2bb = TextStyle(
    fontWeight: FontWeight.bold,
    fontFamily: font,
    fontSize: 20,
    color: Colors.white);
const TextStyle text3bb = TextStyle(
    fontWeight: FontWeight.bold,
    fontFamily: font,
    fontSize: 18,
    color: Colors.white);
const TextStyle text4bb = TextStyle(
    fontWeight: FontWeight.bold,
    fontFamily: font,
    fontSize: 16,
    color: Colors.white);
const TextStyle text5bb = TextStyle(
    fontWeight: FontWeight.bold,
    fontFamily: font,
    fontSize: 14,
    color: Colors.white);
const TextStyle text6bb = TextStyle(
    fontWeight: FontWeight.bold,
    fontFamily: font,
    fontSize: 12,
    color: Colors.white);
const TextStyle text7bb = TextStyle(
    fontWeight: FontWeight.bold,
    fontFamily: font,
    fontSize: 10,
    color: Colors.white);
const TextStyle text8bb = TextStyle(
    fontWeight: FontWeight.bold,
    fontFamily: font,
    fontSize: 12,
    color: Colors.white);

// ==> Text normal de couleur rouge
const TextStyle text1bn = TextStyle(
    fontWeight: FontWeight.normal,
    fontFamily: font,
    fontSize: 22,
    color: Colors.white);
const TextStyle text2bn = TextStyle(
    fontWeight: FontWeight.normal,
    fontFamily: font,
    fontSize: 20,
    color: Colors.white);
const TextStyle text3bn = TextStyle(
    fontWeight: FontWeight.normal,
    fontFamily: font,
    fontSize: 18,
    color: Colors.white);
const TextStyle text4bn = TextStyle(
    fontWeight: FontWeight.normal,
    fontFamily: font,
    fontSize: 16,
    color: Colors.white);
const TextStyle text5bn = TextStyle(
    fontWeight: FontWeight.normal,
    fontFamily: font,
    fontSize: 14,
    color: Colors.white);
const TextStyle text6bn = TextStyle(
    fontWeight: FontWeight.normal,
    fontFamily: font,
    fontSize: 12,
    color: Colors.white);
const TextStyle text7bn = TextStyle(
    fontWeight: FontWeight.normal,
    fontFamily: font,
    fontSize: 10,
    color: Colors.white);
const TextStyle text8bn = TextStyle(
    fontWeight: FontWeight.normal,
    fontFamily: font,
    fontSize: 12,
    color: Colors.white);
