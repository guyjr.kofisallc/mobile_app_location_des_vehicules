import 'dart:convert';

import 'package:dio/dio.dart';
import '../constants.dart';
import '../entities/login.dart';

class LoginService {
  String _apikey = '$SERVER_IP/rapidoseat/auth/signin';
  final Dio _dio = new Dio();

  Future<dynamic> infoClient(String username, String password) async {
    var params = {
      "username": "$username",
      "password": "$password",
    };
    print("connexion $_apikey");
    try {
      Response loginData = await _dio.post(
        _apikey,
        // options: Options(headers: {
        //   HttpHeaders.contentTypeHeader: "application/json",
        // }),
        data: jsonEncode(params),
      );

      return Login.fromJson(loginData.data);

      // return CurentUserDataData.data.runtimeType(CurentUserData);
      // return List<Categorie>.from(((categorieData.data) as List).map((json) {
      //   return Categorie.fromJson(json);
      // }));
    } on DioError catch (e) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx and is also not 304.
      if (e.response != null) {
        print('Dio error!');
        print('STATUS: ${e.response?.statusCode}');
        print('DATA: ${e.response?.data}');
        print('HEADERS: ${e.response?.headers}');
        return e.response?.statusCode;
      } else {
        // Error due to setting up or sending the request
        print("\n \n \n  ");
        print('Erreurr sending request!');
        print("\n \n \n  ");
        print(e.message);
        return "Connexion Internet";
      }
    }
  }
}
