import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:rapidos_mobile/entities/messageData.dart';
import '../constants.dart';

class MessageServices {
  String _apikey = '$SERVER_IP/rapidoseat/message/all';
  String _apikey2 = '$SERVER_IP/rapidoseat/message/add';
  final Dio _dio = new Dio();

  Future<dynamic> AfficheMessage() async {
    var jwt = await storage.read(key: "jwt");
    _dio.options.contentType = Headers.formUrlEncodedContentType;
    _dio.options.headers.addAll({"authorization": "Bearer $jwt"});

    try {
      Response messageData = await _dio.get(
        _apikey,
      );
      return List<MessageData>.from(((messageData.data) as List).map((json) {
        return MessageData.fromJson(json);
      }));
    } on DioError catch (e) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx and is also not 304.
      if (e.response != null) {
        print('Dio error!');
        print('STATUS: ${e.response?.statusCode}');
        print('DATA: ${e.response?.data}');
        print('HEADERS: ${e.response?.headers}');
        return e.response?.statusCode.toString();
      } else {
        // Error due to setting up or sending the request
        print("\n \n \n  ");
        print('Erreurr sending request!');
        print("\n \n \n  ");
        print(e.message);
        return "600";
      }
    }
  }
}
