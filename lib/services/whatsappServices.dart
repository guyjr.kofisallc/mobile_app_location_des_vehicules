import 'dart:convert';

import 'package:dio/dio.dart';
import '../constants.dart';
import '../entities/login.dart';

class WhatsappService {
  String _apikey = '$SERVER_IP/rapidoseat/whasapp/all';
  final Dio _dio = new Dio();

  Future<dynamic> message(String message) async {
    var params = {"numero": "680401646", "texte": "$message"};
    try {
      print("Envoie du message zapppppppppppp");
      Response loginData = await _dio.post(
        _apikey,
        // options: Options(headers: {
        //   HttpHeaders.contentTypeHeader: "application/json",
        // }),
        data: jsonEncode(params),
      );

      return "ok";
    } on DioError catch (e) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx and is also not 304.
      if (e.response != null) {
        print('Dio error!');
        print('STATUS: ${e.response?.statusCode}');
        print('DATA: ${e.response?.data}');
        print('HEADERS: ${e.response?.headers}');
        return e.response?.statusCode;
      } else {
        // Error due to setting up or sending the request
        print("\n \n \n  ");
        print('Erreurr sending request!');
        print("\n \n \n  ");
        print(e.message);
        return "Connexion Internet";
      }
    }
  }
}
