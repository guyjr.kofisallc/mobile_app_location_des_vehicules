import 'dart:convert';
import 'package:dio/dio.dart';
import '../constants.dart';

class MailService {
  String _apikey = '$SERVER_IP/rapidoseat/email/sendMail';
  final Dio _dio = new Dio();

  Future<dynamic> messageFogetPassword(String mail, String password) async {
    var params = {
      "recipient": "$mail",
      "msgBody": "$password",
      "subject": "Code de verification RapidosEat"
    };
    try {
      print("Envoie du message zapppppppppppp");
      Response loginData = await _dio.post(
        _apikey,
        // options: Options(headers: {
        //   HttpHeaders.contentTypeHeader: "application/json",
        // }),
        data: jsonEncode(params),
      );

      return "200";
    } on DioError catch (e) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx and is also not 304.
      if (e.response != null) {
        print('Dio error!');
        print('STATUS: ${e.response?.statusCode}');
        print('DATA: ${e.response?.data}');
        print('HEADERS: ${e.response?.headers}');
        return e.response?.statusCode;
      } else {
        // Error due to setting up or sending the request
        print("\n \n \n  ");
        print('Erreurr sending request!');
        print("\n \n \n  ");
        print(e.message);
        return "600";
      }
    }
  }
}
