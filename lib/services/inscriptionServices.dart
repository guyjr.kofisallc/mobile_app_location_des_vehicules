import 'dart:convert';

import 'package:dio/dio.dart';
import '../constants.dart';

class InscriptiomServices {
  String _apikey = '$SERVER_IP/rapidoseat/auth/signup';
  final Dio _dio = new Dio();

  Future<dynamic> inscription(
      String name,
      String username,
      String email,
      String phone,
      double latitude,
      double longitude,
      String password,
      String adresse) async {
    var params = {
      "name": "$name",
      "username": "$username",
      "email": "$email",
      "phone": "$phone",
      "position": {"latitude": latitude, "longitude": longitude},
      "password": "$password",
      "address": "$adresse"
    };
    print(params.toString());
    try {
      Response loginData = await _dio.post(
        _apikey,
        // options: Options(headers: {
        //   HttpHeaders.contentTypeHeader: "application/json",
        // }),
        data: jsonEncode(params),
      );

      return loginData.statusCode;

      // return CurentUserDataData.data.runtimeType(CurentUserData);
      // return List<Categorie>.from(((categorieData.data) as List).map((json) {
      //   return Categorie.fromJson(json);
      // }));
    } on DioError catch (e) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx and is also not 304.
      if (e.response != null) {
        print('Dio error!');
        print('STATUS: ${e.response?.statusCode}');
        print('DATA: ${e.response?.data}');
        print('HEADERS: ${e.response?.headers}');
        return e.response?.statusCode;
      } else {
        // Error due to setting up or sending the request
        print("\n \n \n  ");
        print('Erreurr sending request!');
        print("\n \n \n  ");
        print(e.message);
        return "Connexion Internet";
      }
    }
  }
}
