import 'package:dio/dio.dart';
import 'package:rapidos_mobile/entities/LigneCommande.dart';
import '../constants.dart';
import '../entities/Panier.dart';

class EnvoiCartitems {
  EnvoiCartitems();
  String _apikey = '$SERVER_IP/rapidoseat/cartitem/add';
  final Dio _dio = new Dio();

  Future<dynamic> sendcartItems(FlutterCart cart, int idCommande) async {
    var jwt = await storage.read(key: "jwt");
    _dio.options.headers.addAll({"authorization": "Bearer $jwt"});
    print("//////////////////////");
    print("Envoie des element de la commande");
    print("//////////////////////");

    try {
      print("le numero de la commande est ------------> ici est" +
          idCommande.toString());

      List<LigneCommande> listCommande = [];
      for (var i = 0; i < cart.cartItem.length; i++) {
        var productName2 = cart.cartItem[i].productName;
        Cart cart1 = new Cart();
        cart1.cartId = idCommande;

        ProduitCommande produitCommande =
            new ProduitCommande(produitId: cart.cartItem[i].productId);
        LigneCommande ligneCommande = new LigneCommande(
            cart: cart1,
            price: cart.cartItem[i].subTotal.toInt(),
            quantite: cart.cartItem[i].quantity,
            produit: produitCommande);
        print("voilaaa ---->" + ligneCommande.quantite.toString());
        Response cartItem =
            await _dio.post(_apikey, data: ligneCommande.toJson());
        print("+voila la reponse" + cartItem.data.toString());
      }
      print("//////////////////////");
      print("Fin envoie element de la commande");
      print("//////////////////////");
      return "envoyee";
    } on DioError catch (e) {
      // The request was made and the server responded with a status code0
      // that falls out of the range of 2xx and is also not 304.
      if (e.response != null) {
        print('Dio error!');
        print('STATUS: ${e.response?.statusCode}');
        print('DATA: ${e.response?.data}');
        print('HEADERS: ${e.response?.headers}');
      } else {
        // Error due to setting up or sending the request
        print("\n \n \n  ");
        print('Erreurr sending request!');
        print("\n \n \n  ");
        print(e.message);
      }
    }
  }
}
