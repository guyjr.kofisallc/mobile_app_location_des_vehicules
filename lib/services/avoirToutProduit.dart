import 'package:dio/dio.dart';
import 'package:rapidos_mobile/entities/allProduit.dart';
import '../constants.dart';

class AfficheProduit {
  AfficheProduit(this.restoId);
  String restoId;
  String _apikey1 = '$SERVER_IP/rapidoseat/produit/alls?id=';
  final Dio _dio = new Dio();

  Future<dynamic> AfficheToutProduit() async {
    var jwt = await storage.read(key: "jwt"); //aggecte le token a la variable
    _dio.options.contentType = Headers.formUrlEncodedContentType;
    _dio.options.headers.addAll({"authorization": "Bearer $jwt"});

    try {
      Response produitData = await _dio.get(
        "$_apikey1" + "$restoId",
      );
      return List<Produit>.from(((produitData.data) as List).map((json) {
        return Produit.fromJson(json);
      }));
    } on DioError catch (e) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx and is also not 304.
      if (e.response != null) {
        print('Dio error!');
        print('STATUS: ${e.response?.statusCode}');
        print('DATA: ${e.response?.data}');
        print('HEADERS: ${e.response?.headers}');
        return e.response?.statusCode;
      } else {
        // Error due to setting up or sending the request
        print("\n \n \n  ");
        print('Erreurr sending request!');
        print("\n \n \n  ");
        print(e.message);
        return "600";
      }
    }
  }
}
