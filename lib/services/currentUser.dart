import 'package:dio/dio.dart';
import 'package:rapidos_mobile/entities/currentUserData.dart';
import 'package:rapidos_mobile/entities/restaurants.dart';
import '../constants.dart';
import '../entities/userData.dart';

class CurrentUser {
  String _apikey = '$SERVER_IP/rapidoseat/user/currentUser';
  String _apikey2 = '$SERVER_IP/rapidoseat/user/searchUserById?id=';
  final Dio _dio = new Dio();

  Future<dynamic> infoClient() async {
    var jwt = await storage.read(key: "jwt");

    _dio.options.contentType = Headers.formUrlEncodedContentType;
    _dio.options.headers.addAll({"authorization": "Bearer $jwt"});

    try {
      Response curentUserDataData = await _dio.get(
        _apikey,
      );
      return CurentUserData.fromJson(curentUserDataData.data);
    } on DioError catch (e) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx and is also not 304.
      if (e.response != null) {
        print('Dio error!');
        print('STATUS: ${e.response?.statusCode}');
        print('DATA: ${e.response?.data}');
        print('HEADERS: ${e.response?.headers}');
      } else {
        // Error due to setting up or sending the request
        print("\n \n \n  ");
        print('Erreurr sending request!');
        print("\n \n \n  ");
        print(e.message);
      }
    }
  }

  Future<dynamic> findRestoByid(String id) async {
    var jwt = await storage.read(key: "jwt");

    _dio.options.contentType = Headers.formUrlEncodedContentType;
    _dio.options.headers.addAll({"authorization": "Bearer $jwt"});

    try {
      Response curentUserDataData = await _dio.get(
        _apikey2 + id,
      );
      return Restaurant.fromJson(curentUserDataData.data);
    } on DioError catch (e) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx and is also not 304.
      if (e.response != null) {
        print('Dio error!');
        print('STATUS: ${e.response?.statusCode}');
        print('DATA: ${e.response?.data}');
        print('HEADERS: ${e.response?.headers}');
        return e.response?.statusCode.toString();
      } else {
        // Error due to setting up or sending the request
        print("\n \n \n  ");
        print('Erreurr sending request!');
        print("\n \n \n  ");
        print(e.message);
        return "600";
      }
    }
  }

  Future<dynamic> findUserByid(String id) async {
    var jwt = await storage.read(key: "jwt");

    _dio.options.contentType = Headers.formUrlEncodedContentType;
    _dio.options.headers.addAll({"authorization": "Bearer $jwt"});

    try {
      Response curentUserDataData = await _dio.get(
        _apikey2 + id,
      );
      return UserData.fromJson(curentUserDataData.data);
    } on DioError catch (e) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx and is also not 304.
      if (e.response != null) {
        print('Dio error!');
        print('STATUS: ${e.response?.statusCode}');
        print('DATA: ${e.response?.data}');
        print('HEADERS: ${e.response?.headers}');
        return e.response?.statusCode.toString();
      } else {
        // Error due to setting up or sending the request
        print("\n \n \n  ");
        print('Erreurr sending request!');
        print("\n \n \n  ");
        print(e.message);
        return "600";
      }
    }
  }
}
