import 'package:dio/dio.dart';
import 'package:rapidos_mobile/entities/listeCommandeModel.dart';
import '../constants.dart';

class ListCommandeServices {
  String _apikey1 = '$SERVER_IP/rapidoseat/cart/voirbyiduser?idUser=';
  final Dio _dio = new Dio();

  Future<dynamic> afficheListCommande() async {
    var jwt = await storage.read(key: "jwt");
    var idUser = await storage.read(key: "idUser");

    ///aggecte le token a la variable
    _dio.options.contentType = Headers.formUrlEncodedContentType;
    _dio.options.headers.addAll({"authorization": "Bearer $jwt"});

    try {
      Response produitData = await _dio.get(
        "$_apikey1" + "$idUser",
      );
      return List<ListCommandeModel>.from(
          ((produitData.data) as List).map((json) {
        return ListCommandeModel.fromJson(json);
      }));
    } on DioError catch (e) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx and is also not 304.
      if (e.response != null) {
        print('Dio error!');
        print('STATUS: ${e.response?.statusCode}');
        print('DATA: ${e.response?.data}');
        print('HEADERS: ${e.response?.headers}');
        return e.response?.statusCode;
      } else {
        // Error due to setting up or sending the request
        print("\n \n \n  ");
        print('Erreurr sending request!');
        print("\n \n \n  ");
        print(e.message);
        return "600";
      }
    }
  }
}
