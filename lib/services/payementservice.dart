import 'dart:convert';

import 'package:dio/dio.dart';
import '../constants.dart';
import '../entities/Payement.dart';
import '../entities/tokenPayement.dart';
import '../entities/verifierPayement.dart';

class PayementService {
  PayementService();
  String _apikey = 'https://campay.net/api/token/';
  String _apikey2 = 'https://campay.net/api/collect/';
  String _apikey3 = 'https://campay.net/api/transaction/';
  final Dio _dio = new Dio();

  Future<dynamic> sendInfo() async {
    var params = {
      "username":
          "G9UkMdZFVo3fKvf3ED5e6jubacW5ZIWpcYA_Mu5uSZ8yFPXuK4JT6ffzrR7h7Emh6-_9P1Bsw8dGCuhwzzIWFw",
      "password":
          "8kBtJfr7ZC1GTfT_F28TGPB-jmfwP-mynqwBB6yevUF9CCv8k-n7wEmhUxoaJY0gztyqQUdUrCid3oBNUv6lLw"
    };
    var jwt = await storage.read(key: "jwt");
    print("//////////////////////");
    print("demande de token");
    print("//////////////////////");

    try {
      Response tokenResponse =
          await _dio.post(_apikey, data: jsonEncode(params));
      print(tokenResponse.statusCode.toString());

      return TokenPayement.fromJson(tokenResponse.data);
    } on DioError catch (e) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx and is also not 304.
      if (e.response != null) {
        print('Dio error!');
        print('STATUS: ${e.response?.statusCode}');
        print('DATA: ${e.response?.data}');
        print('HEADERS: ${e.response?.headers}');
        return e.response.statusCode;
      } else {
        // Error due to setting up or sending the request
        print("\n \n \n ");
        print('Erreurr sending request!');
        print("\n \n \n  ");
        print(e.message);
        return "Probleme de connexion";
      }
    }
  }

  Future<dynamic> sendPayement(
      String total, String numero, String cle, String description) async {
    var params = {
      "amount": "$total",
      "currency": "XAF",
      "from": "$numero",
      "description": "$description",
      "external_reference": "",
      "external_user": ""
    };
    print('Envoie du paiement' + numero);

    try {
      print("//////////////////////");
      print("on fait un payement");
      print(cle);
      print("//////////////////////");
      Response tokenResponse = await _dio.post(_apikey2,
          data: jsonEncode(params),
          options: Options(headers: {"Authorization": "$cle"}));
      print(tokenResponse.statusCode.toString());
      return PayementEntities.fromJson(tokenResponse.data);
      // var tokenResponse = {
      //   "reference": "jfgdkjf",
      //   "ussdCode": "kjnf",
      //   "operator": "kgkgfg"
      // };

      // return PayementEntities.fromJson(tokenResponse);
      // print("on paye");
      // return null;
    } on DioError catch (e) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx and is also not 304.
      if (e.response != null) {
        print('Dio error!');
        print('STATUS: ${e.response?.statusCode}');
        print('DATA: ${e.response?.data}');
        print('HEADERS: ${e.response?.headers}');
        return e.response.statusCode;
      } else {
        // Error due to setting up or sending the request
        print("\n \n \n ");
        print('Erreurr sending request!');
        print("\n \n \n  ");
        print(e.message);
        return "Probleme de connexion";
      }
    }
  }

  Future<dynamic> verifierPayement() async {
    var cle = await storage.read(key: "cle");
    var reference = await storage.read(key: "reference");
    try {
      print("//////////////////////");
      print("lien     " + _apikey3 + "$reference");
      print(cle);
      print("//////////////////////");
      Response tokenResponse = await _dio.get(_apikey3 + "$reference",
          options: Options(headers: {"Authorization": "$cle"}));
      print(tokenResponse.statusCode.toString());

      return VerifierPayement.fromJson(tokenResponse.data);
    } on DioError catch (e) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx and is also not 304.
      if (e.response != null) {
        print('Dio error!');
        print('STATUS: ${e.response?.statusCode}');
        print('DATA: ${e.response?.data}');
        print('HEADERS: ${e.response?.headers}');
        return e.response.statusCode;
      } else {
        // Error due to setting up or sending the request
        print("\n \n \n ");
        print('Erreurr sending request!');
        print("\n \n \n  ");
        print(e.message);
        return "Probleme de connexion";
      }
    }
  }
}
