import 'package:dio/dio.dart';
import '../constants.dart';

class PasswordVerify {
  PasswordVerify();
  String motcle;
  String _apikey = '$SERVER_IP/rapidoseat/user/userexist?emailorusername=';
  String _apikey2 = '$SERVER_IP/rapidoseat/user/modifpassword?';
  final Dio _dio = new Dio();

  Future<dynamic> verifie(String usermaneOrEmail) async {
    var jwt = await storage.read(key: "jwt");
    _dio.options.contentType = Headers.formUrlEncodedContentType;
    _dio.options.headers.addAll({"authorization": "Bearer $jwt"});

    try {
      print(_apikey + usermaneOrEmail);
      Response responseData = await _dio.get(
        _apikey + usermaneOrEmail,
      );
      print(responseData.data);
      return responseData.data;
    } on DioError catch (e) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx and is also not 304.
      if (e.response != null) {
        print('Dio error!');
        print('STATUS: ${e.response?.statusCode}');
        print('DATA: ${e.response?.data}');
        print('HEADERS: ${e.response?.headers}');
        return e.response?.statusCode;
      } else {
        // Error due to setting up or sending the request
        print("\n \n \n  ");
        print('Erreurr sending request!');
        print("\n \n \n  ");
        print(e.message);
        return 600;
      }
    }
  }

  Future<dynamic> modifyPassword(String email, String password) async {
    var jwt = await storage.read(key: "jwt");
    _dio.options.contentType = Headers.formUrlEncodedContentType;
    _dio.options.headers.addAll({"authorization": "Bearer $jwt"});
    motcle = "email=" + email + "&newpassword=" + password;
    try {
      print(_apikey2 + motcle);
      Response responseData = await _dio.put(
        _apikey2 + motcle,
      );
      print(responseData.data);
      return responseData.statusCode;
    } on DioError catch (e) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx and is also not 304.
      if (e.response != null) {
        print('Dio error!');
        print('STATUS: ${e.response?.statusCode}');
        print('DATA: ${e.response?.data}');
        print('HEADERS: ${e.response?.headers}');
        return e.response?.statusCode;
      } else {
        // Error due to setting up or sending the request
        print("\n \n \n  ");
        print('Erreurr sending request!');
        print("\n \n \n  ");
        print(e.message);
        return 600;
      }
    }
  }
}
