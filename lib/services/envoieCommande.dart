import 'dart:convert';

import 'package:dio/dio.dart';
import '../constants.dart';
import '../entities/CommandeSend.dart';

class EnvoiCommande {
  EnvoiCommande();
  CommandeSend commande;
  String _apikey = '$SERVER_IP/rapidoseat/cart/command';
  String _apikey2 = '$SERVER_IP/rapidoseat/cart/modifcommande/annuler';

  final Dio _dio = new Dio();

  Future<dynamic> sendCommande(var commande) async {
    var jwt = await storage.read(key: "jwt");
    _dio.options.headers.addAll({"authorization": "Bearer $jwt"});

    try {
      Response commandeData = await _dio.post(_apikey, data: commande);
      print(commandeData.data);

      return commandeData.data;
    } on DioError catch (e) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx and is also not 304.
      if (e.response != null) {
        print('Dio error!');
        print('STATUS: ${e.response?.statusCode}');
        print('DATA: ${e.response?.data}');
        print('HEADERS: ${e.response?.headers}');
      } else {
        // Error due to setting up or sending the request
        print("\n \n \n  ");
        print('Erreurr sending request!');
        print("\n \n \n  ");
        print(e.message);
      }
    }
  }

  Future<dynamic> annulerCommande(String idResto) async {
    var jwt = await storage.read(key: "jwt");
    _dio.options.headers.addAll({"authorization": "Bearer $jwt"});
    print("//////////////////////");
    print("On annule la commande");
    print("//////////////////////");

    var param = {"cartId": commande.cartId};

    try {
      Response commandeData = await _dio
          .put(_apikey2 + "?idCommande=" + idResto, data: jsonEncode(param));
      print(commandeData.data);

      return commandeData.statusCode;
    } on DioError catch (e) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx and is also not 304.
      if (e.response != null) {
        print('Dio error!');
        print('STATUS: ${e.response?.statusCode}');
        print('DATA: ${e.response?.data}');
        print('HEADERS: ${e.response?.headers}');
        return e.response?.statusCode;
      } else {
        // Error due to setting up or sending the request
        print("\n \n \n  ");
        print('Erreurr sending request!');
        print("\n \n \n  ");
        print(e.message);
        return "600";
      }
    }
  }
}
