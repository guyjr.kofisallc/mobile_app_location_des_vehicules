import 'dart:convert';

import 'package:dio/dio.dart';
import '../constants.dart';

class MessageServices2 {
  String _apikey = '$SERVER_IP/rapidoseat/message/add';
  final Dio _dio = new Dio();

  Future<dynamic> envoyerMessage(String message) async {
    var jwt = await storage.read(key: "jwt");
    _dio.options.headers.addAll({"authorization": "Bearer $jwt"});
    var params = {"message": "$message"};
    print(_apikey);
    print(jsonEncode(params));

    try {
      Response messageData = await _dio.post(_apikey, data: jsonEncode(params));
      print(messageData.statusCode.toString());
      return messageData.statusCode.toString();
    } on DioError catch (e) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx and is also not 304.
      if (e.response != null) {
        print('Dio error!');
        print('STATUS: ${e.response?.statusCode}');
        print('DATA: ${e.response?.data}');
        print('HEADERS: ${e.response?.headers}');
        return e.response?.statusCode.toString();
      } else {
        // Error due to setting up or sending the request
        print("\n \n \n  ");
        print('Erreurr sending request!');
        print("\n \n \n  ");
        print(e.message);
        return "600";
      }
    }
  }
}
