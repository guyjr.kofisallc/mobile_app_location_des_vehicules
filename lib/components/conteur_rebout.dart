import 'package:flutter/material.dart';

import '../constants.dart';

class CompteRebour extends StatefulWidget {
  const CompteRebour();

  @override
  State<CompteRebour> createState() => _MyStatefulWidgetState();
}

/// This is the private State class that goes with MyStatefulWidget.
/// AnimationControllers can be created with `vsync: this` because of TickerProviderStateMixin.
class _MyStatefulWidgetState extends State<CompteRebour>
    with TickerProviderStateMixin {
  AnimationController controller;

  @override
  void initState() {
    controller = AnimationController(
      vsync: this,
      duration: const Duration(seconds: 2400),
    )..addListener(() {
        setState(() {});
      });
    controller.repeat(reverse: true);
    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: kDefaultPadding, right: kDefaultPadding),
      child: LinearProgressIndicator(
        backgroundColor: Colors.grey.shade200,
        value: controller.value,
        semanticsLabel: 'Linear progress indicator',
        minHeight: 10,
      ),
    );
  }
}
