import 'package:flutter/material.dart';
import 'package:rapidos_mobile/entities/listeCommandeModel.dart';

import 'body_detail_list_commande.dart';

class DetailListCommande extends StatelessWidget {
  DetailListCommande(this.listCommandeModel);
  ListCommandeModel listCommandeModel;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(context),
      body: BodyDetailListCommande(listCommandeModel),
      //bottomNavigationBar: Footer_all(),
    );
  }

  AppBar buildAppBar(BuildContext context) {
    return AppBar(
      centerTitle: true,
      iconTheme: IconThemeData(color: Colors.black),
      title: Text(
        "Commande",
        style: TextStyle(
            color: Colors.black, fontWeight: FontWeight.bold, fontSize: 18),
      ),
      // leading: IconButton(
      //     color: kPrimaryColor,
      //     onPressed: () {
      //       Navigator.of(context)
      //           .push(new MaterialPageRoute(builder: (context) => Acceuil()));
      //     },
      //     icon: Icon(
      //       Icons.arrow_back_ios_sharp,
      //       size: 30,
      //     )),
    );
  }
}
