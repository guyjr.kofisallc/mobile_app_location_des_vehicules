import 'package:flutter/material.dart';
import 'package:rapidos_mobile/components/connexion.dart';

import 'bodyRestaurePassword.dart';

class RestaurePassword extends StatelessWidget {
  const RestaurePassword({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return Navigator.of(context)
            .push(new MaterialPageRoute(builder: (context) => Connexion()));
      },
      child: Scaffold(
        appBar: buildAppBar(context),
        body: BodyRestaurePassword(),
      ),
    );
  }

  AppBar buildAppBar(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.grey.shade50,
      centerTitle: true,
      // title: Text(
      //   "Connexion",
      //   style: TextStyle(
      //     color: Colors.black,
      //     fontWeight: FontWeight.bold,
      //   ),
      // ),
      shadowColor: Colors.transparent,
      iconTheme: IconThemeData(color: Colors.black),
      // leading: Container(
      //   width: 20,
      //   height: 35,
      //   decoration: BoxDecoration(
      //     color: kPrimaryColorgrey,
      //     borderRadius: BorderRadius.circular(60.0),
      //   ),
      //   child: IconButton(
      //       color: Colors.black,
      //       onPressed: () {},
      //       icon: Icon(
      //         Icons.arrow_back_ios_sharp,
      //         size: 30,
      //       )),
      // ),
    );
  }
}
