import 'package:flutter/material.dart';
import 'package:rapidos_mobile/components/body_checkout_page.dart';
import '../constants.dart';
import 'drawer_client.dart';

class CheckoutPage extends StatelessWidget {
  CheckoutPage();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: (ClientDrawer()),
      appBar: buildAppBar(context),
      body: BodyCheckout(),
      // bottomNavigationBar: Footer_all(),
    );
  }

  AppBar buildAppBar(BuildContext context) {
    return AppBar(
      centerTitle: true,
      title: Text(
        "Resumé",
        style: text3nb,
      ),
      // leading: IconButton(
      //     color: Colors.red,
      //     onPressed: () {
      //       Navigator.of(context).push(new MaterialPageRoute(
      //           builder: (context) => AdresseLivraison()));
      //     },
      //     icon: Icon(
      //       Icons.arrow_back_ios_sharp,
      //       size: 30,
      //     )),
    );
  }
}
