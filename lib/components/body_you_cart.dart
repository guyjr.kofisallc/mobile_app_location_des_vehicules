import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:rapidos_mobile/components/adresse_livraison.dart';
import 'package:rapidos_mobile/components/home_page.dart';
import 'package:rapidos_mobile/constants.dart';
import 'package:rapidos_mobile/entities/restaurants.dart';

import '../entities/cartElement.dart';

class BodyCart extends StatefulWidget {
  BodyCart(this.restaurant);
  Restaurant restaurant;
  @override
  _BodyCartState createState() => _BodyCartState();
}

class _BodyCartState extends State<BodyCart> {
  _BodyCartState(
      // this.cart
      );
  // FlutterCart cart;
  int total;
  double PrixUnite = 550;
  int quantite = 1, index;
  double totalQte = 8500.0 * 5;

  void augmenteQte(int index) {
    setState(() {
      cart.cartItem[index].quantity = cart.cartItem[index].quantity + 1;
      cart.cartItem[index].subTotal =
          cart.cartItem[index].unitPrice * cart.cartItem[index].quantity;
    });
  }

  void dismissible(int index) {
    setState(() {
      cart.deleteItemFromCart(index);
    });
  }

  void diminueQte(int index) {
    if (quantite <= 2) {
      quantite = 2;
      cart.cartItem[index].quantity = 2;
    }
    setState(() {
      cart.cartItem[index].quantity = cart.cartItem[index].quantity - 1;
      cart.cartItem[index].subTotal =
          cart.cartItem[index].unitPrice * cart.cartItem[index].quantity;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    if (cart.cartItem.isEmpty) {
      return Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              // color: Colors.amber,
              padding: EdgeInsets.all(8),
              child: Image.asset(
                'assets/images/panierVide.png',
                width: size.width * 0.8,
                height: size.width * 0.8,
                fit: BoxFit.cover,
              ),
            ),
            Text(
              "Votre Panier est vide !!!",
              style: text2nn,
              textAlign: TextAlign.center,
            ),
          ],
        ),
      );
    }

    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          Padding(padding: const EdgeInsets.only(top: 15)),
          Container(
            height: size.height * 0.6,
            child: ListView.builder(
                itemCount: cart.cartItem.length,
                itemBuilder: (context, index) {
                  totalQte = cart.cartItem[index].unitPrice;
                  print(index);
                  return Dismissible(
                    onDismissed: (direction) {
                      // cart.decrementItemFromCart(index),
                      setState(() {
                        cart.cartItem.removeAt(index);
                      });
                      // dismissible(index)
                    },
                    key: ValueKey<CartItem>(cart.cartItem[index]),
                    // key: Key("2"),
                    background: Container(
                      padding: EdgeInsets.symmetric(horizontal: 15),
                      decoration: BoxDecoration(
                        color: Colors.amber,
                        borderRadius: BorderRadius.circular(15),
                      ),
                      child: Row(
                        children: [Spacer(), Icon(Icons.delete)],
                      ),
                    ),
                    child: Container(
                      color: Colors.grey.shade200,
                      padding: EdgeInsets.only(bottom: kDefaultPadding - 10),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          ListTile(
                            contentPadding: EdgeInsets.all(kDefaultPadding),
                            leading: Container(
                              width: size.width * 0.25,
                              child: ClipRRect(
                                  //un peu comme un conteneur mais avec border radius
                                  borderRadius: BorderRadius.circular(9.0),
                                  child: Image.network(
                                    // "https://pbs.twimg.com/media/DNjWneUX4AIXUMW.jpg",
                                    cart.cartItem[index].productDetails,
                                    fit: BoxFit.cover,
                                  )),
                            ),
                            title: Text(
                              cart.cartItem[index].productName.toString(),
                              style: text5nb,
                            ),
                            minVerticalPadding: 0,
                            subtitle: Row(
                              children: <Widget>[
                                Padding(
                                    padding: const EdgeInsets.only(top: 25)),
                                IconButton(
                                  onPressed: () {
                                    diminueQte(index);
                                  },
                                  icon: Icon(
                                    Icons.remove_circle_outline,
                                    size: 20,
                                    color: Colors.grey.shade600,
                                  ),
                                ),
                                Text(
                                  cart.cartItem[index].quantity.toString(),
                                  style: TextStyle(
                                      color: kPrimaryColor, fontSize: 20),
                                ),
                                IconButton(
                                  onPressed: () {
                                    augmenteQte(index);
                                  },
                                  icon: Icon(
                                    Icons.add_circle_outline_rounded,
                                    size: 20,
                                    color: Colors.grey.shade600,
                                  ),
                                )
                              ],
                            ),
                            trailing: Text(
                              cart.cartItem[index].subTotal.hashCode
                                      .toString() +
                                  " XAF",
                              style: text5rb,
                            ),
                          ),
                        ],
                      ),
                    ),
                  );
                }),
          ),
          Padding(padding: const EdgeInsets.only(top: 15)),
          Container(
            decoration: BoxDecoration(
                border: Border(
              top: BorderSide(
                color: Colors.black,
              ),
            )),
            padding: const EdgeInsets.only(top: 15),
            child: Row(
              children: <Widget>[
                Container(
                  padding: const EdgeInsets.only(left: kDefaultPadding),
                  width: size.width / 2,
                  child: Text(
                    "TOTAL : ",
                    style: text1nb,
                  ),
                ),
                //Padding(padding: const EdgeInsets.only(left: kDefaultPadding)),
                Container(
                    width: size.width / 2,
                    padding: const EdgeInsets.only(right: kDefaultPadding),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      verticalDirection: VerticalDirection.down,
                      children: [
                        Text(
                          cart.getTotalAmount().toString() + " FCFA",
                          style: text1rb,
                        ),
                      ],
                    ))
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 55),
          ),
          Row(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: kDefaultPadding),
              ),
              SizedBox(
                width: size.width * 0.45,
                height: 50,
                child: TextButton(
                  style: boutonRouge,
                  onPressed: () {
                    Navigator.of(context).push(new MaterialPageRoute(
                        builder: (context) => AdresseLivraison(
                            // cart
                            widget.restaurant)));
                  },
                  child: Text(
                    "Payer maintenant",
                    style: text5bb,
                  ),
                ),
              ),
              Expanded(
                  child: TextButton(
                onPressed: () {
                  Navigator.of(context).push(
                      new MaterialPageRoute(builder: (context) => HomePage()));
                },
                child: Text(
                  "Annuler",
                  style: text5nb,
                ),
              ))
            ],
          ),
        ],
      ),
    );
  }
}
