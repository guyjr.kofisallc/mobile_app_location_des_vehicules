import 'dart:io';

import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:url_launcher/url_launcher.Dart' as UrlLauncher;
import 'package:url_launcher/url_launcher.Dart';
import '../constants.dart';
import 'dart:async';

class BodyContactezNous extends StatefulWidget {
  BodyContactezNous();

  @override
  State<BodyContactezNous> createState() => _BodyContactezNousState();
}

class _BodyContactezNousState extends State<BodyContactezNous> {
  void whatsappSend({
    @required int phone,
    @required String message,
  }) async {
    String url() {
      if (Platform.isAndroid) {
        // add the [https]
        return "https://wa.me/$phone/?text=${Uri.parse(message)}"; // new line
      } else {
        // add the [https]
        return "https://api.whatsapp.com/send?phone=$phone=${Uri.parse(message)}"; // new line
      }
    }

    UrlLauncher.launch(url());
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
        child: Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(top: kDefaultPadding),
          child: Text(
            "RAPIDOS Cameroun",
            style: TextStyle(
                fontWeight: FontWeight.w400,
                fontSize: 20,
                color: kPrimaryColor),
          ) ,
        ),
        // Share.share('check out my website https://example.com', subject: 'Look what I made!'),
        Padding(padding: EdgeInsets.only(top: 25)),
        BlockTitle(TitleAndSubtitle("Adresse", "Bonamoussadi Bloc J"),
            TitleAndSubtitle("Adresse mail", "rapidoscm@gmail.com")),
        Padding(padding: EdgeInsets.only(top: 25)),
        BlockTitle(TitleAndSubtitle("Whatsapp , Message", "+237 680 401 646"),
            TitleAndSubtitle("Facebook , TikTok , Instagram", "rapidoseat")),
        Padding(padding: EdgeInsets.only(top: 25)),
        SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(
            children: [
              SocialMedia(
                  function: () {
                    UrlLauncher.launch("mailto:hammel.rapidos@gmail.com");
                  },
                  color: Colors.blueGrey,
                  image: Icons.mail_outlined),
              SocialMedia(
                  function: () {
                    whatsappSend(phone: 680401646, message: "");
                  },
                  color: Colors.yellow,
                  image: Icons.whatsapp_outlined),
              SocialMedia(
                  function: () async {
                    // _sendingSMS();
                    UrlLauncher.launch("sms:+237680401646");
                  },
                  color: kPrimaryColor,
                  image: Icons.message_outlined),
              SocialMedia(
                  function: () {
                    UrlLauncher.launch(
                        "https://www.facebook.com/RapidosEats?mibextid=LQQJ4d");
                  },
                  color: Colors.blueGrey,
                  image: Icons.facebook_outlined),
              SocialMedia(
                  function: () {
                    UrlLauncher.launch(
                        "https://www.tiktok.com/@rapidos_cameroun?_t=8Xotkmlyqo9&_r=1");
                  },
                  color: kPrimaryColor,
                  image: Icons.tiktok_outlined),
              SocialMedia(
                  function: () {
                    UrlLauncher.launch(
                        "https://www.instagram.com/rapidos_eats");
                  },
                  color: kPrimaryColor,
                  image: MdiIcons.instagram),
              SocialMedia(
                  function: () {
                    UrlLauncher.launch("https://rapidoseat.com");
                  },
                  color: Colors.blueGrey,
                  image: MdiIcons.web)
            ],
          ),
        )
      ],
    ));
  }
}

class BlockTitle extends StatelessWidget {
  BlockTitle(this.t1, this.t2);
  TitleAndSubtitle t1, t2;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            offset: Offset(0, 5),
            blurRadius: 5,
            color: Colors.grey.withOpacity(0.8),
          )
        ],
      ),
      padding: EdgeInsets.all(kDefaultPadding),
      width: size.width,
      child: Column(
        children: [
          t1,
          Padding(padding: EdgeInsets.only(top: 25)),
          t2,
        ],
      ),
    );
  }
}

class SocialMedia extends StatelessWidget {
  const SocialMedia({
    Key key,
    @required this.function,
    @required this.color,
    @required this.image,
  }) : super(key: key);

  final Function function;
  final Color color;
  final IconData image;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      // onTap: function,
      onTap: (() {
        // displayMessageConstruction(context);
        function();
      }),
      child: Container(
        margin: EdgeInsets.only(left: kDefaultPadding),
        padding: EdgeInsets.all(10),
        // height: 30,
        // width: 30,
        decoration: BoxDecoration(
            color: color.withOpacity(0.3),
            borderRadius: BorderRadius.circular(10)),
        child: Center(child: Icon(image)),
        height: 50,
        width: 50,
      ),
    );
  }
}

class TitleAndSubtitle extends StatelessWidget {
  TitleAndSubtitle(this.title, this.subtitle);
  String title;
  String subtitle;
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          border: Border(bottom: BorderSide(color: Colors.grey.shade100))),
      // height: 50,
      child: Column(
        children: [
          Container(
            // width: (size.width - kDefaultPadding * 2) / 2,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                  title,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 13,
                  ),
                ),
              ],
            ),
          ),
          Padding(padding: EdgeInsets.only(top: 15)),
          Container(
            //width: (size.width - kDefaultPadding * 2) / 2,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                  subtitle,
                  style: TextStyle(fontSize: 12),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
