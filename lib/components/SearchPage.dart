import 'package:flutter/material.dart';
import 'package:loading_overlay_pro/animations/bouncing_line.dart';
import 'package:rapidos_mobile/services/afficheResto%20copy.dart';

import '../constants.dart';
import 'les_plus_populaires.dart';

//Represents the Homepage widget
class SearchPage extends StatefulWidget {
  //`createState()` will create the mutable state for this widget at
  //a given location in the tree.
  @override
  _HomeState createState() => _HomeState();
}

//Our Home state, the logic and internal state for a StatefulWidget.
class _HomeState extends State<SearchPage> {
//Build our Home widget
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
        // appBar: new AppBar(
        //   title: new Text("SearchView ListView"),
        // ),
        body: HeaderWithSearchBox(
      size: size,
    ));
  }
}

class HeaderWithSearchBox extends StatefulWidget {
  const HeaderWithSearchBox({
    Key key,
    this.size,
  }) : super(key: key);

  final Size size;

  @override
  _HeaderWithSearchBoxState createState() => _HeaderWithSearchBoxState();
}

class _HeaderWithSearchBoxState extends State<HeaderWithSearchBox> {
  final _formKey = GlobalKey<FormState>();

  void recherche(String motcle) {
    setState(() {
      con = ListeRestoSearch(
        mocle: motcle,
      );
    });
  }

  dynamic con = Container(
    child: Text(
      "Lancer une recherche",
      style: TextStyle(
        fontWeight: FontWeight.bold,
        fontFamily: 'DayRoman',
        fontSize: 22,
      ),
    ),
  );

  //VARIABLE POUR LES TEXTFIELD
  final TextEditingController _searchController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return SingleChildScrollView(
      child: Column(
        children: [
          Container(
            height: size.height * 0.23,
            child: Stack(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(
                    top: kDefaultPadding,
                    left: kDefaultPadding,
                    right: kDefaultPadding,
                    bottom: 36 + kDefaultPadding,
                  ),
                  height: widget.size.height * 0.2,
                  decoration: BoxDecoration(
                      color: kPrimaryColor,
                      borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(36),
                        bottomRight: Radius.circular(36),
                      )),
                  child: Row(
                    children: <Widget>[
                      Text(
                        'Rapidos-eat',
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontFamily: 'DayRoman',
                            fontSize: 30,
                            color: Colors.white),
                      ),
                      Spacer(),
                      // Image.asset(
                      //   'assets/images/logo.png',
                      //   height: 130,
                      //   width: 130,
                      // ),
                    ],
                  ),
                ),
                Positioned(
                  bottom: 0,
                  left: 0,
                  right: 0,
                  child: Container(
                    alignment: Alignment.center,
                    margin: EdgeInsets.symmetric(horizontal: kDefaultPadding),
                    // padding: EdgeInsets.only(left: kDefaultPadding, bottom: 0),
                    height: 54,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(29),
                        boxShadow: [
                          BoxShadow(
                              offset: Offset(0, 10),
                              blurRadius: 50,
                              color: kPrimaryColor.withOpacity(0.23))
                        ]),
                    child: Form(
                      key: _formKey,
                      child: Padding(
                        padding: const EdgeInsets.all(0.0),
                        child: TextFormField(
                          controller: _searchController,
                          textInputAction: TextInputAction.search,
                          onChanged: (value) {
                            recherche(_searchController.text);
                          },
                          onFieldSubmitted: (value) {
                            if (_formKey.currentState.validate()) {
                              _formKey.currentState.save();
                              recherche(_searchController.text);
                            }
                          },
                          onSaved: (newValue) =>
                              {recherche(_searchController.text)},
                          decoration: InputDecoration(
                              contentPadding:
                                  const EdgeInsets.symmetric(vertical: 15.0),
                              fillColor: Colors.white,
                              filled: true,
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(30.0),
                                  borderSide: const BorderSide(width: 0.8)),
                              enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(30.0),
                                  borderSide: BorderSide(
                                      width: 0.8,
                                      color: Theme.of(context).primaryColor)),
                              hintText: "Que voulez vous manger",
                              suffixIcon: IconButton(
                                  icon: const Icon(
                                    Icons.search,
                                    size: 30,
                                  ),
                                  onPressed: () {
                                    recherche(_searchController.text);
                                  }),
                              prefixIcon: Padding(
                                padding: EdgeInsets.only(left: kDefaultPadding),
                              )
                              // suffixIcon: IconButton(
                              //     icon: const Icon(Icons.clear),
                              //     onPressed: () {}),
                              ),
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
          con,
        ],
      ),
    );
  }
}

class ListeRestoSearch extends StatelessWidget {
  ListeRestoSearch({this.mocle, Key key}) : super(key: key);
  String mocle;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: kDefaultPadding * 2.5),
      child: FutureBuilder(
        future: AfficheRestaurantSearch(mocle).AfficheResto(mocle),
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return Container(
              // height: size.height,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Center(
                      child: Image.asset(
                    "assets/images/donut.gif",
                    width: 150,
                    height: 150,
                  )),
                ],
              ),
            );
          } else {
            if (snapshot.data.length != 0) {
              return SingleChildScrollView(
                child: Column(
                  children: [
                    Text(
                      "Voici la liste des restauranta qui font dans \"$mocle\"",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontFamily: 'DayRoman',
                          fontSize: 15,
                          color: Colors.black),
                    ),
                    Container(
                      padding: EdgeInsets.all(kDefaultPadding),
                      child: ListView.separated(
                        separatorBuilder: (_, m) => Divider(),
                        scrollDirection: Axis.vertical,
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        itemCount: snapshot.data.length,
                        itemBuilder: (context, index) {
                          String _image, _detail, _name;
                          if (snapshot.data[index].description != null) {
                            _image = snapshot.data[index].photo.toString();
                            _detail =
                                snapshot.data[index].description.toString();
                          } else {
                            _image =
                                "https://pbs.twimg.com/media/DNjWneUX4AIXUMW.jpg";
                            _detail =
                                "Notre équipe vous accueille pour tous les moments de la journée et le soir, dans une ambiance conviviale, raffinée et… gourmande";
                          }
                          if (snapshot.data[index].name != null) {
                            _name = snapshot.data[index].name.toString();
                          } else {
                            _name = "no mane";
                          }

                          return SinglePopulary(snapshot.data[index]);
                        },
                      ),
                    ),
                  ],
                ),
              );
            } else {
              return Container(
                padding: EdgeInsets.only(bottom: kDefaultPadding * 2.5),
                child: Center(
                  child: Text(
                    "Aucun resultat trouve",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontFamily: 'DayRoman',
                        fontSize: 15,
                        color: kPrimaryColor),
                  ),
                ),
              );
            }
          }
        },
      ),
    );
  }
}
