import 'package:flutter/material.dart';
import 'body_map.dart';

class Map extends StatelessWidget {
  Map();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(context),
      body: BodyMap(),

      //bottomNavigationBar: Footer_all(),
    );
  }

  AppBar buildAppBar(BuildContext context) {
    return AppBar(
      centerTitle: true,
      title: Text(
        "Map",
        style: TextStyle(
          color: Colors.black,
          fontWeight: FontWeight.bold,
        ),
      ),
      actions: <Widget>[],
    );
  }
}
