import 'dart:ui';
import 'package:flutter/material.dart';
import '../constants.dart';

class TitleWithMoreBtn extends StatelessWidget {
  const TitleWithMoreBtn({
    this.title = '',
  });
  final String title;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: kDefaultPadding),
      child: Row(
        children: [
          TitleWithCustormUnderLine(
            text: title,
          ),
          Spacer(),
        ],
      ),
    );
  }
}

class TitleWithCustormUnderLine extends StatelessWidget {
  const TitleWithCustormUnderLine({
    this.text = '',
  });

  final String text;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      height: size.height * 0.03,
      child: Stack(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(left: kDefaultPadding / 4),
            child: Text(
              text,
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontFamily: font,
                fontSize: size.height * 0.02,
              ),
            ),
          ),
          Positioned(
            bottom: 0,
            left: 0,
            right: 0,
            child: Container(
              margin: EdgeInsets.only(right: kDefaultPadding / 4),
              height: 7,
              color: Colors.grey.withOpacity(0.1),
            ),
          )
        ],
      ),
    );
  }
}
