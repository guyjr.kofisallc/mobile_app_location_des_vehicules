import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:rapidos_mobile/components/title_with_more_btn.dart';
import '../constants.dart';
import '../services/afficheResto.dart';
import 'SearchPage.dart';
import 'caroussel_meilleures_offre.dart';
import 'caroussel_statut.dart';
import 'cuisine_detail.dart';
import 'les_plus_populaires.dart';

class BodyDetailAcceuil extends StatefulWidget {
  BodyDetailAcceuil();

  @override
  State<BodyDetailAcceuil> createState() => _BodyDetailAcceuilState();
}

class _BodyDetailAcceuilState extends State<BodyDetailAcceuil> {
  final storage = FlutterSecureStorage();
  void videCart() {
    cart.deleteAllCart();
  }

  // @override
  // void initState() {
  //   // getCurentLocation();
  // }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    videCart();
    // getCurentLocation();
    // print(actue.latitude);
    return SingleChildScrollView(
      child: Column(
        children: [
          InkWell(
            onTap: () {
              Navigator.of(context).push(
                  new MaterialPageRoute(builder: (context) => SearchPage()));
            },
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: kDefaultPadding),
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 11),
              // height: 40,
              decoration: BoxDecoration(
                  color: Colors.white,
                  border:
                      Border.all(style: BorderStyle.solid, color: Colors.grey),
                  borderRadius: BorderRadius.circular(30)),
              child: Row(
                children: [
                  Icon(
                    Icons.search,
                    size: 23,
                  ),
                  Padding(padding: EdgeInsets.symmetric(horizontal: 10)),
                  Text(
                    "Que voulez vous manger",
                    style: TextStyle(color: Colors.grey),
                  ),
                  Spacer(),
                  Text(""),
                  Icon(
                    Icons.clear,
                    size: 23,
                  ),
                ],
              ),
            ),
          ),

          // TitleWithMoreBtn(title: 'Meilleures offres'),
          Padding(padding: const EdgeInsets.only(top: 0)),
          CarousselMeilleurOffre(size: size),
          TitleWithMoreBtn(title: 'Catégories populaires'),
          Container(
            margin: EdgeInsets.only(top: 10),
            width: size.width,
            child: SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  children: [
                    Padding(padding: EdgeInsets.only(left: kDefaultPadding)),
                    CategoriePopulaire(
                        function: () {
                          Navigator.of(context).push(new MaterialPageRoute(
                              builder: (context) => DetailCuisine("burger")));
                        },
                        color: kPrimaryColor,
                        image: Image.asset("assets/images/burger.png")),
                    CategoriePopulaire(
                        function: () {
                          Navigator.of(context).push(new MaterialPageRoute(
                              builder: (context) => DetailCuisine("pizza")));
                        },
                        color: Colors.amber,
                        image: Image.asset("assets/images/pizza.png")),
                    CategoriePopulaire(
                        function: () {
                          Navigator.of(context).push(new MaterialPageRoute(
                              builder: (context) => DetailCuisine("poulet")));
                        },
                        color: Colors.black,
                        image: Image.asset("assets/images/poulet.png")),
                    CategoriePopulaire(
                        function: () {
                          Navigator.of(context).push(new MaterialPageRoute(
                              builder: (context) =>
                                  DetailCuisine("fast food")));
                        },
                        color: Colors.green,
                        image: Image.asset("assets/images/ship.png")),
                    CategoriePopulaire(
                        function: () {
                          Navigator.of(context).push(new MaterialPageRoute(
                              builder: (context) => DetailCuisine("Africain")));
                        },
                        color: Colors.blueGrey,
                        image: Image.asset("assets/images/salade.png")),
                    CategoriePopulaire(
                        function: () {
                          Navigator.of(context).push(new MaterialPageRoute(
                              builder: (context) => DetailCuisine("boisson")));
                        },
                        color: Colors.yellow,
                        image: Image.asset("assets/images/jus.png")),
                    CategoriePopulaire(
                        function: () {
                          Navigator.of(context).push(new MaterialPageRoute(
                              builder: (context) => DetailCuisine("glace")));
                        },
                        color: kPrimaryColor,
                        image: Image.asset("assets/images/glace.png")),
                    Padding(padding: EdgeInsets.only(left: kDefaultPadding))
                  ],
                )),
          ),
          Padding(padding: const EdgeInsets.only(top: 15)),
          TitleWithMoreBtn(title: 'Produits populaires'),
          Padding(padding: const EdgeInsets.only(top: 10)),
          Carousell_statut(
              // cart: cart,
              ),
          // Padding(padding: const EdgeInsets.only(top: 25)),

          // Padding(padding: const EdgeInsets.only(top: 5)),
          TitleWithMoreBtn(title: 'Nos restaurants'),
          ListeResto(),
        ],
      ),
    );
  }
}

class CategoriePopulaire extends StatelessWidget {
  const CategoriePopulaire({
    Key key,
    @required this.function,
    @required this.color,
    @required this.image,
  }) : super(key: key);

  final Function function;
  final Color color;
  final Image image;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      // onTap: function,
      onTap: (() {
        // displayMessageConstruction(context);
        function();
      }),
      child: Container(
        margin: EdgeInsets.only(left: kDefaultPadding),
        padding: EdgeInsets.all(10),
        // height: 30,
        // width: 30,
        decoration: BoxDecoration(
            color: color.withOpacity(0.3),
            borderRadius: BorderRadius.circular(10)),
        child: Center(child: image),
        height: 50,
        width: 50,
      ),
    );
  }
}

class ListeResto extends StatelessWidget {
  ListeResto(
      {
      // this.cart,
      Key key})
      : super(key: key);
  // FlutterCart cart;
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: FutureBuilder(
        future: AfficheRestaurant().AfficheResto(),
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return Container(
              // height: size.height,
              color: Colors.white,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Center(
                      child: Image.asset(
                    "assets/images/donut.gif",
                    width: 150,
                    height: 150,
                  )),
                ],
              ),
            );
          } else if (snapshot.hasError) {
            return Column(
              children: [
                Center(
                    child: Image.asset(
                  "assets/images/erreur.png",
                  width: 100,
                  height: 100,
                )),
                Text("Probleme de Serveur veillez ressayer plus tard"),
              ],
            );
          } else if (snapshot.data == "600") {
            return Column(
              children: [
                Center(
                  child: Image.asset(
                    "assets/images/erreur.png",
                    width: 100,
                    height: 100,
                  ),
                ),
                Text("pas de connexion a internet"),
              ],
            );
          } else if (snapshot.data == "500") {
            return Column(
              children: [
                Center(
                    child: Image.asset(
                  "assets/images/erreur.png",
                  width: 100,
                  height: 100,
                )),
                Text("Probleme de Serveur veillez ressayer plus tard"),
              ],
            );
          } else {
            return Container(
              padding: EdgeInsets.all(kDefaultPadding),
              child: ListView.separated(
                separatorBuilder: (_, m) => Divider(),
                scrollDirection: Axis.vertical,
                shrinkWrap: true,
                reverse: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount: snapshot.data.length,
                itemBuilder: (context, index) {
                  String _image, _detail, _name;
                  if (snapshot.data[index].description != null) {
                    _image = snapshot.data[index].photo.toString();
                    _detail = snapshot.data[index].description.toString();
                  } else {
                    _image = "https://pbs.twimg.com/media/DNjWneUX4AIXUMW.jpg";
                    _detail =
                        "Notre équipe vous accueille pour tous les moments de la journée et le soir, dans une ambiance conviviale";
                  }
                  if (snapshot.data[index].name != null) {
                    _name = snapshot.data[index].name.toString();
                  } else {
                    _name = "no mane";
                  }
                  if (snapshot.data[index].roles[0].name.toString() ==
                      "Name.ROLE_CLIENT") {
                    cart.deleteAllCart();
                    return SinglePopulary(snapshot.data[index]);
                  } else {
                    cart.deleteAllCart();
                    return SinglePopulary(snapshot.data[index]);
                  }
                },
              ),
            );
          }
        },
      ),
    );
  }
}
