import 'dart:io';

import 'package:flutter/material.dart';
import 'package:rapidos_mobile/components/body_detail_acceuil.dart';
import '../constants.dart';
import 'drawer_client.dart';
import 'notification.dart';

class DetailAcceuil extends StatelessWidget {
  DetailAcceuil();
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => exit(0),
      child: Scaffold(
        backgroundColor: Colors.white,
        drawer: (ClientDrawer()),
        appBar: buildAppBar(context),
        body: BodyDetailAcceuil(),
        // bottomNavigationBar: Footer_all(),
      ),
    );
  }

  AppBar buildAppBar(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return AppBar(
      backgroundColor: Colors.white,
      elevation: 0.5,
      centerTitle: true,
      leading: Builder(
        builder: (BuildContext context) {
          return IconButton(
            icon: Image.asset(
              "assets/images/burger.gif",
              width: 30,
              height: 30,
              // color: Colors.red,
            ),
            onPressed: () {
              Scaffold.of(context).openDrawer();
            },
            tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
          );
        },
      ),
      title: Container(
        padding: EdgeInsets.all(6),
        child: ClipRRect(
          //un peu comme un conteneur mais avec border radius
          borderRadius: BorderRadius.circular(9.0),
          child: Image.asset(
            "assets/images/rapidoseat.png",
            width: size.width * 0.4,
            height: size.width * 0.09,
            fit: BoxFit.cover,
          ),
        ),
      ),
      actions: <Widget>[
        IconButton(
            color: kPrimaryColor,
            onPressed: () {
              Navigator.of(context).push(new MaterialPageRoute(
                  builder: (context) => NotificationPage()));
            },
            icon: Icon(
              Icons.notifications_active_outlined,
              size: 30,
            )),
      ],
    );
  }
}
