import 'package:flutter/material.dart';
import 'package:rapidos_mobile/components/body_payement.dart';
import 'package:rapidos_mobile/components/loadPayement.dart';
import 'package:rapidos_mobile/constants.dart';
import 'package:rapidos_mobile/entities/adresse.dart';
import '../entities/CommandeSend.dart';
import '../entities/Panier.dart';
import '../services/payementservice.dart';
import 'body_adresse_livraison.dart';

displayDialog(
  BuildContext context,
  String title,
  String text,
) {
  Size size = MediaQuery.of(context).size;
  showDialog(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
          child: Container(
            constraints: BoxConstraints(maxHeight: 200),
            child: Padding(
              padding: const EdgeInsets.all(kDefaultPadding),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      InkWell(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: CircleAvatar(
                          child: Icon(
                            Icons.close,
                            color: Colors.white,
                            size: 14,
                          ),
                          backgroundColor: kPrimaryColor,
                          radius: 13,
                        ),
                      ),
                    ],
                  ),
                  // Text(
                  //   "Asso’o, Connecte toi !",
                  //   style: TextStyle(
                  //     fontFamily: font,
                  //     fontSize: 20,
                  //     fontWeight: FontWeight.bold,
                  //     fontStyle: FontStyle.normal,
                  //   ),
                  //   textAlign: TextAlign.left,
                  // ),
                  Text(
                    "Error",
                    style: TextStyle(
                      fontFamily: font,
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      fontStyle: FontStyle.normal,
                    ),
                    textAlign: TextAlign.left,
                  ),
                  // Text(
                  //   "de ways",
                  //   style: TextStyle(
                  //     fontFamily: font,
                  //     fontSize: 20,
                  //     fontWeight: FontWeight.bold,
                  //     fontStyle: FontStyle.normal,
                  //   ),
                  //   textAlign: TextAlign.left,
                  // ),
                  Padding(padding: EdgeInsets.all(5)),
                  Text(
                    text,
                    style: TextStyle(
                      fontFamily: font,
                      fontSize: 13,
                      // fontWeight: FontWeight.bold,
                      fontStyle: FontStyle.normal,
                    ),
                    textAlign: TextAlign.left,
                  ),
                  Padding(padding: EdgeInsets.all(5)),
                ],
              ),
            ),
          ),
        );
      });
}

// Adresse _adresseClient = new Adresse();
// FlutterCart cart2 = new FlutterCart();
final _formKey1 = GlobalKey<FormState>();

List<String> countries;
Widget load = Text(
  "Payer",
  style: TextStyle(
      color: Colors.white,
      fontSize: 14,
      fontFamily: 'DayRoman',
      fontWeight: FontWeight.bold),
);
//   void change() {
//   setState(() {
//     load = Center(
//     child: CircularProgressIndicator(
//       color: Colors.white,
//       strokeWidth: 2,
//     ),
//   ),;
//   });
// }

// ];

displayDialogFacture(
    BuildContext context,
    String adresse,
    final FlutterCart cart,
    String nomResto,
    String idResto,
    double distance,
    PositionCart positionCart) {
  Size size = MediaQuery.of(context).size;
  int livraison = 1000;
  // if (distance > 7)
  //   livraison = 1500;
  // else if (distance > 14) livraison = 2000;
  // FlutterCart cart1 = cart;
  distance < 4
      ? livraison = 500
      : distance < 10
          ? livraison = 1000
          : distance < 14
              ? livraison = 1500
              : livraison = 2000;

  TextEditingController _numberController1 = new TextEditingController();
  showDialog(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
          child: Container(
            height: size.height * 0.65,
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.all(kDefaultPadding),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        InkWell(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: CircleAvatar(
                            child: Icon(
                              Icons.close,
                              color: Colors.white,
                              size: 14,
                            ),
                            backgroundColor: kPrimaryColor,
                            radius: 13,
                          ),
                        ),
                      ],
                    ),
                    FutureBuilder(
                        future: PayementService().sendInfo(),
                        builder: (context, snapshot) {
                          if (!snapshot.hasData) {
                            return Container(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Center(
                                      child: Image.asset(
                                    "assets/images/donut.gif",
                                    width: 150,
                                    height: 150,
                                  )),
                                ],
                              ),
                            );
                          } else if (snapshot.data == "500") {
                            Navigator.pop(context);
                            return displayDialog(
                                context,
                                "Probleme avec le serveur , veillez Essayer plus tard",
                                "OUPS!!!");
                          } else if (snapshot.data == "401") {
                            Navigator.pop(context);
                            return displayDialog(
                                context,
                                "Probleme d'authorisation , veillez Essayer plus tard",
                                "OUPS!!!");
                          } else if (snapshot.data == "Probleme de connexion") {
                            Navigator.pop(context);
                            return displayDialog(
                                context,
                                "Probleme de connexion , veillez verifier votre connexion",
                                "OUPS!!!");
                          } else {
                            return Container(
                              child: Form(
                                key: _formKey1,
                                child: SingleChildScrollView(
                                  child: Column(children: [
                                    Center(
                                      child: Text(
                                        "FACTURE",
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                    Container(
                                        padding: EdgeInsets.all(0),
                                        margin: EdgeInsets.only(
                                            top: kDefaultPadding),
                                        child: SingleChildScrollView(
                                          child: Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              children: <Widget>[
                                                AfficheTitrePrix2(
                                                  size: size,
                                                  titre: "Commande",
                                                  prix: cart.getTotalAmount(),
                                                ),
                                                AfficheTitrePrix2(
                                                  size: size,
                                                  titre: "Livraison",
                                                  prix: double.parse(
                                                      livraison.toString()),
                                                ),
                                                Divider(
                                                  height: 10,
                                                  thickness: 5,
                                                ),
                                                AfficheTitrePrix2(
                                                  size: size,
                                                  titre: "Total",
                                                  prix: (
                                                      // cart.getTotalAmount()
                                                      cart.getTotalAmount() +
                                                          livraison),
                                                ),
                                                Padding(
                                                    padding: EdgeInsets.only(
                                                        top: 20)),
                                                Container(
                                                  child: Text(
                                                    "Entrer le numero de telephone pour le payement (RETRAIT de TAKWID GROUP)",
                                                    style: TextStyle(
                                                        fontSize: 9,
                                                        fontWeight:
                                                            FontWeight.bold),
                                                    textAlign:
                                                        TextAlign.justify,
                                                  ),
                                                ),
                                                Image.asset(
                                                  "assets/images/payement.jpg",
                                                  width: 250,
                                                  height: 50,
                                                  // color: Colors.red,
                                                ),
                                                TextFieldContainer(
                                                  child: TextFormField(
                                                    onTap: () {},
                                                    keyboardType:
                                                        TextInputType.number,
                                                    validator: (value1) {
                                                      if (value1 == null ||
                                                          value1.isEmpty) {
                                                        // displayDialog(context,
                                                        //     "Veillez renseigner votre numero d'utilisateur");
                                                        return 'Entrer votre numero ';
                                                      }
                                                      return null;
                                                    },
                                                    controller:
                                                        _numberController1,
                                                    onChanged: (value1) {},
                                                    decoration: InputDecoration(
                                                      icon: Icon(Icons.phone),
                                                      labelText: "6********",
                                                      labelStyle: TextStyle(
                                                        fontFamily: 'DayRoman',
                                                        fontSize: 10,
                                                      ),
                                                      border: InputBorder.none,
                                                    ),
                                                  ),
                                                ),
                                              ]),
                                        )),
                                    Padding(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 5, vertical: 0),
                                    ),
                                    Center(
                                      child: SizedBox(
                                        width: size.width * 0.75,
                                        height: 50,
                                        child: TextButton(
                                          style: boutonRouge,
                                          onPressed: () async {
                                            var telephone =
                                                _numberController1.text;

                                            if (_formKey1.currentState
                                                .validate()) {
                                              if (telephone.startsWith(
                                                      "6", 0) &&
                                                  telephone.length == 9) {
                                                if (!telephone.startsWith(
                                                        "5", 1) &&
                                                    !telephone
                                                        .startsWith("7", 1) &&
                                                    !telephone.startsWith(
                                                        "8", 1) &&
                                                    !telephone.startsWith(
                                                        "9", 1)) {
                                                  print(telephone[1]);
                                                  ScaffoldMessenger.of(context)
                                                      .showSnackBar(
                                                    const SnackBar(
                                                        backgroundColor:
                                                            kPrimaryColor,
                                                        content: Text(
                                                          'Numero invalide',
                                                          style: TextStyle(
                                                              fontFamily:
                                                                  'DayRoman',
                                                              fontSize: 10,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold),
                                                        )),
                                                  );
                                                } else {
                                                  String numero =
                                                      "237" + telephone;
                                                  String cle = "token " +
                                                      snapshot.data.token;
                                                  storage.write(
                                                      key: "cle", value: cle);
                                                  String token = "token " +
                                                      snapshot.data.token;
                                                  // change();

                                                  displayDialogPay(
                                                      context,
                                                      numero,
                                                      token,
                                                      adresse,
                                                      cart,
                                                      positionCart,
                                                      livraison,
                                                      nomResto,
                                                      idResto);
                                                }
                                              } else {
                                                ScaffoldMessenger.of(context)
                                                    .showSnackBar(
                                                  const SnackBar(
                                                      backgroundColor:
                                                          kPrimaryColor,
                                                      content: Text(
                                                        'Numero invalide',
                                                        style: TextStyle(
                                                            fontFamily:
                                                                'DayRoman',
                                                            fontSize: 10,
                                                            fontWeight:
                                                                FontWeight
                                                                    .bold),
                                                      )),
                                                );
                                              }
                                            } else {
                                              ScaffoldMessenger.of(context)
                                                  .showSnackBar(
                                                const SnackBar(
                                                    backgroundColor:
                                                        kPrimaryColor,
                                                    content: Text(
                                                      'Remplir le numero',
                                                      style: TextStyle(
                                                          fontFamily:
                                                              'DayRoman',
                                                          fontSize: 10,
                                                          fontWeight:
                                                              FontWeight.bold),
                                                    )),
                                              );
                                            }

                                            // Navigator.push(
                                            //     context,
                                            //     MaterialPageRoute(
                                            //         builder: (context) => SimpleS3Test2(
                                            //             "3",
                                            //             numero,
                                            //             cle,
                                            //             "je teste mes payement3")));
                                          },
                                          child: load,
                                        ),
                                      ),
                                    ),
                                  ]),
                                ),
                              ),
                            );
                          }
                        }),
                    Padding(padding: EdgeInsets.all(5)),
                  ],
                ),
              ),
            ),
          ),
        );
      });
}
