import 'package:flutter/material.dart';
import 'package:flutter_cart/flutter_cart.dart';
import 'package:flutter_phone_direct_caller/flutter_phone_direct_caller.dart';
import 'package:flutter_svg/svg.dart';
import 'package:rapidos_mobile/entities/CommandeSend.dart';
import 'package:rapidos_mobile/entities/adresse.dart';
import '../constants.dart';
import 'command_final.dart';

CommandeSend _commande = new CommandeSend();

class BodyPayement extends StatefulWidget {
  BodyPayement(this._adresseClient, this.cart, this.nom, this.idResto);
  Adresse _adresseClient;
  FlutterCart cart;
  String nom, idResto;

  @override
  _BodyPayementState createState() => _BodyPayementState();
}

class _BodyPayementState extends State<BodyPayement> {
  String _verticalGroupValue = "Orange Money";
  String _verticalGroupValue2 = "oui";

  List<String> _status = ["Orange Money", "MTN Mobile Money"];
  List<String> _status2 = ["Oui", "Non"];
  List<Widget> containt = [
    Text(
      "Veillez confirmer le transfert d'argent apres validation svp",
      style: TextStyle(fontFamily: 'DayRoman', fontWeight: FontWeight.bold),
    ),
    SinglePay(
        image: "assets/images/om.jpg",
        nom: "Orange money",
        hint: "Faite un depot a  \n ce numero 695400777"
        // hint: "Faite un depôt a ce numero 695400777",
        ),
    SinglePay(
      image: "assets/images/momo.jpg",
      nom: "MTN Mobile Money",
      hint: "Faite un depot a \n ce numero 671204626",
    ),
  ];
  List<String> messagePayement = [
    " ",
    " Faite un depot a  \n ce numero 695400777",
    "Faite un depot a  \n ce numero 671204626"
  ];

  _callNumber(String montant) async {
    bool res = await FlutterPhoneDirectCaller.callNumber(
        "*126*1*1*671204626*" + montant.toString() + "#");
  }

  _callNumber2(String montant) async {
    bool res = await FlutterPhoneDirectCaller.callNumber(
        "#150*1*1*695400777*" + montant.toString() + "#");
  }

  int payement = 0;
  int afficheNum = 0;
  int simInPhone =
      0; //0 veut dire la puce est dans le telephone et 1 elle nest pas
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return SingleChildScrollView(
      child: Container(
          width: size.width - (kDefaultPadding * 2),
          //color: Colors.amber,
          padding: EdgeInsets.only(top: kDefaultPadding),
          margin:
              EdgeInsets.only(left: kDefaultPadding, right: kDefaultPadding),
          child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                ),
                SvgPicture.asset(
                  "assets/images/payement.svg",
                  width: size.width * 0.6,
                  //width: 185,
                ),
                AfficheTitrePrix(
                  size: size,
                  titre: "Commande",
                  prix: widget.cart.getTotalAmount(),
                ),
                AfficheTitrePrix(
                  size: size,
                  titre: "Livraison",
                  prix: 1000,
                ),
                AfficheTitrePrix(
                  size: size,
                  titre: "Total",
                  prix: (widget.cart.getTotalAmount() + 1000),
                ),
                SizedBox(
                  width: size.width / 2,
                  height: 50,
                  child: TextButton(
                    style: boutonRouge,
                    onPressed: () async {
                      if (widget._adresseClient.rue == null ||
                          widget._adresseClient.appartement == null) {
                        widget._adresseClient.rue = "";
                        widget._adresseClient.appartement = "";
                      }

                      if (simInPhone == 0) {
                        if (payement == 0) {
                          _callNumber2(
                              (widget.cart.getTotalAmount().hashCode + 1000)
                                  .toString());
                          // Navigator.of(context).push(
                          //   new MaterialPageRoute(
                          //     builder: (context) => CommandeFinal(
                          //         widget._adresseClient,
                          //         cart,
                          //         widget.nom,
                          //         _verticalGroupValue,
                          //         widget.idResto,
                          //         _commande),
                          //   ),
                          // );
                        } else {}
                      } else {}
                    },
                    child: Text(
                      "Je valide",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 16,
                      ),
                    ),
                  ),
                ),
                Padding(padding: EdgeInsets.only(top: 25)),
              ])),
    );
  }
}

class SinglePay extends StatelessWidget {
  SinglePay({
    this.image,
    this.hint,
    this.nom,
    Key key,
  }) : super(key: key);
  String image, nom, hint;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      color: Colors.grey.shade200,
      padding: EdgeInsets.only(bottom: kDefaultPadding - 10, top: 10),
      child: Column(
        //mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          ListTile(
            //contentPadding: EdgeInsets.all(kDefaultPadding),
            leading: Container(
              width: 60,
              height: 70,
              child: ClipRRect(
                //un peu comme un conteneur mais avec border radius
                borderRadius: BorderRadius.circular(110.0),
                child: Image.asset(
                  image,
                  fit: BoxFit.cover,
                ),
              ),
            ),
            title: Text(
              nom,
              style:
                  TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
            ),
            //minVerticalPadding: 0,
            subtitle: Row(
              children: <Widget>[
                Text(hint)
                // Padding(padding: const EdgeInsets.only(top: 25)),
                // Container(
                //   //margin: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                //   padding: EdgeInsets.symmetric(horizontal: 5, vertical: 1),
                //   width: size.width * 0.45,
                //   decoration: BoxDecoration(
                //       //borderRadius: BorderRadius.circular(29),
                //       ),
                //   child: TextField(
                //     onChanged: null,
                //     decoration: InputDecoration(
                //       labelText: hint,
                //       // suffixIcon: Icon(
                //       //   Icons.visibility,
                //       //   color: kPrimaryColor,
                //       // ),
                //       border: UnderlineInputBorder(),
                //     ),
                //   ),
                // ),
              ],
            ),
            // trailing: Text(
            //   "588 XAF",
            //   style: TextStyle(
            //       fontWeight: FontWeight.bold, color: Colors.red),
            // ),
          ),
        ],
      ),
    );
  }
}

class AfficheTitrePrix extends StatelessWidget {
  AfficheTitrePrix({this.prix, this.size, this.titre, Key key})
      : super(key: key);

  Size size;
  double prix;
  String titre;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: size.width * 0.6,
      padding: const EdgeInsets.only(top: 15),
      child: Row(
        children: <Widget>[
          Container(
            //padding: const EdgeInsets.only(left: kDefaultPadding),
            // width: (size.width / 2) - kDefaultPadding,
            child: Text(
              titre,
              style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                  fontFamily: 'DayRoman',
                  fontSize: 10),
            ),
          ),
          //Padding(padding: const EdgeInsets.only(left: kDefaultPadding)),
          Spacer(),
          Container(
              //color: Colors.amber,
              // width: (size.width / 2) - kDefaultPadding,
              child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Text(
                prix.toString() + " FCFA",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: kPrimaryColor,
                  fontSize: 10,
                  fontFamily: 'DayRoman',
                ),
              ),
            ],
          ))
        ],
      ),
    );
  }
}

class AfficheTitrePrix2 extends StatelessWidget {
  AfficheTitrePrix2({this.prix, this.size, this.titre, Key key})
      : super(key: key);

  Size size;
  double prix;
  String titre;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: size.width * 0.7,
      padding: const EdgeInsets.only(top: 15),
      child: Row(
        children: <Widget>[
          Container(
            //padding: const EdgeInsets.only(left: kDefaultPadding),
            // width: (size.width / 2) - kDefaultPadding,
            child: Text(
              titre,
              style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                  fontFamily: 'DayRoman',
                  fontSize: 10),
            ),
          ),
          //Padding(padding: const EdgeInsets.only(left: kDefaultPadding)),
          Spacer(),
          Container(
              //color: Colors.amber,
              // width: (size.width / 2) - kDefaultPadding,
              child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Text(
                prix.toString() + " FCFA",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: kPrimaryColor,
                  fontSize: 10,
                  fontFamily: 'DayRoman',
                ),
              ),
            ],
          ))
        ],
      ),
    );
  }
}
