import 'package:rapidos_mobile/components/pdf/model/customer.dart';
import 'package:rapidos_mobile/components/pdf/model/supplier.dart';
import 'package:rapidos_mobile/entities/cartElement.dart';

class Invoice {
  final InvoiceInfo info;
  final Supplier supplier;
  final Customer customer;
  final List<CartItem> items;

  const Invoice({
    this.info,
    this.supplier,
    this.customer,
    this.items,
  });
}

class InvoiceInfo {
  final DateTime date;
  final String modePayement;
  final String number;
  final String reference;
  final String position;
  final String nomResto;
  final int livraison;

  const InvoiceInfo(
      {this.date,
      this.modePayement,
      this.number,
      this.reference,
      this.position,
      this.nomResto,
      this.livraison});
}

class InvoiceItem {
  final String description;
  final DateTime date;
  final int quantity;
  final double vat;
  final double unitPrice;

  const InvoiceItem({
    this.description,
    this.date,
    this.quantity,
    this.vat,
    this.unitPrice,
  });
}
