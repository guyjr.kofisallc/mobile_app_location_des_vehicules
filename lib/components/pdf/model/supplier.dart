class Supplier {
  final String name;
  final String mail;
  final String address;

  const Supplier({
    this.name,
    this.address,
    this.mail,
  });
}
