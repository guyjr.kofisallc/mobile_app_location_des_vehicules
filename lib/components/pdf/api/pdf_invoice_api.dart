import 'dart:io';
import 'dart:typed_data';
import 'package:flutter/services.dart';
import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart' as pw;
import 'package:pdf/widgets.dart';
import 'package:rapidos_mobile/components/pdf/api/pdf_api.dart';
import '../../../entities/Panier.dart';
import '../model/customer.dart';
import '../model/invoice.dart';
import '../model/supplier.dart';
import '../utils.dart';

class PdfInvoiceApi {
  static Future<File> generate(Invoice invoice, FlutterCart cart) async {
    final pdf = Document();
    Uint8List logobytes;
    // PdfImage _logoImage;
    ByteData _bytes = await rootBundle.load('assets/images/logoFacture.png');
    logobytes = _bytes.buffer.asUint8List();

    pdf.addPage(MultiPage(
      build: (context) => [
        buiuldheader(logobytes, invoice),
        SizedBox(height: 0.2 * PdfPageFormat.cm),
        buildTitle(invoice),
        buildInvoice(invoice),
        Divider(),
        buildTotal(invoice, cart),
        SizedBox(height: 5 * PdfPageFormat.cm),
        pw.Row(children: [
          pw.Text("NB:", style: TextStyle(fontWeight: FontWeight.bold)),
          pw.Text(
              "Cette facture n'est valable uniquement pour une et une seule commande. \n il est a presenter au livreur avant reception de la commande.")
        ]),
      ],
      footer: (context) => buildFooter(invoice),
    ));
    String nameDoc = "Fature Rapidos(" + DateTime.now().toString() + ").pdf";

    return PdfApi.saveDocument(name: nameDoc, pdf: pdf);
  }

  static pw.Column buiuldheader(Uint8List logobytes, Invoice invoice) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: 1 * PdfPageFormat.cm),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            pw.Container(
              child: logobytes != null
                  ? pw.Image(pw.MemoryImage(logobytes), fit: pw.BoxFit.contain)
                  : pw.Container(),
              decoration: pw.BoxDecoration(
                color: PdfColors.white,
                // border: BoxBorder(width: 0.5)),
              ),
              height: 120,
              padding: pw.EdgeInsets.all(10),
            ),
            Container(
              height: 60,
              width: 60,
              child: BarcodeWidget(
                barcode: Barcode.qrCode(),
                data: invoice.info.reference,
              ),
            ),
          ],
        ),
        SizedBox(height: 1 * PdfPageFormat.cm),
        Row(
          crossAxisAlignment: CrossAxisAlignment.end,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            buildCustomerAddress(invoice.supplier),
            buildInvoiceInfo(invoice.info),
          ],
        ),
      ],
    );
  }

  static Widget buildCustomerAddress(Supplier supplier) => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(supplier.name, style: TextStyle(fontWeight: FontWeight.bold)),
          Text(supplier.mail),
          Text(supplier.address),
        ],
      );

  static Widget buildInvoiceInfo(InvoiceInfo info) {
    final titles = <String>[
      'Date de payement: ',
      'Mode de payement: ',
      'Numero de Payement: ',
      'Reference: ',
      'Position de livraison: '
    ];
    final data = <String>[
      Utils.formatDate(info.date),
      info.modePayement,
      info.number.toString(),
      info.reference,
      info.position
    ];

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: List.generate(titles.length, (index) {
        final title = titles[index];
        final value = data[index];

        return buildText(title: title, value: value, width: 300);
      }),
    );
  }

  static Widget buildSupplierAddress(Supplier supplier) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(supplier.name, style: TextStyle(fontWeight: FontWeight.bold)),
        SizedBox(height: 1 * PdfPageFormat.mm),
        Text(supplier.address),
      ],
    );
  }

  static Widget buildTitle(Invoice invoice) => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Facture',
            style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
          ),
          SizedBox(height: 0.8 * PdfPageFormat.cm),
          Text(invoice.info.nomResto),
          SizedBox(height: 0.8 * PdfPageFormat.cm),
        ],
      );

  static Widget buildInvoice(Invoice invoice) {
    final headers = ['Produit', 'Date', 'Quantity', 'Unit Price', 'Total'];
    final data = invoice.items.map((item) {
      // final total = item.unitPrice * item.quantity * (1 + item.vat);

      print("(//////////////////)");
      print(item.toString());
      print("(//////////////////)");
      return [
        item.productName,
        Utils.formatDate(DateTime.now()),
        '${item.quantity}',
        '${item.unitPrice}',
        '${item.subTotal}',
      ];
    }).toList();

    return Table.fromTextArray(
      headers: headers,
      data: data,
      border: null,
      headerStyle: TextStyle(fontWeight: FontWeight.bold),
      headerDecoration: BoxDecoration(color: PdfColors.grey300),
      cellHeight: 30,
      cellAlignments: {
        0: Alignment.centerLeft,
        1: Alignment.centerRight,
        2: Alignment.centerRight,
        3: Alignment.centerRight,
        4: Alignment.centerRight,
      },
    );
  }

  static Widget buildTotal(Invoice invoice, FlutterCart cart) {
    // final netTotal = 1000;
    // final vatPercent = 500;
    // final vat = netTotal * vatPercent;
    // final total = netTotal + vat;
    print("(//////////////////)");
    print(cart.getCartItemCount());
    print("(//////////////////)");

    return Container(
      alignment: Alignment.centerRight,
      child: Row(
        children: [
          Spacer(flex: 6),
          Expanded(
            flex: 4,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                buildText(
                  title: 'Total Net',
                  value: cart.getTotalAmount().toString() + " FCFA",
                  unite: true,
                ),
                buildText(
                  title: 'Livraison',
                  value: invoice.info.livraison.toString() + " FCFA",
                  unite: true,
                ),
                Divider(),
                buildText(
                  title: 'Total ',
                  titleStyle: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.bold,
                  ),
                  value: (cart.getTotalAmount() + 1000).toString() + " FCFA",
                  unite: true,
                ),
                SizedBox(height: 2 * PdfPageFormat.mm),
                Container(height: 1, color: PdfColors.grey400),
                SizedBox(height: 0.5 * PdfPageFormat.mm),
                Container(height: 1, color: PdfColors.grey400),
              ],
            ),
          ),
        ],
      ),
    );
  }

  static Widget buildFooter(Invoice invoice) => Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Divider(),
          SizedBox(height: 2 * PdfPageFormat.mm),
          buildSimpleText(
              title: 'RAPIDOS SARL ,',
              value: 'Nouvelle route 7eme, BP 6087 Douala-Cameroun'),
          SizedBox(height: 1 * PdfPageFormat.mm),
          buildSimpleText(title: 'N.I.U:', value: "M0721163009837"),
          SizedBox(height: 1 * PdfPageFormat.mm),
          buildSimpleText(
              title: 'Telephone: ', value: "(+237) 671 204 626 / 695 400 777"),
        ],
      );

  static buildSimpleText({
    String title,
    String value,
  }) {
    final style = TextStyle(fontWeight: FontWeight.bold);

    return Row(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: pw.CrossAxisAlignment.end,
      children: [
        Text(title, style: style),
        SizedBox(width: 2 * PdfPageFormat.mm),
        Text(value),
      ],
    );
  }

  static buildText({
    String title,
    String value,
    double width = double.infinity,
    TextStyle titleStyle,
    bool unite = false,
  }) {
    final style = titleStyle ??
        TextStyle(
            fontWeight: FontWeight.bold, wordSpacing: 0.5, letterSpacing: 0.5);

    return Container(
      width: width,
      child: Row(
        children: [
          Expanded(child: Text(title, style: style)),
          Text(value, style: unite ? style : null),
        ],
      ),
    );
  }
}
