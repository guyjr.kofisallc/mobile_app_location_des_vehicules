import 'package:flutter/material.dart';
import 'package:flutter_cart/flutter_cart.dart';
import 'package:flutter_imagenetwork/flutter_imagenetwork.dart';
import 'package:rapidos_mobile/services/avoirToutProduit.dart';
import 'package:rapidos_mobile/services/currentUser.dart';
import 'package:rapidos_mobile/services/pubService.dart';

import '../constants.dart';
import '../entities/restaurants.dart';
import 'detail_resto.dart';

class Carousell_statut extends StatelessWidget {
  Carousell_statut({
    // this.cart,
    Key key,
  }) : super(key: key);
  // FlutterCart cart;
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return FutureBuilder(
        future: PubService().affichepub(),
        builder: (context, snapshot1) {
          if (!snapshot1.hasData) {
            return Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Center(
                          child: Image.asset(
                        "assets/images/donut.gif",
                        width: 100,
                        height: 100,
                      )),
                    ],
                  ),
                ],
              ),
            );
          } else if (snapshot1.hasError) {
            return Column(
              children: [
                Center(
                    child: Image.asset(
                  "assets/images/erreur.png",
                  width: 50,
                  height: 50,
                )),
                Text("Probleme de Serveur veillez ressayer plus tard"),
              ],
            );
          } else if (snapshot1.data == "600") {
            return Column(
              children: [
                Center(
                  child: Image.asset(
                    "assets/images/erreur.png",
                    width: 50,
                    height: 50,
                  ),
                ),
                Text("pas de connexion a internet"),
              ],
            );
          } else if (snapshot1.data == "500") {
            return Column(
              children: [
                Center(
                    child: Image.asset(
                  "assets/images/erreur.png",
                  width: 50,
                  height: 50,
                )),
                Text("Probleme de Serveur veillez ressayer plus tard"),
              ],
            );
          } else {
            // debut affichage
            int taille = snapshot1.data.length;
            print(taille);
            return FutureBuilder(
                future:
                    AfficheProduit(snapshot1.data[taille - 1].pub.toString())
                        .AfficheToutProduit(),
                builder: (context, snapshot) {
                  if (!snapshot.hasData) {
                    return Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Column(
                            children: [
                              Center(
                                  child: Image.asset(
                                "assets/images/donut.gif",
                                width: 100,
                                height: 100,
                              )),
                            ],
                          ),
                        ],
                      ),
                    );
                  } else if (snapshot.hasError) {
                    return Column(
                      children: [
                        Center(
                            child: Image.asset(
                          "assets/images/erreur.png",
                          width: 50,
                          height: 50,
                        )),
                        Text("Probleme de Serveur veillez ressayer plus tard"),
                      ],
                    );
                  } else if (snapshot.data == "600") {
                    return Column(
                      children: [
                        Center(
                          child: Image.asset(
                            "assets/images/erreur.png",
                            width: 50,
                            height: 50,
                          ),
                        ),
                        Text("pas de connexion a internet"),
                      ],
                    );
                  } else if (snapshot.data == "500") {
                    return Column(
                      children: [
                        Center(
                            child: Image.asset(
                          "assets/images/erreur.png",
                          width: 50,
                          height: 50,
                        )),
                        Text("Probleme de Serveur veillez ressayer plus tard"),
                      ],
                    );
                  } else {
                    return Container(
                      height: 80,
                      child: ListView.builder(
                          scrollDirection: Axis.horizontal,
                          // shrinkWrap: true,
                          // physics: NeverScrollableScrollPhysics(),
                          itemCount: snapshot.data.length > 4 ? 6 : 3,
                          itemBuilder: (context, index) {
                            return Single_photo_statut(
                              image: snapshot.data[index].image,
                              text: snapshot.data[index].designation,
                              restId: snapshot.data[index].createdBy.toString(),
                              idResto: snapshot.data[index].createdBy,
                              // cart: cart,
                            );
                          }),
                    );
                  }
                });
          }
        });
  }
}

class Single_photo_statut extends StatelessWidget {
  Single_photo_statut({
    this.image,
    this.text,
    this.restId,
    this.cart,
    this.idResto,
    Key key,
  }) : super(key: key);

  String text;
  String image, restId;
  FlutterCart cart;
  int idResto;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () async {
        var jwt = await CurrentUser().findRestoByid(idResto.toString());
        Restaurant log = new Restaurant();

        if (jwt.runtimeType == log.runtimeType) {
          Navigator.of(context).push(
              new MaterialPageRoute(builder: (context) => DetailResto(jwt)));

          return null;
        } else if (jwt == "600") {
          displayDialog(context, "Pas de connexion internet");

          return null;
        } else {
          displayDialog(context,
              "Désolé une erreur au niveau du serveur Nos ingénieurs travaillent dessus");
          return null;
        }
      },
      child: Stack(
        children: <Widget>[
          Column(children: <Widget>[
            Padding(padding: const EdgeInsets.only(left: 90)),
            Container(
              height: 65,
              width: 65,
              child: ClipRRect(
                //un peu comme un conteneur mais avec border radius
                borderRadius: BorderRadius.circular(50.0),
                child: AjanuwImage(
                  image: AjanuwNetworkImage(image),
                  fit: BoxFit.cover,
                  width: 350,
                  height: 200,
                  loadingWidget: Image.asset(
                    "assets/images/cloche.png",
                    width: 50,
                    height: 50,
                    // color: Colors.red,
                  ),

                  // loadingBuilder: AjanuwImage.defaultLoadingBuilder,
                  errorBuilder: immageErreurChargement,
                ),
              ),
            ),
          ]),
        ],
      ),
    );
  }

  void displayDialog(BuildContext context, String s) {}
}
