import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:rapidos_mobile/entities/currentUserData.dart';

import '../constants.dart';

class BodyEditerProfil extends StatelessWidget {
  BodyEditerProfil(this.currentUser);
  CurentUserData currentUser;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return SingleChildScrollView(
      child: Container(
          width: size.width - (kDefaultPadding * 2),
          //color: Colors.amber,
          padding: EdgeInsets.only(top: 0),
          margin:
              EdgeInsets.only(left: kDefaultPadding, right: kDefaultPadding),
          child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                ),
                SvgPicture.asset(
                  "assets/images/profil.svg",
                  width: size.width * 0.8,
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                ),
                SingleInfo(
                  hintText: currentUser.name,
                  title: "nom",
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                ),
                SingleInfo(
                  hintText: currentUser.username,
                  title: "nom d'utilisateur",
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                ),
                SingleInfo(
                  hintText: currentUser.email,
                  title: "Email",
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                ),
                SingleInfo(
                  hintText: currentUser.phone,
                  title: "Numero ",
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                ),
                PSingleInfo(
                  hintText: "Password",
                  title: "Mot de passe ",
                ),
                Padding(
                  padding: EdgeInsets.only(top: kDefaultPadding),
                ),
                SizedBox(
                  width: size.width / 2,
                  height: 50,
                  child: TextButton(
                    style: boutonRouge,
                    onPressed: () {
                      // Navigator.of(context).push(new MaterialPageRoute(
                      //     builder: (context) => CommandeFinal()));
                    },
                    child: Text(
                      "Valider",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 16,
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: kDefaultPadding),
                ),
              ])),
    );
  }
}

class SingleInfo extends StatelessWidget {
  SingleInfo({
    Key key,
    this.hintText,
    this.title,
  }) : super(key: key);
  final String hintText, title;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: kDefaultPadding, right: kDefaultPadding),
      child: Row(
        children: [
          Container(
            width: 55,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Text(
                  "$title :",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12),
                ),
              ],
            ),
          ),
          RoundedInputField(
            hintText: "$hintText",
            onChange: (value) {},
          ),
        ],
      ),
    );
  }
}

class PSingleInfo extends StatelessWidget {
  PSingleInfo({
    Key key,
    this.hintText,
    this.title,
  }) : super(key: key);
  final String hintText, title;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: kDefaultPadding, right: kDefaultPadding),
      child: Row(
        children: [
          Container(
            width: 55,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Text(
                  "$title :",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12),
                ),
              ],
            ),
          ),
          PRoundedInputField(
            hintText: "$hintText",
            onChange: (value) {},
          ),
        ],
      ),
    );
  }
}

class RoundedInputField extends StatelessWidget {
  final String hintText;
  final ValueChanged<String> onChange;
  const RoundedInputField({
    Key key,
    this.hintText,
    this.onChange,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFieldContainer(
      child: TextField(
        onChanged: onChange,
        decoration: InputDecoration(
          //icon: Icon(icon),
          hintText: hintText,

          border: UnderlineInputBorder(),
        ),
      ),
    );
  }
}

class PRoundedInputField extends StatelessWidget {
  final String hintText;
  final ValueChanged<String> onChange;
  const PRoundedInputField({
    Key key,
    this.hintText,
    this.onChange,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFieldContainer(
      child: TextField(
        obscureText: true,
        onChanged: onChange,
        decoration: InputDecoration(
          hintText: hintText,
          suffixIcon: Icon(
            Icons.visibility,
            color: kPrimaryColor,
          ),
          border: UnderlineInputBorder(),
        ),
      ),
    );
  }
}

class TextFieldContainer extends StatelessWidget {
  final Widget child;
  const TextFieldContainer({
    Key key,
    this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
      //padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
      width: size.width * 0.60,
      decoration: BoxDecoration(
          //borderRadius: BorderRadius.circular(29),
          ),
      child: child,
    );
  }
}
