import 'package:flutter/material.dart';
import 'package:flutter_imagenetwork/flutter_imagenetwork.dart';
import 'package:rapidos_mobile/services/currentUser.dart';
import 'package:rapidos_mobile/services/notificationServices.dart';

import '../constants.dart';
import 'detail_resto.dart';

class NotificationPage extends StatefulWidget {
  const NotificationPage({Key key}) : super(key: key);

  @override
  State<NotificationPage> createState() => _NotificationState();
}

class _NotificationState extends State<NotificationPage> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
        // drawer: ClientDrawer(),
        appBar: buildAppBar(),
        body: SingleChildScrollView(
          child: Container(
            color: Colors.white,
            child: FutureBuilder(
              future: NotificationServices().afficheNotification(),
              builder: (context, snapshot) {
                if (!snapshot.hasData) {
                  return Container(
                    // height: size.height,
                    color: Colors.white,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Center(
                            child: Image.asset(
                          "assets/images/donut.gif",
                          width: 150,
                          height: 150,
                        )),
                      ],
                    ),
                  );
                } else if (snapshot.hasError) {
                  return Column(
                    children: [
                      Center(
                          child: Image.asset(
                        "assets/images/erreur.png",
                        width: 100,
                        height: 100,
                      )),
                      Text("Probleme de Serveur veillez ressayer plus tard"),
                    ],
                  );
                } else if (snapshot.data == "600") {
                  return Column(
                    children: [
                      Center(
                        child: Image.asset(
                          "assets/images/erreur.png",
                          width: 100,
                          height: 100,
                        ),
                      ),
                      Text("pas de connexion a internet"),
                    ],
                  );
                } else if (snapshot.data == "500") {
                  return Column(
                    children: [
                      Center(
                          child: Image.asset(
                        "assets/images/erreur.png",
                        width: 100,
                        height: 100,
                      )),
                      Text("Probleme de Serveur veillez ressayer plus tard"),
                    ],
                  );
                } else {
                  return Container(
                    padding: EdgeInsets.only(
                        top: kDefaultPadding, bottom: kDefaultPadding),
                    child: ListView.separated(
                      separatorBuilder: (_, m) => Divider(),
                      scrollDirection: Axis.vertical,
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      itemCount: snapshot.data.length,
                      itemBuilder: (context, index) {
                        if (snapshot.data.length == 0) {
                          return Container(
                            child: Center(
                              child: Center(
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Container(
                                      // color: Colors.amber,
                                      padding: EdgeInsets.all(8),
                                      child: Image.asset(
                                        'assets/images/panierVide.png',
                                        width: size.width * 0.8,
                                        height: size.width * 0.8,
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                    Text(
                                      "Vous n'avez pas de notification!!!",
                                      style: TextStyle(color: Colors.red),
                                      textAlign: TextAlign.center,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          );
                        } else {
                          return Container(
                              child: Column(
                            children: [
                              FutureBuilder(
                                  future: CurrentUser().findRestoByid(snapshot
                                      .data[index].createdBy
                                      .toString()),
                                  builder: (context, snapshot2) {
                                    if (!snapshot2.hasData) {
                                      return Container(
                                        // height: size.height,
                                        color: Colors.white,
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            Center(
                                                child: Image.asset(
                                              "assets/images/donut.gif",
                                              width: 50,
                                              height: 50,
                                            )),
                                          ],
                                        ),
                                      );
                                    } else if (snapshot.hasError) {
                                      return Column(
                                        children: [
                                          Center(
                                              child: Image.asset(
                                            "assets/images/erreur.png",
                                            width: 100,
                                            height: 100,
                                          )),
                                          Text(
                                              "Probleme de Serveur veillez ressayer plus tard"),
                                        ],
                                      );
                                    } else if (snapshot2.data == "600") {
                                      return Column(
                                        children: [
                                          Center(
                                            child: Image.asset(
                                              "assets/images/erreur.png",
                                              width: 100,
                                              height: 100,
                                            ),
                                          ),
                                          Text("pas de connexion a internet"),
                                        ],
                                      );
                                    } else if (snapshot2.data == "500") {
                                      return Column(
                                        children: [
                                          Center(
                                              child: Image.asset(
                                            "assets/images/erreur.png",
                                            width: 100,
                                            height: 100,
                                          )),
                                          Text(
                                              "Probleme de Serveur veillez ressayer plus tard"),
                                        ],
                                      );
                                    } else {
                                      DateTime d =
                                          DateTime.fromMillisecondsSinceEpoch(
                                              snapshot2.data.createdAt.toInt() *
                                                  1000);
                                      return InkWell(
                                        onTap: (() {
                                          if (snapshot2.data.username ==
                                              "rapidos") {
                                          } else {
                                            Navigator.of(context).push(
                                                new MaterialPageRoute(
                                                    builder: (context) =>
                                                        DetailResto(
                                                            snapshot2.data
                                                            // cart
                                                            )));
                                          }
                                        }),
                                        child: Container(
                                          padding: EdgeInsets.symmetric(
                                              horizontal: kDefaultPadding),
                                          child: Column(children: [
                                            Row(
                                              children: [
                                                CircleAvatar(
                                                  backgroundImage: NetworkImage(
                                                    snapshot2.data.photo,
                                                  ),
                                                  radius: 25,
                                                ),
                                                Padding(
                                                    padding: EdgeInsets.only(
                                                        left: 10)),
                                                Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    Text(
                                                      snapshot2.data.name,
                                                      style: TextStyle(
                                                          fontWeight:
                                                              FontWeight.bold),
                                                    ),
                                                    Text(
                                                        d.day.toString() +
                                                            "-" +
                                                            d.month.toString() +
                                                            "-" +
                                                            d.year.toString(),
                                                        style: TextStyle(
                                                            fontWeight:
                                                                FontWeight
                                                                    .normal))
                                                  ],
                                                )
                                              ],
                                            ),
                                            Padding(
                                                padding:
                                                    EdgeInsets.only(top: 10)),
                                          ]),
                                        ),
                                      );
                                    }
                                  }),
                              Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: kDefaultPadding),
                                child: Container(
                                  // height: size.height * 0.3,
                                  child: Text(
                                    snapshot.data[index].notification,
                                    style: TextStyle(color: Colors.black),
                                    textAlign: TextAlign.justify,
                                  ),
                                ),
                              ),
                              Padding(padding: EdgeInsets.only(top: 10)),
                              Container(
                                child: AjanuwImage(
                                  image: AjanuwNetworkImage(
                                      snapshot.data[index].image),
                                  fit: BoxFit.fill,
                                  width: size.width,
                                  height: size.height * 0.5,
                                  loadingWidget: Image.asset(
                                    "assets/images/restoAwait.png",
                                    width: 50,
                                    height: 50,
                                    // color: Colors.red,
                                  ),
                                  // loadingBuilder: AjanuwImage.defaultLoadingBuilder,
                                  errorBuilder: immageErreurChargement,
                                ),
                              ),
                            ],
                          ));
                        }
                      },
                    ),
                  );
                }
              },
            ),

            // bottomNavigationBar: Footer_all(),
          ),
        ));
  }

  AppBar buildAppBar() {
    return AppBar(
      centerTitle: true,
      iconTheme: IconThemeData(color: Colors.black),
      title: Text(
        "Notification",
        style: TextStyle(
          color: Colors.black,
          fontWeight: FontWeight.bold,
        ),
      ),
      // leading: Icon(
      //   Icons.fastfood,
      //   size: 30,
      // ),
    );
  }
}
