import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:location/location.dart';
import 'package:rapidos_mobile/constants.dart';
import 'package:rapidos_mobile/services/currentUser.dart';
// import 'package:geolocator/geolocator.Dart';
import 'Detail_du_compte.dart';
import 'connexion.dart';

final storage = FlutterSecureStorage();
Future<String> get jwtOrEmpty async {
  var jwt = await storage.read(key: "jwt");
  if (jwt == null) return "";
  return jwt;
}

class BodyMonProfil extends StatefulWidget {
  BodyMonProfil();

  @override
  _BodyMonProfilState createState() => _BodyMonProfilState();
}

class _BodyMonProfilState extends State<BodyMonProfil> {
  Future<LocationData> currentLocation;
  Location location;

  @override
  void initState() {
    super.initState();

    location = new Location();
    currentLocation = location.getLocation();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return FutureBuilder(
        future: CurrentUser().infoClient(),
        builder: (context, snapshot) {
          if (!snapshot.hasData)
            return Center(
              child: Center(
                  child: Image.asset(
                "assets/images/donut.gif",
                width: 150,
                height: 150,
              )),
            );
          else {
            var str = snapshot.data;

            return SingleChildScrollView(
              child: Container(
                padding: EdgeInsets.only(top: kDefaultPadding),
                color: Colors.grey.shade100,
                width: size.width,
                child: Column(
                  //mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      child: Column(
                        children: [
                          Stack(
                            children: [
                              Container(
                                child: ClipRRect(
                                  //un peu comme un conteneur mais avec border radius
                                  borderRadius: BorderRadius.circular(150.0),
                                  child: Image.asset(
                                    "assets/images/user.png",
                                    fit: BoxFit.cover,
                                    width: 150,
                                    height: 150,
                                  ),
                                ),
                              ),
                              Positioned(
                                  top: 100,
                                  left: 100,
                                  child: Container(
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(25),
                                        color: kPrimaryColor,
                                      ),
                                      child: IconButton(
                                        onPressed: () {},
                                        icon: Icon(
                                          Icons.camera_alt_rounded,
                                          color: Colors.white,
                                        ),
                                      )))
                            ],
                          ),
                          Padding(padding: EdgeInsets.only(top: 20)),
                          Text(
                            str.username.toString(),
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 25,
                                letterSpacing: 0.8),
                          )
                        ],
                      ),
                    ),
                    Padding(padding: EdgeInsets.only(top: 40)),
                    SingleConfiguration(
                      size: size,
                      icon: Icons.person,
                      text: "Détails du compte",
                      ontap: () {
                        Navigator.of(context).push(new MaterialPageRoute(
                            builder: (context) => DetailCompte()));
                      },
                    ),
                    Padding(padding: EdgeInsets.only(top: 25)),
                    SingleConfiguration(
                      size: size,
                      icon: Icons.settings,
                      text: "Paramètres",
                    ),
                    Padding(padding: EdgeInsets.only(top: 25)),
                    SingleConfiguration(
                      size: size,
                      icon: Icons.phone,
                      text: "Me Contacter ",
                    ),
                    Padding(padding: EdgeInsets.only(top: 25)),
                    SizedBox(
                      width: size.width / 2,
                      height: 50,
                      child: TextButton(
                        style: boutonRouge,
                        onPressed: () {
                          storage.delete(key: "jwt");
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => Connexion()));
                        },
                        child: Text(
                          "Déconnexion",
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 16,
                          ),
                        ),
                      ),
                    ),
                    Padding(padding: EdgeInsets.only(top: 25)),
                  ],
                ),
              ),
            );
          }
        });
  }
}

class SingleConfiguration extends StatelessWidget {
  SingleConfiguration({
    this.icon,
    this.text,
    this.ontap,
    Key key,
    @required this.size,
  }) : super(key: key);

  final Size size;
  IconData icon;
  String text;
  Function ontap;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: ontap,
      child: Container(
        //color: Colors.grey.shade200,
        height: 50,
        decoration: BoxDecoration(
            border: Border(
          bottom: BorderSide(color: Colors.black12),
        )),
        padding: EdgeInsets.only(left: kDefaultPadding, right: kDefaultPadding),
        child: Column(
          children: [
            Row(
              children: [
                Container(
                  alignment: Alignment.topLeft,
                  width: (size.width / 4),
                  child: Icon(
                    icon,
                    color: kPrimaryColor,
                    size: 30,
                  ),
                ),
                //Spacer(),
                Container(
                  child: Text(
                    text,
                    style: TextStyle(
                      fontWeight: FontWeight.normal,
                      color: Colors.black,
                      letterSpacing: 0.5,
                      fontSize: 15,
                    ),
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
