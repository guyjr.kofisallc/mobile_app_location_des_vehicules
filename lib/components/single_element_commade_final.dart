import 'package:flutter/material.dart';

import '../constants.dart';

class SingleElement extends StatelessWidget {
  SingleElement({
    @required this.size,
    this.sousTitre,
    this.titre,
  });
  String titre;
  String sousTitre;

  final Size size;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: size.height / 10,
      decoration: BoxDecoration(
          // border: Border(
          //   bottom: BorderSide(color: Colors.black12),
          // ),
          ),
      padding: EdgeInsets.only(
        right: kDefaultPadding,
        left: kDefaultPadding,
      ),
      child: Row(
        children: <Widget>[
          Container(
              width: (size.width / 2) - kDefaultPadding,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Text(
                    "$titre",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 25,
                        color: kPrimaryColor),
                  ),
                ],
              )),
          Container(
              width: (size.width / 2) - kDefaultPadding,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Text(
                    "$sousTitre",
                    style:
                        TextStyle(fontWeight: FontWeight.normal, fontSize: 15),
                  ),
                ],
              ))
        ],
      ),
    );
  }
}
