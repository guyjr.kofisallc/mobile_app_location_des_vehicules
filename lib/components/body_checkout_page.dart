import 'package:flutter/material.dart';
import 'package:rapidos_mobile/components/title_with_more_btn.dart';
import 'package:rapidos_mobile/constants.dart';

class BodyCheckout extends StatelessWidget {
  BodyCheckout();

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return SingleChildScrollView(
      child: Container(
        child: Column(children: <Widget>[
          Padding(padding: EdgeInsets.only(top: 45)),
          TitleWithMoreBtn(title: 'Resumé de livraison'),
          Padding(padding: EdgeInsets.only(top: 10)),
          SingleElement(
            size: size,
            titre: "Payement",
            sousTitre: "Mobile Money",
          ),
          SingleElement(
            size: size,
            titre: "Livrer à l'adresse",
            sousTitre: "Dcshang côté Chefférie",
          ),
          SingleElement(
            size: size,
            titre: "Total Facture",
            sousTitre: "3200 XAF",
          ),
          Padding(padding: EdgeInsets.only(top: 20)),
          SizedBox(
            width: size.width / 2,
            height: 50,
            child: TextButton(
              style: boutonRouge,
              onPressed: () {
                // Navigator.of(context).push(new MaterialPageRoute(
                //     builder: (context) => CommandeFinal()));
              },
              child: Text(
                "Je Valide",
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 16,
                ),
              ),
            ),
          ),
        ]),
      ),
    );
  }
}

class SingleElement extends StatelessWidget {
  SingleElement({
    @required this.size,
    this.sousTitre,
    this.titre,
  });
  String titre, sousTitre;

  final Size size;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: size.height / 10,
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(color: Colors.black12),
        ),
      ),
      padding: EdgeInsets.only(
        right: kDefaultPadding,
        left: kDefaultPadding,
      ),
      child: Row(
        children: <Widget>[
          Container(
              width: (size.width / 2) - kDefaultPadding,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Text(
                    "$titre",
                    style: TextStyle(
                        fontFamily: 'DayRoman',
                        fontWeight: FontWeight.bold,
                        fontSize: 20,
                        color: kPrimaryColor),
                  ),
                ],
              )),
          Container(
              width: (size.width / 2) - kDefaultPadding,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Text(
                    "$sousTitre",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 13,
                      fontFamily: 'DayRoman',
                    ),
                  ),
                ],
              ))
        ],
      ),
    );
  }
}
