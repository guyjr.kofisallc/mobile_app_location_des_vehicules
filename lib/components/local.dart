// import 'package:flutter/material.dart';
// // import 'package:geocoder/geocoder.dart';
// import 'package:geolocator/geolocator.dart';

// import '../constants.dart';

// // class Local extends StatelessWidget {
// //   const Local({Key key}) : super(key: key);

// //   void _getLocation() async {
// //     // GeolocationStatus geolocationStatus = await
// //     // Geolocator.checkGeolocationPermissionStatus();

// //     Position position = await Geolocator.getCurrentPosition(
// //         desiredAccuracy: LocationAccuracy.high);
// //     debugPrint('location: ${position.latitude}');

// //     final coordinates = new Coordinates(position.latitude, position.longitude);
// //     debugPrint('coordinates is: $coordinates');

// //     var addresses =
// //         await Geocoder.local.findAddressesFromCoordinates(coordinates);
// //     var first = addresses.first;
// //     // print number of retured addresses
// //     debugPrint('${addresses.length}');
// //     // print the best address
// //     debugPrint("${first.featureName} : ${first.addressLine}");
// //     //print other address names
// //     // debugPrint(Country:${first.countryName} AdminArea:${first.adminArea} SubAdminArea:${first.subAdminArea}");
// //     // //print more address names
// //     // debugPrint(Locality:${first.locality}: Sublocality:${first.subLocality}");
// //   }

// @override
// Widget build(BuildContext context) {
//   Size size = MediaQuery.of(context).size;
//   // _getLocation();
//   return Wrap(
//     children: [
//       Center(
//         child: Container(
//           padding: EdgeInsets.all(kDefaultPadding),
//           child: Column(
//             children: [
//               Text(
//                 "Retrait Orange / MTN Money",
//               ),
//               Padding(padding: EdgeInsets.all(kDefaultPadding * 0.5)),
//               TextField(
//                 obscureText: false,
//                 decoration: InputDecoration(
//                   labelText: 'Numero de telephone',
//                 ),
//               ),
//               Padding(padding: EdgeInsets.all(kDefaultPadding * 0.5)),
//               Row(
//                 children: [
//                   Container(
//                     width: size.width * 0.4,
//                     child: TextField(
//                       obscureText: false,
//                       cursorColor: kPrimaryColor,
//                       decoration: InputDecoration(
//                         labelText: 'Numero de telephone',
//                       ),
//                     ),
//                   ),
//                   Spacer(),
//                   Container(
//                     width: size.width * 0.4,
//                     child: TextField(
//                       obscureText: true,
//                       decoration: InputDecoration(
//                         labelText: 'Mots de passe ',
//                       ),
//                     ),
//                   ),
//                 ],
//               ),
//               Padding(padding: EdgeInsets.all(kDefaultPadding)),
//               Container(
//                 decoration: BoxDecoration(
//                   borderRadius: BorderRadius.circular(20),
//                   boxShadow: [
//                     BoxShadow(
//                       offset: Offset(0, 5),
//                       blurRadius: 5,
//                       color: Colors.grey.withOpacity(0.8),
//                     )
//                   ],
//                 ),
//                 child: SizedBox(
//                   width: size.width * 0.75,
//                   height: 50,
//                   child: FlatButton(
//                     highlightColor: Colors.grey,
//                     hoverColor: Colors.blue,
//                     shape: RoundedRectangleBorder(
//                       borderRadius: BorderRadius.circular(20),
//                     ),
//                     color: kPrimaryColor,
//                     onPressed: () async {},
//                     child: Text(
//                       "Poursuivre",
//                       style: TextStyle(
//                           color: Colors.white,
//                           fontSize: 16,
//                           fontFamily: 'DayRoman',
//                           fontWeight: FontWeight.bold),
//                     ),
//                   ),
//                 ),
//               ),
//             ],
//           ),
//         ),
//       )
//     ],
//   );
// }
// // }
