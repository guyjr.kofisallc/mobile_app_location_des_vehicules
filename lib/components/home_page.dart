import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:rapidos_mobile/components/notification.dart';
import '../constants.dart';
import 'Acceuil_client.dart';
import 'acceuil.dart';
import 'drawer_client.dart';

final storage = FlutterSecureStorage();
Future<String> get jwtOrEmpty async {
  var jwt = await storage.read(key: "jwt");
  if (jwt == null) return "";
  return jwt;
}

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return FutureBuilder(
        future: jwtOrEmpty,
        builder: (context, snapshot) {
          if (!snapshot.hasData)
            return Container(
              height: size.height,
              color: Colors.white,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Center(
                    child: Center(
                        child: Image.asset(
                      "assets/images/donut.gif",
                      width: 150,
                      height: 150,
                    )),
                  ),
                ],
              ),
            );
          if (snapshot.data != "") {
            var str = snapshot.data;
            var jwt = str.split(".");

            if (jwt.length != 3) {
              return Acceuil();
            } else {
              var payload = json.decode(
                  ascii.decode(base64.decode(base64.normalize(jwt[1]))));
              if (DateTime.fromMillisecondsSinceEpoch(payload["exp"] * 1000)
                  .isAfter(DateTime.now())) {
                return WillPopScope(
                  onWillPop: () async => exit(0),
                  child: Scaffold(
                    backgroundColor: Colors.white,
                    drawer: (ClientDrawer()),
                    appBar: buildAppBar(context),
                    body: Body1Client(),
                    // bottomNavigationBar: Footer_all(),
                  ),
                );

                // return test2(str, payload);

              } else {
                return Acceuil();
              }
            }
          } else {
            return Acceuil();
          }
        });
    //       Scaffold(
    //   drawer: (ClientDrawer()),
    //   appBar: buildAppBar(context),
    //   body: Body1_client(),
    //   bottomNavigationBar: Footer_all(),
    // );
  }

  AppBar buildAppBar(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return AppBar(
      backgroundColor: kPrimaryColor,
      // elevation: 0.5,
      centerTitle: true,
      // leading: Builder(
      //   builder: (BuildContext context) {
      //     return IconButton(
      //       icon: Image.asset(
      //         "assets/images/burger.gif",
      //         width: 30,
      //         height: 30,
      //         // color: Colors.red,
      //       ),
      //       onPressed: () {
      //         Scaffold.of(context).openDrawer();
      //       },
      //       tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
      //     );
      //   },
      // ),
      title: Container(
        padding: EdgeInsets.all(6),
        child: ClipRRect(
          //un peu comme un conteneur mais avec border radius
          borderRadius: BorderRadius.circular(9.0),
          child: Image.asset(
            "assets/images/rapidoseat.png",
            color: Colors.white,
            width: size.width * 0.4,
            height: size.width * 0.09,
            fit: BoxFit.cover,
          ),
        ),
      ),
    );
  }
}
