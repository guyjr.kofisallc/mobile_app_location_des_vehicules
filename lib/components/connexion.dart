import 'package:flutter/material.dart';
import 'package:rapidos_mobile/components/acceuil.dart';
import '../constants.dart';
import 'body_connexion.dart';

class Connexion extends StatelessWidget {
  Connexion();
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => Navigator.of(context)
          .push(new MaterialPageRoute(builder: (context) => Acceuil())),
      child: Scaffold(
        appBar: buildAppBar(context),
        // backgroundColor: Colors.grey.shade100,
        body: BodyConnexion(),
        //bottomNavigationBar: Footer_all(),
      ),
    );
  }

  AppBar buildAppBar(BuildContext context) {
    return AppBar(
      centerTitle: true,
      iconTheme: IconThemeData(color: Colors.black),
      title: Text(
        "Connexion",
        style: TextStyle(
          fontWeight: FontWeight.normal,
          color: Colors.black,
          fontFamily: font,
          fontSize: 18,
        ),
      ),
      // leading: IconButton(
      //     color: kPrimaryColor,
      //     onPressed: () {
      //       Navigator.of(context)
      //           .push(new MaterialPageRoute(builder: (context) => Acceuil()));
      //     },
      //     icon: Icon(
      //       Icons.arrow_back_ios_sharp,
      //       size: 30,
      //     )),
    );
  }
}
