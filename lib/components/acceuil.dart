import 'dart:io';

import 'package:flutter/material.dart';
import 'body_acceuil.dart';

class Acceuil extends StatelessWidget {
  Acceuil();
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => exit(0),
      child: Scaffold(
        backgroundColor: Colors.grey.shade100,
        body: BodyAcceuil(),
        floatingActionButton: Text(
          "Version 2.2.0",
          style: TextStyle(color: Colors.black, letterSpacing: 1),
        ),
      ),
    );
  }
}
