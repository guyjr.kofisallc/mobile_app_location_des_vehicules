import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter_cart/flutter_cart.dart';
import 'package:rapidos_mobile/components/home_page.dart';
import 'package:rapidos_mobile/constants.dart';

class BodyCart2 extends StatefulWidget {
  BodyCart2(this.cart);
  FlutterCart cart;
  @override
  _BodyCartState createState() => _BodyCartState(cart);
}

class _BodyCartState extends State<BodyCart2> {
  _BodyCartState(this.cart);
  FlutterCart cart;
  int total;

  double prixUnite = 550;
  int quantite = 1, index;
  double totalQte = 8500.0 * 5;

  void augmenteQte(int index) {
    setState(() {
      cart.cartItem[index].quantity = cart.cartItem[index].quantity + 1;
      cart.cartItem[index].subTotal =
          cart.cartItem[index].unitPrice * cart.cartItem[index].quantity;
    });
  }

  void diminueQte(int index) {
    if (quantite <= 2) {
      quantite = 2;
    }
    setState(() {
      cart.cartItem[index].quantity = quantite - 1;
      cart.cartItem[index].subTotal =
          cart.cartItem[index].unitPrice * cart.cartItem[index].quantity;
    });
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          Padding(padding: const EdgeInsets.only(top: 15)),
          Container(
            height: size.height * 0.6,
            child: ListView.builder(
                itemCount: cart.cartItem.length,
                itemBuilder: (context, index) {
                  totalQte = cart.cartItem[index].unitPrice;
                  return Dismissible(
                    key: Key("2"),
                    background: Container(
                      padding: EdgeInsets.symmetric(horizontal: 15),
                      decoration: BoxDecoration(
                        color: Colors.amber,
                        borderRadius: BorderRadius.circular(15),
                      ),
                      child: Row(
                        children: [Spacer(), Icon(Icons.delete)],
                      ),
                    ),
                    child: Container(
                      color: Colors.grey.shade200,
                      padding: EdgeInsets.only(bottom: kDefaultPadding - 10),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          ListTile(
                            contentPadding: EdgeInsets.all(kDefaultPadding),
                            leading: Container(
                              width: 95,
                              child: ClipRRect(
                                  //un peu comme un conteneur mais avec border radius
                                  borderRadius: BorderRadius.circular(9.0),
                                  child: Image.network(
                                    cart.cartItem[index].productDetails,
                                    fit: BoxFit.cover,
                                  )),
                            ),
                            title: Text(
                              cart.cartItem[index].productName.toString(),
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold),
                            ),
                            minVerticalPadding: 0,
                            subtitle: Row(
                              children: <Widget>[
                                Padding(
                                    padding: const EdgeInsets.only(top: 25)),
                                IconButton(
                                  onPressed: () {
                                    diminueQte(index);
                                  },
                                  icon: Icon(
                                    Icons.remove_circle_outline,
                                    size: 20,
                                    color: Colors.grey.shade600,
                                  ),
                                ),
                                Text(
                                  cart.cartItem[index].quantity.toString(),
                                  style: TextStyle(
                                      color: kPrimaryColor, fontSize: 20),
                                ),
                                IconButton(
                                  onPressed: () {
                                    augmenteQte(index);
                                  },
                                  icon: Icon(
                                    Icons.add_circle_outline_rounded,
                                    size: 20,
                                    color: Colors.grey.shade600,
                                  ),
                                )
                              ],
                            ),
                            trailing: Text(
                              cart.cartItem[index].subTotal.toString() + "XAF",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: kPrimaryColor),
                            ),
                          ),
                          // Row(
                          //   crossAxisAlignment: CrossAxisAlignment.start,
                          //   children: [
                          //     Padding(
                          //         padding: EdgeInsets.only(
                          //       left: kDefaultPadding,
                          //     )),
                          //     SizedBox(
                          //       width: size.width / 4,
                          //       height: 25,
                          //       child: FlatButton(
                          //         shape: RoundedRectangleBorder(
                          //           borderRadius: BorderRadius.circular(20),
                          //         ),
                          //         color: kPrimaryColor,
                          //         onPressed: () {
                          //           // Navigator.of(context).push(new MaterialPageRoute(
                          //           //     builder: (context) => AdresseLivraison()));
                          //         },
                          //         child: Text(
                          //           "Supprimer",
                          //           style: TextStyle(
                          //             color: Colors.white,
                          //             fontSize: 10,
                          //           ),
                          //         ),
                          //       ),
                          //     ),
                          //   ],
                          // ),
                          Padding(padding: const EdgeInsets.only(top: 15)),
                        ],
                      ),
                    ),
                  );
                }),
          ),
          Padding(padding: const EdgeInsets.only(top: 15)),
          Container(
            decoration: BoxDecoration(
                border: Border(
              top: BorderSide(
                color: Colors.black,
              ),
            )),
            padding: const EdgeInsets.only(top: 15),
            child: Row(
              children: <Widget>[
                Container(
                  padding: const EdgeInsets.only(left: kDefaultPadding),
                  width: size.width / 2,
                  child: Text(
                    "TOTAL : ",
                    style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 25),
                  ),
                ),
                //Padding(padding: const EdgeInsets.only(left: kDefaultPadding)),
                Container(
                    width: size.width / 2,
                    padding: const EdgeInsets.only(right: kDefaultPadding),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      verticalDirection: VerticalDirection.down,
                      children: [
                        Text(
                          cart.getTotalAmount().toString(),
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: kPrimaryColor),
                        ),
                      ],
                    ))
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 55),
          ),
          Row(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: kDefaultPadding),
              ),
              SizedBox(
                width: size.width / 2,
                height: 50,
                child: TextButton(
                  style: boutonRouge,
                  onPressed: () {
                    // Navigator.of(context).push(new MaterialPageRoute(
                    //     builder: (context) => AdresseLivraison()));
                  },
                  child: Text(
                    "Payer maintenant",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                    ),
                  ),
                ),
              ),
              Expanded(
                  child: TextButton(
                onPressed: () {
                  Navigator.of(context).push(
                      new MaterialPageRoute(builder: (context) => HomePage()));
                },
                child: Text("Annuler"),
              ))
            ],
          ),
        ],
      ),
    );
  }
}
