import 'package:flutter/material.dart';
import 'package:rapidos_mobile/services/currentUser.dart';

import '../constants.dart';
import 'editer_profil.dart';

class BodyDetailCompte extends StatelessWidget {
  BodyDetailCompte();

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return FutureBuilder(
        future: CurrentUser().infoClient(),
        builder: (context, snapshot) {
          if (!snapshot.hasData)
            return Container(
              height: size.height,
              child: Center(
                  child: Image.asset(
                "assets/images/donut.gif",
                width: 150,
                height: 150,
              )),
            );
          else {
            var str = snapshot.data;
            print(str);

            return SingleChildScrollView(
              child: Container(
                  padding: EdgeInsets.only(
                      top: kDefaultPadding, bottom: kDefaultPadding),
                  color: Colors.grey.shade100,
                  width: size.width,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(left: kDefaultPadding),
                        child: Text(
                          "INFORMATIONS DE CONNEXION",
                          style: TextStyle(
                              fontWeight: FontWeight.w100, fontSize: 15),
                        ),
                      ),
                      Padding(padding: EdgeInsets.only(top: 25)),
                      Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                              offset: Offset(0, 5),
                              blurRadius: 5,
                              color: Colors.grey.withOpacity(0.8),
                            )
                          ],
                        ),
                        padding: EdgeInsets.all(kDefaultPadding),
                        width: size.width,
                        child: Column(
                          children: [
                            Container(
                              decoration: BoxDecoration(
                                  border: Border(
                                      bottom: BorderSide(
                                          color: Colors.grey.shade100))),
                              height: 50,
                              child: Row(
                                children: [
                                  Container(
                                    width:
                                        (size.width - kDefaultPadding * 2) / 2,
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        Text(
                                          "Nom d'utilisateur",
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 15,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Container(
                                    width:
                                        (size.width - kDefaultPadding * 2) / 2,
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: [
                                        Text(str.username.toString(),
                                            style: TextStyle(fontSize: 13)),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),
                            //Padding(padding: EdgeInsets.only(top: 25)),
                            Container(
                              height: 50,
                              child: Row(
                                children: [
                                  Container(
                                    width:
                                        (size.width - kDefaultPadding * 2) / 2,
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        Text(
                                          "nom",
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 15,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Container(
                                    width:
                                        (size.width - kDefaultPadding * 2) / 2,
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: [
                                        Text(
                                          str.name.toString(),
                                          style: TextStyle(fontSize: 13),
                                        )
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                      Padding(padding: EdgeInsets.only(top: 25)),
                      Padding(
                        padding: const EdgeInsets.only(left: kDefaultPadding),
                        child: Text(
                          "INFORMATIONS PRIVEES",
                          style: TextStyle(
                              fontWeight: FontWeight.w100, fontSize: 15),
                        ),
                      ),
                      Padding(padding: EdgeInsets.only(top: 25)),
                      Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                              offset: Offset(0, 5),
                              blurRadius: 5,
                              color: Colors.grey.withOpacity(0.8),
                            )
                          ],
                        ),
                        padding: EdgeInsets.all(kDefaultPadding),
                        width: size.width,
                        child: Column(
                          children: [
                            Container(
                              decoration: BoxDecoration(
                                  border: Border(
                                      bottom: BorderSide(
                                          color: Colors.grey.shade100))),
                              height: 50,
                              child: Row(
                                children: [
                                  Container(
                                    width: (size.width - kDefaultPadding * 2) *
                                        0.3,
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        Text(
                                          "Email",
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 15,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Container(
                                    width: (size.width - kDefaultPadding * 2) *
                                        0.7,
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: [
                                        Text(
                                          str.email.toString(),
                                          style: TextStyle(fontSize: 13),
                                        ),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),
                            //Padding(padding: EdgeInsets.only(top: 25)),
                            Container(
                              height: 50,
                              child: Row(
                                children: [
                                  Container(
                                    width:
                                        (size.width - kDefaultPadding * 2) / 2,
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        Text(
                                          "Numéro de \n téléphone",
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 15,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Container(
                                    width:
                                        (size.width - kDefaultPadding * 2) / 2,
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: [
                                        Text(
                                          str.phone.toString(),
                                          style: TextStyle(fontSize: 13),
                                        ),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                      Padding(padding: EdgeInsets.only(top: 30)),
                      Center(
                        child: Row(
                          children: <Widget>[
                            Padding(
                              padding:
                                  const EdgeInsets.only(left: kDefaultPadding),
                            ),
                            SizedBox(
                              width: size.width / 2,
                              height: 50,
                              child: TextButton(
                                style: boutonRouge,
                                onPressed: () {
                                  // Navigator.of(context).push(new MaterialPageRoute(
                                  //     builder: (context) => AdresseLivraison())
                                  //  );
                                },
                                child: Text(
                                  "Enrégistrer",
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 16,
                                  ),
                                ),
                              ),
                            ),
                            Expanded(
                                child: TextButton(
                              onPressed: () {
                                Navigator.of(context).push(
                                    new MaterialPageRoute(
                                        builder: (context) =>
                                            EditerProfil(snapshot.data)));
                              },
                              child: Text("Editer profil"),
                            ))
                          ],
                        ),
                      ),
                    ],
                  )),
            );
          }
        });
  }
}
