import 'package:flutter/material.dart';
import 'package:flutter_imagenetwork/flutter_imagenetwork.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:rapidos_mobile/components/body_detail_resto.dart';
import 'package:rapidos_mobile/components/title_with_more_btn.dart';
import 'package:rapidos_mobile/constants.dart';
import 'package:rapidos_mobile/entities/allProduit.dart';

import '../entities/restaurants.dart';
import 'your_Cart.dart';

class BodySingleProduct extends StatefulWidget {
  BodySingleProduct(
      this.produit,
      //  this.cart,
      this.nbreProduit,
      this.totalQte,
      this.resto);
  // FlutterCart cart;
  Produit produit;
  int nbreProduit;
  double totalQte;
  Restaurant resto;

  @override
  _BodySingleProductState createState() => _BodySingleProductState(
      produit,
      // cart,
      nbreProduit,
      totalQte);
}

class _BodySingleProductState extends State<BodySingleProduct> {
  _BodySingleProductState(
      this.produit,
      //  this.cart,
      this.nbreProduit,
      this.totalQte);
  // FlutterCart cart;
  final storage = FlutterSecureStorage();
  Future<String> qteP;
  Produit produit;
  int nbreProduit;
  BodyDetailResto qtePagedetail;
  // String nom = "Burger Nature";
  double prixUnite;
  double quantite = 1;
  double totalQte;

  // void augmenteQtep() {
  //   setState(() {
  //     nbreProduit = nbreProduit + 1;
  //     storage.write(key: "qteP", value: "$nbreProduit");
  //   });
  // }

  void augmenteQte() {
    setState(() {
      quantite = quantite + 1;
      totalQte = prixUnite * quantite;
    });
  }

  void diminueQte() {
    if (quantite <= 2) {
      quantite = 2;
    }
    setState(() {
      quantite = quantite - 1;
      totalQte = prixUnite * quantite;
    });
  }

  @override
  Widget build(BuildContext context) {
    prixUnite = produit.prixUnitaire.toDouble();
    // totalQte = produit.prixUnitaire.toDouble();
    qteP = storage.read(key: "qteP");
    String message = produit.designation;
    Size size = MediaQuery.of(context).size;
    return SingleChildScrollView(
      child: Container(
        child: Column(
          children: <Widget>[
            Stack(
              children: <Widget>[
                Positioned(
                  child: Stack(children: <Widget>[
                    AjanuwImage(
                      image: AjanuwNetworkImage(produit.image),
                      width: size.width,
                      height: size.height / 2,
                      fit: BoxFit.cover,
                      loadingWidget: Container(
                        height: size.height / 2,
                        child: Center(
                          child: Image.asset(
                            "assets/images/cloche.png",
                            width: 100,
                            height: 100,
                            // color: Colors.red,
                          ),
                        ),
                      ),
                      // loadingBuilder: AjanuwImage.defaultLoadingBuilder,
                      errorBuilder: AjanuwImage.defaultErrorBuilder,
                    ),
                  ]),
                ),
                Stack(children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(top: (size.height / 2) - 60),
                    padding: EdgeInsets.only(top: kDefaultPadding),
                    width: size.width,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(60),
                          topRight: Radius.circular(60)),
                      color: Colors.grey.shade300,
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Column(
                          children: <Widget>[
                            Padding(padding: const EdgeInsets.only(top: 25)),
                            TitleWithMoreBtn(title: produit.designation),
                            Row(
                              children: <Widget>[
                                Container(
                                  padding: const EdgeInsets.only(
                                      left: kDefaultPadding),
                                  width: (size.width / 2) - kDefaultPadding,
                                  child: Text(
                                    "-------------------------",
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 15),
                                  ),
                                ),
                                //Padding(padding: const EdgeInsets.only(left: kDefaultPadding)),
                                Container(
                                    width: (size.width / 2) - kDefaultPadding,
                                    padding: const EdgeInsets.only(
                                        right: kDefaultPadding),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.end,
                                      verticalDirection: VerticalDirection.down,
                                      children: [
                                        Text(
                                          "$prixUnite XAF",
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: Colors.black),
                                        ),
                                      ],
                                    )),
                              ],
                            ),
                            // Padding(padding: const EdgeInsets.only(top: 5)),
                            ListTile(
                              title: Text(
                                "Details sur le plat",
                                style: TextStyle(
                                    fontSize: 15, fontWeight: FontWeight.bold),
                              ),
                              subtitle: Text(produit.details),
                            ),
                            Text("------------------------------------"),
                            Row(
                              children: <Widget>[
                                Container(
                                  padding: const EdgeInsets.only(
                                      left: kDefaultPadding),
                                  width: (size.width / 2) - kDefaultPadding,
                                  child: Text(
                                    "TOTAL : ",
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 20),
                                  ),
                                ),
                                Container(
                                    width: (size.width / 2) - kDefaultPadding,
                                    padding: const EdgeInsets.only(
                                        right: kDefaultPadding),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.end,
                                      verticalDirection: VerticalDirection.down,
                                      children: [
                                        Text(
                                          "$totalQte XAF",
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: kPrimaryColor),
                                        ),
                                      ],
                                    )),
                              ],
                            ),
                            Padding(padding: const EdgeInsets.only(top: 15)),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Padding(
                                    padding: const EdgeInsets.only(top: 25)),
                                IconButton(
                                  onPressed: diminueQte,
                                  icon: Icon(
                                    Icons.remove_circle_outline,
                                    size: 20,
                                    color: Colors.grey.shade600,
                                  ),
                                ),
                                Text(
                                  "$quantite",
                                  style: TextStyle(
                                      color: kPrimaryColor, fontSize: 20),
                                ),
                                IconButton(
                                  onPressed: augmenteQte,
                                  icon: Icon(
                                    Icons.add_circle_outline_rounded,
                                    size: 20,
                                    color: Colors.grey.shade600,
                                  ),
                                ),
                                Container(
                                  padding: EdgeInsets.all(5),
                                  height: 50,
                                  width: size.width * 0.45,
                                  //width: size.width / 4,
                                  decoration: BoxDecoration(
                                      color: kPrimaryColor,
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(55))),
                                  child: TextButton(
                                    onPressed: () {
                                      final snackBar = SnackBar(
                                          content: Text(message.toString() +
                                              " a ete ajouter au panier"),
                                          backgroundColor: kPrimaryColor,
                                          elevation: 55,
                                          duration: Duration(seconds: 2));
                                      ScaffoldMessenger.of(context)
                                          .showSnackBar(snackBar);
                                      cart.addToCart(
                                          productId: produit.produitId,
                                          unitPrice: produit.prixUnitaire,
                                          productName: produit.designation,
                                          quantity: quantite.toInt(),
                                          productDetailsObject: produit.image);
                                    },
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        // Icon(
                                        //   Icons.alarm,
                                        //   color: Colors.white,
                                        // ),
                                        Text(
                                          " Ajouter a mon Panier",
                                          style: TextStyle(
                                              fontSize: 11,
                                              fontFamily: 'DayRoman',
                                              color: Colors.white,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ],
                                    ),
                                  ),
                                )
                              ],
                            ),
                            Padding(padding: const EdgeInsets.only(bottom: 25)),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Positioned(
                      top: (size.height / 2) - 75,
                      right: kDefaultPadding,
                      child: InkWell(
                        onTap: () {
                          Navigator.of(context).push(new MaterialPageRoute(
                              builder: (context) => YouCart(widget.resto)));
                        },
                        child: Container(
                          padding: EdgeInsets.all(4),
                          //width: size.width / 4,
                          decoration: BoxDecoration(
                              color: kPrimaryColor,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(55))),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              IconButton(
                                  onPressed: () {},
                                  icon: Icon(
                                    Icons.shopping_cart_outlined,
                                    color: Colors.white,
                                  )),
                            ],
                          ),
                        ),
                      ))
                ])
              ],
            )
          ],
        ),
      ),
    );
  }
}

// class SingleProduitResto extends StatelessWidget {
//   const SingleProduitResto({
//     this.image,
//     Key key,
//   }) : super(key: key);
//   final String image;

//   @override
//   Widget build(BuildContext context) {
//     Size size = MediaQuery.of(context).size;
//     return InkWell(
//       onTap: () {
//         Navigator.of(context).push(
//             new MaterialPageRoute(builder: (context) => SingleProduct(image)));
//       },
//       child: Container(
//         decoration: BoxDecoration(
//             // color: Colors.red,
//             border: Border(
//           top: BorderSide(color: Colors.grey.shade200),
//         )),
//         padding: EdgeInsets.only(bottom: kDefaultPadding - 15),
//         child: Column(
//           mainAxisAlignment: MainAxisAlignment.spaceBetween,
//           children: [
//             ListTile(
//               contentPadding: EdgeInsets.all(kDefaultPadding),
//               leading: Container(
//                 width: 75,
//                 child: ClipRRect(
//                   //un peu comme un conteneur mais avec border radius
//                   borderRadius: BorderRadius.circular(9.0),

//                   child: Image.network(
//                     image,
//                     fit: BoxFit.cover,
//                   ),
//                 ),
//               ),
//               title: Text("Burger King",
//                   style: TextStyle(
//                     color: Colors.black,
//                     fontWeight: FontWeight.bold,
//                     fontSize: 20,
//                   )),
//               minVerticalPadding: 0,
//               subtitle: Column(
//                 crossAxisAlignment: CrossAxisAlignment.start,
//                 children: <Widget>[
//                   Text(
//                     "675 XAF",
//                     style: TextStyle(color: Colors.red, fontSize: 15),
//                   ),
//                   RatingBar.builder(
//                     itemSize: 10.0,
//                     initialRating: 2,
//                     minRating: 1,
//                     direction: Axis.horizontal,
//                     allowHalfRating: true,
//                     itemCount: 5,
//                     itemBuilder: (context, _) => Icon(
//                       Icons.star,
//                       color: Colors.red,
//                     ),
//                     onRatingUpdate: (rating) {
//                       print(rating);
//                     },
//                   ),
//                 ],
//               ),
//               trailing: SizedBox(
//                 width: size.width / 8,
//                 height: 30,
//                 child: FlatButton(
//                   shape: RoundedRectangleBorder(
//                     borderRadius: BorderRadius.circular(5),
//                   ),
//                   color: kPrimaryColor,
//                   onPressed: () {
//                     // Navigator.of(context).push(new MaterialPageRoute(
//                     //     builder: (context) => AdresseLivraison()));
//                   },
//                   child: Icon(
//                     Icons.shopping_cart_outlined,
//                     color: Colors.white,
//                     size: 20,
//                   ),
//                 ),
//               ),
//             ),
//             Row(
//               crossAxisAlignment: CrossAxisAlignment.start,
//               children: [
//                 Padding(
//                     padding: EdgeInsets.only(
//                   left: kDefaultPadding,
//                 )),
//               ],
//             ),
//           ],
//         ),
//       ),
//     );
//   }
// }
