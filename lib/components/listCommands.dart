import 'package:flutter/material.dart';

import '../constants.dart';
import 'bodyListCommand.dart';

class ListCommande extends StatelessWidget {
  const ListCommande({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(context),
      body: BodyListCommande(),
    );
  }

  AppBar buildAppBar(BuildContext context) {
    return AppBar(
      iconTheme: IconThemeData(color: Colors.black),
      centerTitle: true,
      title: Text(
        "Liste des commandes ",
        style: TextStyle(
            fontWeight: FontWeight.bold,
            fontFamily: font,
            fontSize: 15,
            color: Colors.black),
      ),
      actions: [
        Center(
          child: Icon(
            Icons.search,
            color: kPrimaryColor,
          ),
        ),
        Padding(padding: EdgeInsets.all(5))
      ],
    );
  }
}
