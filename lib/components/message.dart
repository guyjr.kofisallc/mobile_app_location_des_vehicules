import 'package:flutter/material.dart';
import 'package:rapidos_mobile/constants.dart';

displayDialogMessage(BuildContext context, String title, String text) {
  Size size = MediaQuery.of(context).size;
  showDialog(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
          child: Container(
            constraints: BoxConstraints(maxHeight: 200),
            child: Padding(
              padding: const EdgeInsets.all(kDefaultPadding),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      InkWell(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: CircleAvatar(
                          child: Icon(
                            Icons.close,
                            color: Colors.white,
                            size: 14,
                          ),
                          backgroundColor: kPrimaryColor,
                          radius: 13,
                        ),
                      ),
                    ],
                  ),
                  Text(
                    "Erreur",
                    style: TextStyle(
                      fontFamily: font,
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      fontStyle: FontStyle.normal,
                    ),
                    textAlign: TextAlign.left,
                  ),
                  Padding(padding: EdgeInsets.all(5)),
                  Text(
                    text,
                    style: TextStyle(
                      fontFamily: font,
                      fontSize: 13,
                      // fontWeight: FontWeight.bold,
                      fontStyle: FontStyle.normal,
                    ),
                    textAlign: TextAlign.left,
                  ),
                  Padding(padding: EdgeInsets.all(5)),
                ],
              ),
            ),
          ),
        );
      });
}
