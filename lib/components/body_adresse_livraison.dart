import 'dart:math';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:geocoder2/geocoder2.dart';
import 'package:rapidos_mobile/components/mapSearch.dart';
import 'package:rapidos_mobile/constants.dart';
import 'package:rapidos_mobile/entities/Panier.dart';
import 'dart:collection';
import 'dart:async';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:rapidos_mobile/components/message.dart';
import 'package:rapidos_mobile/entities/restaurants.dart';
import 'package:search_map_location/utils/google_search/geo_location.dart';
import 'package:search_map_location/utils/google_search/place.dart';
import '../entities/CommandeSend.dart';
import 'facturePage.dart';
import 'dart:ui' as ui;

// ignore: must_be_immutable
class BodyAdresseLivraison extends StatefulWidget {
  BodyAdresseLivraison(this.restaurant);
  Restaurant restaurant;

  @override
  _BodyAdresseLivraisonState createState() => _BodyAdresseLivraisonState();
}

class _BodyAdresseLivraisonState extends State<BodyAdresseLivraison> {
  _BodyAdresseLivraisonState();

  String adresse = "A ma position actuelle";
  PositionCart positionCart = new PositionCart();
  List<String> countries;
  List<Widget> load = [
    Text(
      "Payer",
      style: TextStyle(
          color: Colors.white,
          fontSize: 14,
          fontFamily: 'DayRoman',
          fontWeight: FontWeight.bold),
    ),
    Center(
      child: CircularProgressIndicator(
        color: Colors.white,
        strokeWidth: 2,
      ),
    ),
  ];
  int choix = 0;
  void change() {
    setState(() {
      choix = 1;
    });
  }

  // static const LatLng sourceLocation = LatLng(4.0580037, 9.7260679);

  List<LatLng> polyLineCoordonate = [];
  LocationData currentLocation;
  Completer<GoogleMapController> _controller = Completer();
  LatLng actue = new LatLng(latActuel, lonActuel);
  LatLng actue2 = LatLng(4.0580037, 9.7260679);
  double calculateDistance(lat1, lon1, lat2, lon2) {
    var p = 0.017453292519943295;
    var c = cos;
    var a = 0.5 -
        c((lat2 - lat1) * p) / 2 +
        c(lat1 * p) * c(lat2 * p) * (1 - c((lon2 - lon1) * p)) / 2;
    return 12742 * asin(sqrt(a));
  }

  // Future<LocationData> getCurentLocation() {
  //   Location location = Location();

  //   return location.getLocation().then((location) async {
  //     actue = LatLng(location.latitude, location.longitude);
  //     setState(() {
  //       actue = LatLng(location.latitude, location.longitude);
  //     });

  //     _goToTheLake2(LatLng(location.latitude, location.longitude));
  //     return location;
  //   });
  // }

  // Future<void> getPolyPoints() async {
  //   PolylinePoints polyLinePoints = PolylinePoints();
  //   PolylineResult result = await polyLinePoints.getRouteBetweenCoordinates(
  //       google_api_key,
  //       PointLatLng(sourceLocation.latitude, sourceLocation.longitude),
  //       PointLatLng(sourceLocation.latitude, sourceLocation.longitude));
  //   if (result.points.isNotEmpty) {
  //     result.points.forEach((PointLatLng point) => polyLineCoordonate.add(
  //           LatLng(point.latitude, point.longitude),
  //         ));
  //     setState(() {});
  //   }
  // }

  // Future<void> getChangePlace(Place place) async {
  //   // Geocoding geocoding = new Geocoding(apiKey: google_api_key, language: "fr");

  //   Geolocation geolocation = await place.geolocation;
  //   // Convert to proper package's LatLng
  //   LatLng latLng = LatLng(
  //       geolocation.coordinates.latitude, geolocation.coordinates.longitude);
  //   final bounds = LatLngBounds(southwest: latLng, northeast: latLng);
  //   // Geolocation result = await geocoding.getGeolocation(place);
  //   setState(() {
  //     actue = latLng;
  //   });
  //   // print(result.coordinates);
  // }

  CameraPosition _kGooglePlex(LatLng latLng) {
    return CameraPosition(
      target: LatLng(latActuel, lonActuel),
      zoom: 17.4746,
      bearing: 10,
    );
  }

  Future<void> position() async {
    GeoData data = await Geocoder2.getDataFromCoordinates(
        latitude: latActuel,
        longitude: lonActuel,
        googleMapApiKey: google_api_key);
    print(data.address);
    // String local =
    //     //  addresse.elementAt(0).locality.toString() +
    //     addresse.elementAt(0).addressLine.toString();

    setState(() {
      adresse = data.address;
    });
  }

  Future<void> _goToTheLake(Place place) async {
    Geolocation geolocation = await place.geolocation;
    CameraPosition _kLake = CameraPosition(
        target: LatLng(geolocation.coordinates.latitude,
            geolocation.coordinates.longitude),
        zoom: 17.4);
    final GoogleMapController controller = await _controller.future;
    addMarkers();
    setState(() {
      // marker(actue);
      // marker1.add(Marker(
      //   markerId: MarkerId("current Location"),
      //   position: LatLng(geolocation.coordinates.latitude,
      //       geolocation.coordinates.longitude),
      // ));
      setState(() {
        actue = LatLng(geolocation.coordinates.latitude,
            geolocation.coordinates.longitude);
      });

      actue2 = LatLng(
          geolocation.coordinates.latitude, geolocation.coordinates.longitude);
    });
    controller.animateCamera(CameraUpdate.newCameraPosition(_kLake));
  }

  Set<Marker> marker1 = HashSet<Marker>();
  Marker marker(LatLng latLng) {
    return Marker(
      markerId: MarkerId("current Location"),
      position: latLng,
    );
  }

  bool bo = false;
  Set<Marker> markers = Set();
  Future<Uint8List> getBytesFromAsset(String path, int width) async {
    ByteData data = await rootBundle.load(path);
    ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List(),
        targetWidth: width);
    ui.FrameInfo fi = await codec.getNextFrame();
    return (await fi.image.toByteData(format: ui.ImageByteFormat.png))
        .buffer
        .asUint8List();
  }

  Future<BitmapDescriptor> getBitmapDescriptorFromAssetBytes(
      String path, int width) async {
    final Uint8List imageData = await getBytesFromAsset(path, width);
    return BitmapDescriptor.fromBytes(imageData);
  }

  addMarkers() async {
    final icon = await getBitmapDescriptorFromAssetBytes(
        "assets/images/marker.png", 150);
    markers.clear();
    markers.add(Marker(
      //add start location marker
      markerId: MarkerId(actue.toString()),

      position: actue, //position of marker

      infoWindow: InfoWindow(
        //popup info
        title: 'Starting Point ',
        snippet: 'Start Marker',
      ),
      icon: icon,
    ));
  }

  @override
  void initState() {
    super.initState();
    position();
    addMarkers();
    setState(() {});
  }

  FlutterCart cart1 = new FlutterCart.fromPlayer(cart: cart);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    // FlutterCart cart1 = new FlutterCart.fromPlayer(cart: cart);
    positionCart.latitude = actue.latitude;
    positionCart.longitude = actue.longitude;

    return Stack(children: [
      GoogleMap(
        initialCameraPosition: _kGooglePlex(actue),

        onMapCreated: (GoogleMapController controller) {
          _controller.complete(controller);
        },
        // polylines: {
        //   Polyline(
        //       polylineId: PolylineId("route"),
        //       points: polyLineCoordonate,
        //       color: Colors.blue,
        //       width: 6)
        // },
        markers: markers, //markers to show on map
        mapType: MapType.normal,
      ),
      Positioned(
          bottom: 0,
          child: Container(
            height: size.height * 0.25,
            width: size.width,
            padding: EdgeInsets.all(size.height * 0.02),
            decoration: BoxDecoration(
              color: Colors.grey,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(30),
                topRight: Radius.circular(30),
              ),
              boxShadow: [
                BoxShadow(
                    color: Colors.grey.shade400,
                    offset: Offset(10, 10),
                    blurRadius: 20,
                    spreadRadius: 9),
              ],
            ),
            child: SingleChildScrollView(
                child: Column(
              children: [
                Text("Ou vous livrer ?"),
                Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: kDefaultPadding, vertical: 10),
                ),
                SearchLocation2(
                  apiKey: google_api_key,
                  placeholder: adresse + ("(Ma position actuel)"),
                  language: 'fr',
                  country: 'CM',
                  radius: 100,
                  hasClearButton: false,
                  onSelected: (Place place) {
                    print(place.description);
                    print(place.description.split(', ').length);
                    setState(() {
                      adresse = place.description;
                    });
                    if (place.description.split(', ').length < 2) {
                      displayDialogMessage(context, "Desole",
                          "Faite un choix plus precis de votre localisation(Quartier) ");
                    } else {
                      // getChangePlace(place);
                      setState(() {
                        bo = true;
                      });
                      _goToTheLake(place);
                    }
                  },
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: kDefaultPadding, vertical: 10),
                ),
                SizedBox(
                  width: size.width / 2,
                  height: 50,
                  child: TextButton(
                    style: boutonRouge,
                    onPressed: () {
                      double distance = calculateDistance(
                          widget.restaurant.position.latitude,
                          widget.restaurant.position.longitude,
                          actue.latitude,
                          actue.longitude);
                      print(distance.toString() + "km");

                      if (distance > 50.0) {
                        displayDialogMessage(context, "Desole",
                            "Votre distance de livraison est de ${distance.toInt()} Km, Nous ne pouvons pas vous livrer a plus de 50 Km. Veillez choisir un resaurant plus proche de chez vous");
                      } else {
                        displayDialogFacture(
                            context,
                            adresse,
                            cart1,
                            widget.restaurant.name,
                            widget.restaurant.userId.toString(),
                            distance,
                            positionCart);
                      }
                    },
                    child: Text(
                      "Valider",
                      style: TextStyle(
                        color: Colors.white,
                        fontFamily: 'DayRoman',
                        fontWeight: FontWeight.bold,
                        fontSize: 12,
                      ),
                    ),
                  ),
                ),
              ],
            )),
            //bottomNavigationBar: Footer_all(),
          ))
    ]);
  }
}

class TextFieldContainer extends StatelessWidget {
  final Widget child;
  const TextFieldContainer({
    Key key,
    this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 0),
      width: size.width * 0.8,
      decoration: BoxDecoration(
        color: Colors.grey.shade300,
        borderRadius: BorderRadius.circular(29),
      ),
      child: child,
    );
  }
}
