import 'package:flutter/material.dart';
import 'package:flutter_cart/flutter_cart.dart';
import 'package:rapidos_mobile/entities/adresse.dart';
import 'body_payement.dart';

class Payement extends StatelessWidget {
  Payement(this._adresseClient, this.cart, this.nom, this.idResto);
  Adresse _adresseClient;
  FlutterCart cart;
  String nom, idResto;
  @override
  Widget build(BuildContext context) {
    print('\n  ' + cart.getTotalAmount().toString() + '\n ' + nom);
    return Scaffold(
      backgroundColor: Colors.grey.shade100,
      appBar: buildAppBar(context),
      body: BodyPayement(_adresseClient, cart, nom, idResto),
      //bottomNavigationBar: Footer_all(),
    );
  }

  AppBar buildAppBar(BuildContext context) {
    return AppBar(
      centerTitle: true,
      title: Text(
        "Payement",
        style: TextStyle(
          color: Colors.black,
          fontWeight: FontWeight.bold,
        ),
      ),
      // leading: IconButton(
      //     color: Colors.red,
      //     onPressed: () {
      //       Navigator.of(context)
      //           .push(new MaterialPageRoute(builder: (context) => HomePage()));
      //     },
      //     icon: Icon(
      //       Icons.arrow_back_ios_sharp,
      //       size: 30,
      //     )),
    );
  }
}
