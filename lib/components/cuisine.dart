import 'package:flutter/material.dart';
import '../constants.dart';
import 'body_cuisine.dart';

class Cuisine extends StatelessWidget {
  Cuisine();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(context),
      body: BodyCuisine(),
      // bottomNavigationBar: Footer_all(),
    );
  }

  AppBar buildAppBar(BuildContext context) {
    return AppBar(
      centerTitle: true,
      iconTheme: themeIcon,
      title: Text(
        "Cuisine",
        style: text3nb,
      ),
      // leading: IconButton(
      //     color: Colors.red,
      //     onPressed: () {
      //       Navigator.of(context)
      //           .push(new MaterialPageRoute(builder: (context) => HomePage()));
      //     },
      //     icon: Icon(
      //       Icons.arrow_back_ios_sharp,
      //       size: 30,
      //     )),
    );
  }
}
