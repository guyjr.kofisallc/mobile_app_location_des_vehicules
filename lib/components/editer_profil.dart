import 'package:flutter/material.dart';
import 'package:rapidos_mobile/entities/currentUserData.dart';
import 'package:rapidos_mobile/services/currentUser.dart';
import 'body_editer_profil.dart';

class EditerProfil extends StatelessWidget {
  EditerProfil(this.currentUser);
  CurentUserData currentUser;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(context),
      body: BodyEditerProfil(currentUser),
      //bottomNavigationBar: Footer_all(),
    );
  }

  AppBar buildAppBar(BuildContext context) {
    return AppBar(
      centerTitle: true,
      title: Text(
        "Editer Profil",
        style: TextStyle(
          color: Colors.black,
          fontWeight: FontWeight.bold,
        ),
      ),
      // leading: IconButton(
      //     color: Colors.red,
      //     onPressed: () {
      //       Navigator.of(context)
      //           .push(new MaterialPageRoute(builder: (context) => YouCart()));
      //     },
      //     icon: Icon(
      //       Icons.arrow_back_ios_sharp,
      //       size: 30,
      //     )),
    );
  }
}
