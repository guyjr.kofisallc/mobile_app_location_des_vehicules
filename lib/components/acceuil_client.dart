import 'package:flutter/material.dart';
import 'package:rapidos_mobile/components/constructionPage.dart';
import 'package:rapidos_mobile/components/detail_acceuil.dart';
import '../constants.dart';
import 'lineAndSpace.dart';

// ignore: camel_case_types
class Body1Client extends StatefulWidget {
  Body1Client();

  @override
  State<Body1Client> createState() => _Body1ClientState();
}

class _Body1ClientState extends State<Body1Client> {
  Color initColor1 = ksecondaryColor;
  Color initColor2 = Colors.white;
  int select = 1;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      height: size.height,
      width: size.width,
      color: kPrimaryColor,
      padding: const EdgeInsets.symmetric(horizontal: kDefaultPadding),
      child: SingleChildScrollView(
          child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
            smallSpace(size.height * 0.07),
            const Text(
              "Hi ! Choisissez",
              style: TextStyle(
                  color: Colors.white,
                  letterSpacing: 0.5,
                  fontSize: 20,
                  wordSpacing: 0.9,
                  fontWeight: FontWeight.bold),
            ),
            Row(
              children: const [
                Text(
                  "Le",
                  style: TextStyle(
                      color: Colors.white,
                      letterSpacing: 0.5,
                      fontSize: 20,
                      wordSpacing: 0.9,
                      fontWeight: FontWeight.bold),
                ),
                Text(
                  " Service que vous desirez.",
                  style: TextStyle(
                      color: ksecondaryColor,
                      letterSpacing: 0.5,
                      fontSize: 20,
                      wordSpacing: 0.9,
                      fontWeight: FontWeight.bold),
                ),
              ],
            ),
            smallSpace(50),
            Container(
              color: Color.fromRGBO(0, 0, 0, 0),
              child: Column(
                children: [
                  Row(
                    children: [
                      InkWell(
                        onTap: (() => Navigator.of(context).push(
                            new MaterialPageRoute(
                                builder: (context) => DetailAcceuil()))),
                        child: Container(
                          padding: const EdgeInsets.symmetric(
                              vertical: 10, horizontal: 10),
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius:
                                BorderRadius.circular(radiusBorder * 2),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.2),
                                spreadRadius: 2,
                                blurRadius: 5,
                                offset: const Offset(
                                    0, 3), // changes position of shadow
                              ),
                            ],
                          ),
                          // height: size.height / 3,
                          width: size.width * 0.4,
                          child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                smallSpace(30),
                                Center(
                                  child: Column(
                                    children: [
                                      Image.asset(
                                        "assets/images/restaurant.png",

                                        height: 100,
                                        fit: BoxFit.fill,
                                        // color: kPrimaryColor,
                                      ),
                                      smallSpace(20),
                                      const Text(
                                        "Restaurant",
                                        style: TextStyle(
                                            color: kPrimaryColor,
                                            letterSpacing: 0.5,
                                            fontSize: 15,
                                            wordSpacing: 0.9,
                                            fontWeight: FontWeight.w600),
                                      ),
                                      // const Text(
                                      //   "Organisation",
                                      //   style: TextStyle(
                                      //       color: Colors.black,
                                      //       letterSpacing: 0.5,
                                      //       fontSize: 13,
                                      //       wordSpacing: 0.9,
                                      //       fontWeight: FontWeight.w600),
                                      // ),
                                    ],
                                  ),
                                ),
                                smallSpace(15),
                              ]),
                        ),
                      ),
                      const Spacer(),
                      InkWell(
                        onTap: (() => displayMessageConstruction(context)),
                        child: Container(
                          padding: const EdgeInsets.symmetric(
                              vertical: 10, horizontal: 10),
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius:
                                BorderRadius.circular(radiusBorder * 2),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.2),
                                spreadRadius: 2,
                                blurRadius: 5,
                                offset: const Offset(
                                    0, 3), // changes position of shadow
                              ),
                            ],
                          ),
                          // height: size.height / 3,
                          width: size.width * 0.4,
                          child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                smallSpace(30),
                                Center(
                                  child: Column(
                                    children: [
                                      Image.asset(
                                        "assets/images/store.png",

                                        height: 100,
                                        fit: BoxFit.fill,
                                        // color: kPrimaryColor,
                                      ),
                                      smallSpace(20),
                                      const Text(
                                        "supermarché",
                                        style: TextStyle(
                                            color: kPrimaryColor,
                                            letterSpacing: 0.5,
                                            fontSize: 15,
                                            wordSpacing: 0.9,
                                            fontWeight: FontWeight.w600),
                                      ),
                                    ],
                                  ),
                                ),
                                smallSpace(15),
                              ]),
                        ),
                      )
                    ],
                  ),
                  smallSpace(15),
                  Row(
                    children: [
                      InkWell(
                        onTap: (() => displayMessageConstruction(context)),
                        child: Container(
                          padding: const EdgeInsets.symmetric(
                              vertical: 10, horizontal: 10),
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius:
                                BorderRadius.circular(radiusBorder * 2),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.2),
                                spreadRadius: 2,
                                blurRadius: 5,
                                offset: const Offset(
                                    0, 3), // changes position of shadow
                              ),
                            ],
                          ),
                          // height: size.height / 3,
                          width: size.width * 0.4,
                          child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                smallSpace(30),
                                Center(
                                  child: Column(
                                    children: [
                                      Image.asset(
                                        "assets/images/vin.png",
                                        height: 100,
                                        fit: BoxFit.fill,
                                        // color: kPrimaryColor,
                                      ),
                                      smallSpace(20),
                                      const Text(
                                        "Cave a vin",
                                        style: TextStyle(
                                            color: kPrimaryColor,
                                            letterSpacing: 0.5,
                                            fontSize: 15,
                                            wordSpacing: 0.9,
                                            fontWeight: FontWeight.w600),
                                      ),
                                    ],
                                  ),
                                ),
                                smallSpace(15),
                              ]),
                        ),
                      ),
                      const Spacer(),
                      // InkWell(
                      //   onTap: (() => changeSelect(2)),
                      //   child: Container(
                      //     padding: const EdgeInsets.symmetric(
                      //         vertical: 10, horizontal: 10),
                      //     decoration: BoxDecoration(
                      //       color: Colors.white,
                      //       borderRadius:
                      //           BorderRadius.circular(radiusBorder * 2),
                      //       boxShadow: [
                      //         BoxShadow(
                      //           color: Colors.grey.withOpacity(0.2),
                      //           spreadRadius: 2,
                      //           blurRadius: 5,
                      //           offset: const Offset(
                      //               0, 3), // changes position of shadow
                      //         ),
                      //       ],
                      //     ),
                      //     // height: size.height / 3,
                      //     width: size.width * 0.4,
                      //     child: Column(
                      //         crossAxisAlignment: CrossAxisAlignment.start,
                      //         children: [
                      //           smallSpace(30),
                      //           Center(
                      //             child: Column(
                      //               children: [
                      //                 Image.asset(
                      //                   "assets/images/glace.png",

                      //                   height: 100,
                      //                   fit: BoxFit.fill,
                      //                   // color: kPrimaryColor,
                      //                 ),
                      //                 smallSpace(20),
                      //                 const Text(
                      //                   "Home",
                      //                   style: TextStyle(
                      //                       color: kPrimaryColor,
                      //                       letterSpacing: 0.5,
                      //                       fontSize: 15,
                      //                       wordSpacing: 0.9,
                      //                       fontWeight: FontWeight.w500),
                      //                 ),
                      //                 const Text(
                      //                   "Personal",
                      //                   style: TextStyle(
                      //                       color: Colors.black,
                      //                       letterSpacing: 0.5,
                      //                       fontSize: 13,
                      //                       wordSpacing: 0.9,
                      //                       fontWeight: FontWeight.w600),
                      //                 ),
                      //               ],
                      //             ),
                      //           ),
                      //           smallSpace(15),
                      //         ]),
                      //   ),
                      // )
                    ],
                  ),
                ],
              ),
            )
          ])),
    );
  }
}
