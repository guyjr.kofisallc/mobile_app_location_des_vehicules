import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:rapidos_mobile/entities/CartItem.dart';
import 'package:rapidos_mobile/services/whatsappServices.dart';
import 'package:splashscreen/splashscreen.dart';
import '../constants.dart';
import '../entities/Panier.dart';
import '../services/currentUser.dart';
import '../services/envoieCartItems.dart';
import '../services/envoieCommande.dart';
import 'body_commande_final.dart';
import 'home_page.dart';

class CommandeFinal extends StatelessWidget {
  CommandeFinal(this._adresseClient, this.cart, this.nomResto, this.payement,
      this.idResto, this.commande, this.numero, this.reference, this.livraison);
  FlutterCart cart;
  String _adresseClient;
  int livraison;
  String nomResto, payement, idResto, numero, reference;
  var commande;
  String decisionPayement;
  CartItemCommand cartItemCommand = new CartItemCommand();
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: EnvoiCommande().sendCommande(commande),
      builder: (context, snapshot) {
        if (!snapshot.hasData) {
          return SplashScreen(
            seconds: 1,
            backgroundColor: kPrimaryColor,
            title: new Text(
              'Bienvenue',
              style: TextStyle(
                  fontSize: 10,
                  color: Colors.white,
                  fontFamily: 'DayRoman',
                  fontWeight: FontWeight.w300),
              textScaleFactor: 2,
            ),

            image: Image.asset("assets/images/log1.jpeg"),
            // loadingText: Text("Loading"),
            navigateAfterSeconds: null,
            photoSize: 100.0,
            loaderColor: Colors.white,
          );
        } else {
          return FutureBuilder(
            future: EnvoiCartitems().sendcartItems(cart, snapshot.data),
            builder: (context, snapshot) {
              if (!snapshot.hasData) {
                return SplashScreen(
                  seconds: 1,
                  navigateAfterSeconds: null,
                  backgroundColor: kPrimaryColor,
                  title: Text(
                    'Veillez valider le payement(TAKWID GROUP) ou composer *126# ou #150#',
                    style: TextStyle(
                        fontSize: 10,
                        color: Colors.white,
                        fontFamily: 'DayRoman',
                        fontWeight: FontWeight.w300),
                    textScaleFactor: 2,
                  ),

                  image: Image.asset("assets/images/log1.jpeg"),
                  // loadingText: Text("Loading"),
                  photoSize: 100.0,
                  loaderColor: Colors.white,
                );
              } else {
                // Navigator.of(context).push(new MaterialPageRoute(
                //     builder: (context) => CommandeInter(_adresseClient, cart,
                //         nomResto, payement, idResto, commande)));
                var id = snapshot.data;
                return CommandeInter(
                    _adresseClient,
                    cart,
                    nomResto,
                    payement,
                    idResto,
                    id.toString(),
                    commande,
                    numero,
                    reference,
                    livraison);
              }
            },
          );
        }
      },
    );
  }
}

class CommandeInter extends StatelessWidget {
  CommandeInter(
      this._adresseClient,
      this.cart,
      this.nomResto,
      this.payement,
      this.idResto,
      this.idCommande,
      this.commande,
      this.numero,
      this.reference,
      this.livraison);
  FlutterCart cart;
  String _adresseClient;
  String nomResto, payement, idResto, idCommande, numero, reference;
  var commande;
  int livraison;
  String decisionPayement;
  CartItemCommand cartItemCommand = new CartItemCommand();
  void user() async {
    var user = await CurrentUser().infoClient();
    String el = "";
    for (var i = 0; i < cart.cartItem.length; i++) {
      el = el +
          cart.cartItem[i].productName.toString() +
          "\n" +
          cart.cartItem[i].quantity.toString() +
          "   *   " +
          cart.cartItem[i].unitPrice.toString() +
          "  =  *" +
          cart.cartItem[i].subTotal.toString() +
          " FCFA*\n" +
          "---\n ";
    }
    String num = numero.split("237")[1];
    int total = cart.getTotalAmount().toInt() + livraison;
    String messageWhat = "*NOUVELLE COMMANDE* \n" +
        "-------------------- \n" +
        "*$nomResto* \n" +
        "-------------------- \n \n" +
        "____________________________________________\n" +
        "*${user.name}*\n" +
        "${user.email}\n" +
        "${user.address}\n" +
        "$payement\n" +
        "$numero\n" +
        "$reference\n" +
        "$_adresseClient\n" +
        "____________________________________________\n \n" +
        "--------------------------------------------\n" +
        "$el" +
        "\n Livraison              *$livraison FCFA*" +
        "\n _________________________________________" +
        "\n *TOTAL* :          *$total FCFA* \n" +
        "-------------------- \n \n";

    WhatsappService().message(messageWhat);
  }

  @override
  Widget build(BuildContext context) {
    user();
    return WillPopScope(
      onWillPop: () async {
        cart.deleteAllCart();
        Navigator.of(context)
            .push(new MaterialPageRoute(builder: (context) => HomePage()));
        return false;
      },
      child: Scaffold(
        appBar: buildAppBar(context, idCommande),
        body: BodyCommandeFinal(_adresseClient, cart, nomResto, payement,
            numero, reference, livraison),
        // bottomNavigationBar: Footer_all(),
      ),
    );
  }

  AppBar buildAppBar(BuildContext context, String id) {
    return AppBar(
      iconTheme: IconThemeData(color: Colors.black),
      centerTitle: true,
      title: Text(
        "Votre Commande",
        style: TextStyle(
            fontWeight: FontWeight.bold,
            fontFamily: font,
            fontSize: 15,
            color: Colors.black),
      ),
      actions: [
        Center(
          child: TextButton(
            onPressed: () {
              dislayAnnulerCommande(context, id);
            },
            child: Text(
              'Annuler',
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontFamily: font,
                  fontSize: 15,
                  color: kPrimaryColor),
            ),
          ),
        ),
        Padding(padding: EdgeInsets.all(5))
      ],
    );
  }
}

dislayAnnulerCommande(BuildContext context, String id) {
  Size size = MediaQuery.of(context).size;
  showDialog(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
          child: Container(
            constraints: BoxConstraints(maxHeight: 350),
            child: Padding(
              padding: EdgeInsets.fromLTRB(
                  kDefaultPadding, kDefaultPadding, kDefaultPadding, 0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      InkWell(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: CircleAvatar(
                          child: Icon(
                            Icons.close,
                            color: Colors.white,
                            size: 14,
                          ),
                          backgroundColor: kPrimaryColor,
                          radius: 13,
                        ),
                      ),
                    ],
                  ),
                  Text(
                    "Etes vous sur de vouloir annuler la commande ?",
                    style: TextStyle(
                      fontFamily: font,
                      fontSize: 15,
                      fontWeight: FontWeight.bold,
                      fontStyle: FontStyle.normal,
                    ),
                    textAlign: TextAlign.left,
                  ),
                  Center(
                    child: Container(
                      // color: Colors.amber,
                      padding: EdgeInsets.all(8),
                      child: SvgPicture.asset(
                        'assets/images/undraw_cancel_re_pkdm.svg',
                        width: 150,
                        height: 150,
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  Spacer(),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      TextButton(
                        onPressed: () {
                          EnvoiCommande().annulerCommande(id);
                          Navigator.pop(context);
                          Navigator.pop(context);
                          Navigator.pop(context);
                          Navigator.pop(context);
                          Navigator.pop(context);
                          Navigator.pop(context);
                        },
                        child: Text(
                          "oui",
                          style: TextStyle(
                            fontFamily: font,
                            fontSize: 13,
                            color: kPrimaryColor,
                            // fontWeight: FontWeight.bold,
                            fontStyle: FontStyle.normal,
                          ),
                          textAlign: TextAlign.left,
                        ),
                      ),
                      TextButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: Text(
                          "non",
                          style: TextStyle(
                            color: kPrimaryColor,
                            fontFamily: font,
                            fontSize: 13,
                            // fontWeight: FontWeight.bold,
                            fontStyle: FontStyle.normal,
                          ),
                          textAlign: TextAlign.left,
                        ),
                      ),
                    ],
                  ),
                  Padding(padding: EdgeInsets.all(5)),
                ],
              ),
            ),
          ),
        );
      });
}
