import 'dart:core';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:rapidos_mobile/components/connexion.dart';
import 'package:rapidos_mobile/services/inscriptionServices.dart';
import '../constants.dart';
import 'package:email_validator/email_validator.dart';

class BodyInscription extends StatefulWidget {
  BodyInscription();

  @override
  _BodyInscriptionState createState() => _BodyInscriptionState();
}

class _BodyInscriptionState extends State<BodyInscription> {
  final _formKey = GlobalKey<FormState>();

  final TextEditingController _nameController = TextEditingController();

  final TextEditingController _usernameController = TextEditingController();

  final TextEditingController _emailController = TextEditingController();

  final TextEditingController _telephoneController = TextEditingController();

  final TextEditingController _adresseController = TextEditingController();

  final TextEditingController _passwordController = TextEditingController();

  final TextEditingController _confirmpasswordController =
      TextEditingController();
  bool visible = true;

  Widget wi = Text(
    "Valider",
    style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
  );

  void changeTextToLoader() {
    setState(() {
      wi = CircularProgressIndicator(
        color: Colors.white,
      );
    });
  }

  void changeLoaderTextTo() {
    setState(() {
      wi = Text(
        "Valider",
        style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
      );
    });
  }

  displayDialog(BuildContext context, String text, String subTitle) {
    Size size = MediaQuery.of(context).size;
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0)),
            child: Container(
              constraints: BoxConstraints(maxHeight: 180),
              child: Padding(
                padding: const EdgeInsets.all(kDefaultPadding),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        InkWell(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: CircleAvatar(
                            child: Icon(
                              Icons.close,
                              color: Colors.white,
                              size: 14,
                            ),
                            backgroundColor:
                                text == "Erreur" ? kPrimaryColor : Colors.green,
                            radius: 13,
                          ),
                        ),
                      ],
                    ),
                    Text(
                      text,
                      style: TextStyle(
                        fontFamily: font,
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        fontStyle: FontStyle.normal,
                      ),
                      textAlign: TextAlign.left,
                    ),
                    Padding(padding: EdgeInsets.all(5)),
                    Text(
                      subTitle,
                      style: TextStyle(
                        fontFamily: font,
                        fontSize: 13,
                        // fontWeight: FontWeight.bold,
                        fontStyle: FontStyle.normal,
                      ),
                      textAlign: TextAlign.left,
                    ),
                    Padding(padding: EdgeInsets.all(5)),
                  ],
                ),
              ),
            ),
          );
        });
  }

  void rendVisble() {
    setState(() {
      if (visible == true) {
        visible = false;
      } else {
        visible = true;
      }
    });
  }

  LocationData currentLocation;
  LatLng actue = new LatLng(4.0580037, 9.7260679);

  Future<LocationData> getCurentLocation() async {
    try {
      Location location = Location();
      var loc = await location.getLocation().then((location) {
        setState(() {
          actue = LatLng(location.latitude, location.longitude);
        });
        return currentLocation = location;
      });
      return loc;
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

  @override
  void initState() {
    super.initState();

    getCurentLocation();
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return SingleChildScrollView(
        child: Container(
            width: size.width - (kDefaultPadding * 2),
            //color: Colors.amber,
            padding: EdgeInsets.only(top: kDefaultPadding),
            margin:
                EdgeInsets.only(left: kDefaultPadding, right: kDefaultPadding),
            child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Container(
                      // margin: EdgeInsets.only(top: kDefaultPadding),
                      child: Column(children: <Widget>[
                    Container(
                      width: size.width,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Crée un compte pour",
                            style: TextStyle(
                              fontFamily: font,
                              fontSize: 15,
                              fontWeight: FontWeight.bold,
                              fontStyle: FontStyle.normal,
                            ),
                            textAlign: TextAlign.left,
                          ),
                          Text(
                            "Passer une commande!",
                            style: TextStyle(
                              fontFamily: font,
                              fontSize: 15,
                              fontWeight: FontWeight.bold,
                              fontStyle: FontStyle.normal,
                            ),
                            textAlign: TextAlign.left,
                          ),
                        ],
                      ),
                    ),
                    Padding(padding: EdgeInsets.all(10)),
                    Form(
                        key: _formKey,
                        child: Column(
                          children: [
                            TextFieldContainer(
                              child: TextFormField(
                                validator: (value) {
                                  if (value == null || value.isEmpty) {
                                    return 'Entrer le nom ';
                                  }
                                  return null;
                                },
                                controller: _nameController,
                                onChanged: (value) {},
                                decoration: InputDecoration(
                                  icon: Icon(Icons.person),
                                  labelText: "Nom",
                                  labelStyle: TextStyle(
                                      fontFamily: 'DayRoman', fontSize: 14),
                                  border: InputBorder.none,
                                ),
                              ),
                            ),
                            TextFieldContainer(
                              child: TextFormField(
                                validator: (value) {
                                  if (value == null ||
                                      value.isEmpty ||
                                      value.split(" ").length > 1) {
                                    return 'Entrer votre username  en un mot';
                                  }
                                  return null;
                                },
                                controller: _usernameController,
                                onChanged: (value) {},
                                keyboardType: TextInputType.name,
                                decoration: InputDecoration(
                                  icon: Icon(Icons.person),
                                  labelText: "Nom d'utilisateur",
                                  labelStyle: TextStyle(
                                      fontFamily: 'DayRoman', fontSize: 14),
                                  border: InputBorder.none,
                                ),
                              ),
                            ),
                            TextFieldContainer(
                              child: TextFormField(
                                validator: (value) {
                                  if (value.length < 5 ||
                                      !value.contains('@') ||
                                      value == null ||
                                      value.isEmpty) {
                                    return 'Entrer adresse mail valide';
                                  }
                                  return null;
                                },
                                controller: _emailController,
                                keyboardType: TextInputType.emailAddress,
                                onChanged: (value) {},
                                decoration: InputDecoration(
                                  icon: Icon(Icons.mail),
                                  labelText: "Adresse mail",
                                  labelStyle: TextStyle(
                                      fontFamily: 'DayRoman', fontSize: 14),
                                  border: InputBorder.none,
                                ),
                              ),
                            ),
                            TextFieldContainer(
                              child: TextFormField(
                                keyboardType: TextInputType.number,
                                validator: (value) {
                                  if (value == null || value.isEmpty) {
                                    return 'Entrer votre Numero de telephone';
                                  }
                                  // else if (value.length != 8) {
                                  //   return 'Entrer un Numero de telephone valide';
                                  // }
                                  return null;
                                },
                                controller: _telephoneController,
                                onChanged: (value) {},
                                decoration: InputDecoration(
                                  icon: Icon(Icons.phone),
                                  labelText: "Numéro de Telehone",
                                  labelStyle: TextStyle(
                                      fontFamily: 'DayRoman', fontSize: 14),
                                  border: InputBorder.none,
                                ),
                              ),
                            ),
                            TextFieldContainer(
                              child: TextFormField(
                                validator: (value) {
                                  if (value == null || value.isEmpty) {
                                    return 'Entrer votre Adresse';
                                  }
                                  return null;
                                },
                                controller: _adresseController,
                                onChanged: (value) {},
                                decoration: InputDecoration(
                                  icon: Icon(Icons.map),
                                  labelText: "Localisation(Ville  )",
                                  labelStyle: TextStyle(
                                      fontFamily: 'DayRoman', fontSize: 14),
                                  border: InputBorder.none,
                                ),
                              ),
                            ),
                            TextFieldContainer(
                              child: TextFormField(
                                validator: (value) {
                                  if (value == null || value.isEmpty) {
                                    return 'Entrer votre mot de passe ';
                                  }
                                  return null;
                                },
                                controller: _passwordController,
                                obscureText: visible,
                                keyboardType: TextInputType.visiblePassword,
                                onChanged: (value) {},
                                decoration: InputDecoration(
                                  icon: Icon(Icons.lock),
                                  labelText: "Mot de passe",
                                  labelStyle: TextStyle(
                                      fontFamily: 'DayRoman', fontSize: 14),
                                  suffixIcon: IconButton(
                                    onPressed: () {
                                      rendVisble();
                                    },
                                    icon: Icon(Icons.visibility),
                                    color: Color.fromRGBO(250, 25, 47, 1),
                                  ),
                                  border: InputBorder.none,
                                ),
                              ),
                            ),
                            TextFieldContainer(
                              child: TextFormField(
                                validator: (value) {
                                  if (value == null || value.isEmpty) {
                                    return 'Entrer votre confirmation de  mot de passe ';
                                  }
                                  return null;
                                },
                                controller: _confirmpasswordController,
                                keyboardType: TextInputType.visiblePassword,
                                obscureText: visible,
                                onChanged: (value) {},
                                decoration: InputDecoration(
                                  icon: Icon(Icons.lock),
                                  labelText: "Confirmation Mot de passe",
                                  labelStyle: TextStyle(
                                      fontFamily: 'DayRoman', fontSize: 14),
                                  suffixIcon: IconButton(
                                    onPressed: () {
                                      rendVisble();
                                    },
                                    icon: Icon(Icons.visibility),
                                    color: Color.fromRGBO(250, 25, 47, 1),
                                  ),
                                  border: InputBorder.none,
                                ),
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 5, vertical: 0),
                            ),
                            Padding(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 5, vertical: kDefaultPadding),
                            ),
                            Container(
                              decoration: BoxDecoration(
                                color: kPrimaryColor,
                                border: Border.all(
                                  width: 3,
                                  color: kPrimaryColor,
                                ),
                                borderRadius: BorderRadius.circular(10.0),
                              ),
                              child: Material(
                                color: kPrimaryColor,
                                borderRadius: BorderRadius.circular(10.0),
                                // button color
                                child: InkWell(
                                  splashColor: Colors
                                      .grey, // Couleur qui "envahit" le bouton lors du focus
                                  child: SizedBox(
                                    width: size.width - (kDefaultPadding * 2),
                                    height: 40,
                                    child: Center(
                                      child: wi,
                                    ),
                                  ),
                                  onTap: () async {
                                    var name = _nameController.text;
                                    var username = _usernameController.text
                                        .toLowerCase()
                                        .replaceAll(" ", "");
                                    var email = _emailController.text
                                        .toLowerCase()
                                        .replaceAll(" ", "");
                                    var telephone = _telephoneController.text;
                                    var password = _passwordController.text;
                                    var adresse = _adresseController.text;
                                    var confirmPass =
                                        _confirmpasswordController.text;
                                    bool isValid =
                                        EmailValidator.validate(email);

                                    // var pos = getCurentLocation();

                                    if (_formKey.currentState.validate()) {
                                      changeTextToLoader();
                                      if (isValid == false)
                                        displayDialog(context, "Erreur",
                                            "Adresse mail invalide, Veillez changer votre adresse mail");
                                      else if (!telephone.startsWith("6", 0) ||
                                          !telephone.startsWith("2", 1) &&
                                              !telephone.startsWith("5", 1) &&
                                              !telephone.startsWith("6", 1) &&
                                              !telephone.startsWith("7", 1) &&
                                              !telephone.startsWith("8", 1) &&
                                              !telephone.startsWith("9", 1)) {
                                        changeLoaderTextTo();
                                        displayDialog(context, "Erreur",
                                            "Numero de telephone invalide Veillez changer votre numero de telephone");
                                      } else if (telephone.length != 9) {
                                        changeLoaderTextTo();
                                        displayDialog(context, "Erreur",
                                            "Numero de telephone invalide Veillez changer votre numero de telephone");
                                      } else if (password.length < 4) {
                                        changeLoaderTextTo();
                                        displayDialog(context, "Erreur",
                                            "Mot de passe doit contenir au moins 4 caracteres");
                                      } else if (password != confirmPass) {
                                        changeLoaderTextTo();
                                        displayDialog(context, "Erreur",
                                            "Erreur de confirmation de mot de passe");
                                      } else {
                                        var res = await InscriptiomServices()
                                            .inscription(
                                                name,
                                                username.toLowerCase(),
                                                email,
                                                telephone,
                                                actue.latitude,
                                                actue.longitude,
                                                password,
                                                adresse);
                                        if (res == 201) {
                                          Navigator.of(context).push(
                                              new MaterialPageRoute(
                                                  builder: (context) =>
                                                      Connexion()));
                                          changeLoaderTextTo();
                                          displayDialog(context, "Success",
                                              "Votre Compte a été crée avec succes ; Maintenant connecté vous");
                                        } else if (res == 409 || res == 400) {
                                          changeLoaderTextTo();
                                          displayDialog(context, "Erreur",
                                              "Ce nom d'utilisateur ou votre email est deja enregistré Essayez de changer votre nom d'utilsateur ou l'adresse mail");
                                        } else {
                                          changeLoaderTextTo();
                                          displayDialog(context, "Erreur",
                                              "Erreur de réseau internet ou de connection au serveur");
                                        }
                                      }
                                    }
                                  },
                                ),
                              ),
                            ),
                          ],
                        ))
                  ])),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 5, vertical: 10),
                  ),
                  Center(
                    child: TextButton(
                      onPressed: () => Navigator.push(context,
                          MaterialPageRoute(builder: (context) => Connexion())),
                      child: Text(
                        "J'ai deja un compte !!!",
                        style: TextStyle(
                            fontFamily: 'DayRoman',
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 5, vertical: 10),
                  ),
                  // Container(
                  //   width: size.width * 0.75,
                  //   height: 50,
                  //   padding: EdgeInsets.only(
                  //       left: kDefaultPadding, right: kDefaultPadding),
                  //   decoration: BoxDecoration(
                  //     borderRadius: BorderRadius.circular(20),
                  //     color: Colors.blue[900],
                  //   ),
                  //   child: Row(
                  //     children: [
                  //       Icon(
                  //         Icons.facebook,
                  //         color: Colors.white,
                  //       ),
                  //       Spacer(),
                  //       FlatButton(
                  //         highlightColor: Colors.blue[1000],
                  //         hoverColor: Colors.blue,
                  //         shape: RoundedRectangleBorder(),
                  //         onPressed: () {
                  //           // Navigator.of(context).push(new MaterialPageRoute(
                  //           //     builder: (context) => Connexion()));
                  //         },
                  //         child: Text(
                  //           "Connexion avec Facebook",
                  //           style: TextStyle(
                  //               color: Colors.white,
                  //               fontSize: 10,
                  //               fontFamily: 'DayRoman',
                  //               fontWeight: FontWeight.bold),
                  //         ),
                  //       ),
                  //     ],
                  //   ),
                  // ),
                  // Center(
                  //   child: Text(
                  //     "ou",
                  //     style: TextStyle(
                  //       fontFamily: 'DayRoman',
                  //     ),
                  //   ),
                  // ),
                  // Padding(
                  //   padding: EdgeInsets.symmetric(horizontal: 5, vertical: 0),
                  // ),
                  // Container(
                  //   width: size.width * 0.75,
                  //   height: 50,
                  //   padding: EdgeInsets.only(
                  //       left: kDefaultPadding, right: kDefaultPadding),
                  //   decoration: BoxDecoration(
                  //     borderRadius: BorderRadius.circular(20),
                  //     color: Colors.grey[200],
                  //   ),
                  //   child: Row(
                  //     children: [
                  //       Icon(
                  //         Icons.g_mobiledata,
                  //         color: Colors.yellow[800],
                  //         size: 35,
                  //       ),
                  //       Spacer(),
                  //       FlatButton(
                  //         highlightColor: Colors.blue[1000],
                  //         hoverColor: Colors.blue,
                  //         shape: RoundedRectangleBorder(),
                  //         onPressed: () {
                  //           // Navigator.of(context).push(new MaterialPageRoute(
                  //           //     builder: (context) => Connexion()));
                  //         },
                  //         child: Text(
                  //           "Connexion avec Google",
                  //           style: TextStyle(
                  //               color: Colors.green[700],
                  //               fontSize: 10,
                  //               fontFamily: 'DayRoman',
                  //               fontWeight: FontWeight.bold),
                  //         ),
                  //       ),
                  //     ],
                  //   ),
                  // ),
                  Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: 5, vertical: kDefaultPadding),
                  ),
                ])));
  }
}

class TextFieldContainer extends StatelessWidget {
  final Widget child;
  const TextFieldContainer({
    Key key,
    this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 2),
      width: size.width - (kDefaultPadding * 2),
      decoration: BoxDecoration(
        border: Border.all(
          width: 2,
          color: Colors.grey,
        ),
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
      ),
      child: child,
    );
  }
}

// class TextFieldContainer extends StatelessWidget {
//   final Widget child;
//   const TextFieldContainer({
//     Key key,
//     this.child,
//   }) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     Size size = MediaQuery.of(context).size;
//     return Container(
//       margin: EdgeInsets.symmetric(vertical: 7),
//       padding: EdgeInsets.symmetric(horizontal: 10, vertical: 0),
//       width: size.width * 0.8,
//       decoration: BoxDecoration(
//           color: Colors.grey.shade100,
//           borderRadius: BorderRadius.circular(29),
//           border: Border.all(color: Colors.grey)),
//       child: child,
//     );
//   }
// }
