import 'dart:collection';
import 'dart:async';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class BodyMap extends StatefulWidget {
  @override
  _BodyMapState createState() => _BodyMapState();
}

class _BodyMapState extends State<BodyMap> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      width: size.width,
      child: ClipRRect(
        //un peu comme un conteneur mais avec border radius
        borderRadius: BorderRadius.circular(9.0),
        child: MapSample(),
        // Image.asset(
        //   "assets/images/map.png",
        //   height: size.height,
        //   width: size.width / 2,
        //   fit: BoxFit.fill,
        // ),
      ),
    );
  }
}

class MapSample extends StatefulWidget {
  @override
  State<MapSample> createState() => MapSampleState();
}

class MapSampleState extends State<MapSample> {
  Completer<GoogleMapController> _controller = Completer();

  static final CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(4.0583749, 9.7272868),
    zoom: 14.4746,
  );

  static final CameraPosition _kLake = CameraPosition(
      bearing: 192.8334901395799,
      target: LatLng(4.0580037, 9.7260679),
      tilt: 59.440717697143555,
      zoom: 19.151926040649414);

  Set<Marker> _markers = HashSet<Marker>();
  int _markerIdCounter = 1;

  bool _isMarker = false;

  // @override
  // void initState() {
  //   super.initState();
  //   super.initState();
  //   _locationData = widget.location;
  // }

  void _setMarkers(LatLng point) {
    final String markerIdval = 'marker_id$_markerIdCounter';
    _markerIdCounter++;
    setState(() {
      print(
          'Marker | Latitude: ${point.latitude} longitude: ${point.longitude}');
      _markers.add(
        Marker(
          markerId: MarkerId(markerIdval),
          position: point,
        ),
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    LatLng po = new LatLng(4.0580037, 9.7260679);
    Set<Marker> _markers = HashSet<Marker>();
    _markers.add(
      Marker(
        markerId: MarkerId("1"),
        position: po,
      ),
    );
    return new Scaffold(
      body: GoogleMap(
        markers: _markers,
        mapType: MapType.hybrid,
        initialCameraPosition: _kGooglePlex,
        onMapCreated: (GoogleMapController controller) {
          _controller.complete(controller);
        },
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          _goToTheLake();
        },
        label: Text('Zoomer'),
        icon: Icon(Icons.car_rental),
      ),
    );
  }

  Future<void> _goToTheLake() async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(_kLake));
  }
}
