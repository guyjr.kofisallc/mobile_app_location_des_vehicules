import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_imagenetwork/flutter_imagenetwork.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:rapidos_mobile/components/body_adresse_livraison.dart';
import 'package:rapidos_mobile/entities/restaurants.dart';
import '../constants.dart';
import 'constructionPage.dart';
import 'detail_resto.dart';

class SinglePopulary extends StatefulWidget {
  SinglePopulary(
    this.resto,
  );

  Restaurant resto;

  @override
  State<SinglePopulary> createState() => _SinglePopularyState();
}

class _SinglePopularyState extends State<SinglePopulary> {
  // FlutterCart cart;
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    var rng = Random();
    var now = new DateTime.now();
    double calculateDistance(lat1, lon1, lat2, lon2) {
      var p = 0.017453292519943295;
      var c = cos;
      var a = 0.5 -
          c((lat2 - lat1) * p) / 2 +
          c(lat1 * p) * c(lat2 * p) * (1 - c((lon2 - lon1) * p)) / 2;
      return 12742 * asin(sqrt(a));
      return 0.0;
    }

    double distance = calculateDistance(widget.resto.position.latitude,
        widget.resto.position.longitude, latActuel, lonActuel);
    // double distance =
    //     calculateDistance(4.0564617, 4.0564617, 4.0564617, 4.0564617);
    return InkWell(
      onTap: () {
        if (int.parse(widget.resto.heureOuverture) < now.hour) {
          if (int.parse(widget.resto.heureFermerture) < now.hour) {
            displayMessageproduit(context);
          } else {
            Navigator.of(context).push(new MaterialPageRoute(
                builder: (context) => new DetailResto(widget.resto
                    // cart
                    )));
          }
        } else {
          print("on entre");
          if (int.parse(widget.resto.heureFermerture) > now.hour) {
            displayMessageproduit(context);
          } else {
            Navigator.of(context).push(new MaterialPageRoute(
                builder: (context) => new DetailResto(widget.resto
                    // cart
                    )));
          }
        }
        // Navigator.of(context).push(new MaterialPageRoute(
        //     builder: (context) => new DetailResto(widget.resto
        //         // cart
        //         )));
      },
      child: Container(
        width: size.width - (kDefaultPadding * 2),
        // height: size.height * 0.28,
        padding: EdgeInsets.all(kDefaultPadding / 2),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(
              offset: Offset(0, 5),
              blurRadius: 10,
              color: Colors.grey.withOpacity(0.8),
            )
          ],
        ),
        child: Stack(children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              ClipRRect(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20),
                    topRight: Radius.circular(20)),
                child: AjanuwImage(
                  image: AjanuwNetworkImage(widget.resto.photo),
                  fit: BoxFit.cover,
                  width: size.width - (kDefaultPadding * 2),
                  height: size.height * 0.15,
                  loadingWidget: Image.asset(
                    "assets/images/restoAwait.png",
                    width: 50,
                    height: 50,
                    // color: Colors.red,
                  ),
                  // loadingBuilder: AjanuwImage.defaultLoadingBuilder,
                  errorBuilder: immageErreurChargement,
                ),
              ),
              Container(
                // height: size.height * 0.1,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(20),
                      bottomRight: Radius.circular(20)),
                  color: Colors.grey.shade100,
                ),
                padding: EdgeInsets.only(bottom: 10),
                child: Column(
                  children: <Widget>[
                    ListTile(
                      title: Text(
                        widget.resto.name,
                        style: text4nb,
                      ),
                      subtitle: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            widget.resto.address,
                            style: text6nb,
                          ),
                          Text(
                            widget.resto.description,
                            style: text6nn,
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: kDefaultPadding),
                      child: Row(
                        children: <Widget>[
                          Text("4.16", style: text6nn),
                          RatingBar.builder(
                            itemSize: 15.0,
                            initialRating: 0,
                            minRating: 1,
                            direction: Axis.horizontal,
                            allowHalfRating: true,
                            itemCount: 1,
                            itemBuilder: (context, _) => Icon(
                              Icons.star,
                              color: kPrimaryColor,
                            ),
                            onRatingUpdate: (rating) {
                              print(rating);
                            },
                          ),
                          Text(
                            "(${rng.nextInt(5) + 3})",
                            style: text6nn,
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
          Positioned(
              top: (size.height * 0.15) - 20,
              right: kDefaultPadding,
              child: Container(
                padding: EdgeInsets.all(5),
                //width: size.width / 4,
                decoration: BoxDecoration(
                    color: kPrimaryColor,
                    borderRadius: BorderRadius.all(Radius.circular(55))),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset(
                      "assets/images/liv.png",
                      width: 20,
                      height: 20,
                      color: Colors.white,
                      // color: Colors.red,
                    ),
                    Padding(padding: EdgeInsets.only(left: 10)),
                    Text(
                      distance < 4
                          ? "XAF 500 "
                          : distance < 10
                              ? "XAF 1000"
                              : distance < 14
                                  ? "XAF 1500"
                                  : " XAF 2000",
                      style: TextStyle(
                          fontSize: 15,
                          color: Colors.white,
                          fontFamily: 'DayRoman',
                          fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ))
        ]),
      ),
    );
  }
}
