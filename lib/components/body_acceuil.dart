import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_svg/svg.dart';
import 'package:rapidos_mobile/components/inscription.dart';
import '../constants.dart';
import '../entities/login.dart';
import '../services/loginService.dart';
import 'body_connexion.dart';
import 'connexion.dart';
import 'home_page.dart';

class BodyAcceuil extends StatefulWidget {
  BodyAcceuil();

  @override
  State<BodyAcceuil> createState() => _BodyAcceuilState();
}

class _BodyAcceuilState extends State<BodyAcceuil> {
  final storage = FlutterSecureStorage();

  Widget wi = Text(
    'Continuer comme visiteur',
    style: TextStyle(
      color: Colors.black,
      fontWeight: FontWeight.bold,
      fontSize: 15,
      fontFamily: font,
    ),
    textAlign: TextAlign.center,
  );
  void changeTextToLoader() {
    setState(() {
      wi = CircularProgressIndicator(
        color: Colors.white,
      );
    });
  }

  void changeLoaderTextTo() {
    setState(() {
      wi = Text(
        "Valider",
        style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return SingleChildScrollView(
      child: Container(
          width: size.width - (kDefaultPadding * 2),
          //color: Colors.amber,
          padding: EdgeInsets.only(top: kDefaultPadding),
          margin:
              EdgeInsets.only(left: kDefaultPadding, right: kDefaultPadding),
          child: Column(mainAxisAlignment: MainAxisAlignment.start, children: <
              Widget>[
            Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: 20, vertical: kDefaultPadding * 1.5),
            ),
            SvgPicture.asset(
              "assets/images/undraw_online_groceries_a02y.svg",
              width: size.width * 0.6,
              //width: 185,
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: 20, vertical: kDefaultPadding),
            ),
            Center(
              child: Container(
                //color: Colors.amber,
                padding: EdgeInsets.only(left: kDefaultPadding * 2),
                child: Row(
                  children: [
                    Container(
                      width: (size.width / 2) - kDefaultPadding * 2,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            "Bienvenue à",
                            style: TextStyle(
                                fontSize: 15,
                                color: Color.fromRGBO(250, 25, 47, 1),
                                fontFamily: 'DayRoman',
                                fontWeight: FontWeight.w300),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      width: 96,
                      child: ClipRRect(
                        //un peu comme un conteneur mais avec border radius
                        borderRadius: BorderRadius.circular(9.0),
                        child: Image.asset(
                          "assets/images/logo.png",
                          // width: size.width * 60,
                          fit: BoxFit.cover,
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: 20, vertical: kDefaultPadding - 10),
            ),
            Center(
              child: Text(
                  "Commandez vos repas favoris dans les restaurants  de votre choix et suivez votre commande en temps réel",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontWeight: FontWeight.w400,
                      fontSize: 10,
                      color: Colors.black,
                      fontFamily: 'DayRoman',
                      letterSpacing: 0.5)),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 25),
            ),
            SizedBox(
              width: size.width * 0.75,
              height: 50,
              child: FlatButton(
                highlightColor: Colors.white,
                hoverColor: Colors.blue,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
                color: Color.fromRGBO(250, 25, 47, 1),
                onPressed: () {
                  Navigator.of(context).push(
                      new MaterialPageRoute(builder: (context) => Connexion()));
                },
                child: Text(
                  "Connexion",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                      fontFamily: 'DayRoman',
                      fontWeight: FontWeight.bold),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 15),
            ),
            Container(
              decoration: BoxDecoration(
                border: Border.all(color: Color.fromRGBO(250, 25, 47, 1)),
                borderRadius: BorderRadius.circular(20),
              ),
              child: SizedBox(
                width: size.width * 0.75,
                height: 50,
                child: FlatButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20),
                  ),
                  color: Colors.transparent,
                  onPressed: () {
                    Navigator.of(context).push(new MaterialPageRoute(
                        builder: (context) => Inscription()));
                  },
                  child: Text(
                    "Inscription",
                    style: TextStyle(
                        color: Color.fromRGBO(250, 25, 47, 1),
                        fontSize: 16,
                        fontFamily: 'DayRoman',
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ),
            ),
            TextButton(
              style: TextButton.styleFrom(
                textStyle: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                ),
              ),
              onPressed: () async {
                var jwt =
                    await LoginService().infoClient("visiteur", "visiteur");
                Login log = new Login();
                if (jwt.toString() == "401") {
                  changeLoaderTextTo();
                  displayDialog(context,
                      " Nombre maximal de visiteur atteint ,  s'il vous plait veillez vous inscrire");
                  return null;
                } else if (jwt.runtimeType == log.runtimeType) {
                  if (jwt.userRole == "ROLE_CLIEN") {
                    changeLoaderTextTo();
                    displayDialog(context,
                        "Vous n'etes pas autorise a acceder a cette application. veillez aller vers celui reserve au client");
                  } else {
                    var str = jwt.accessToken.split(".");
                    var payload = json.decode(
                        ascii.decode(base64.decode(base64.normalize(str[1]))));
                    storage.write(key: "jwt", value: jwt.accessToken);
                    storage.write(key: "username", value: "Visiteur");
                    storage.write(key: "role", value: jwt.userRole);
                    storage.write(key: "idClient", value: payload["sub"]);
                    storage.write(
                        key: "idUser", value: payload['sub'].toString());
                    changeLoaderTextTo();
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => HomePage()));
                  }

                  return null;
                } else if (jwt.toString() == "500") {
                  changeLoaderTextTo();
                  displayDialog(context,
                      "Désolé une erreur au niveau du serveur Nos ingénieurs travaillent dessus");
                  return null;
                } else {
                  changeLoaderTextTo();
                  displayDialog(context, "Pas de connexion internet");
                  return null;
                }
              },
              child: Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    wi,
                    Icon(
                      Icons.arrow_forward_sharp,
                      color: kPrimaryColor,
                      size: 30,
                    )
                  ],
                ),
              ),
            ),
          ])),
    );
  }
}
