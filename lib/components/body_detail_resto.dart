import 'package:flutter/material.dart';
import 'package:flutter_imagenetwork/flutter_imagenetwork.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:rapidos_mobile/components/single_product.dart';
import 'package:rapidos_mobile/components/title_with_more_btn.dart';
import 'package:rapidos_mobile/components/your_Cart.dart';
import 'package:rapidos_mobile/constants.dart';
import 'package:rapidos_mobile/entities/allProduit.dart';
import 'package:rapidos_mobile/entities/restaurants.dart';
import 'package:rapidos_mobile/services/avoirToutProduit.dart';

class BodyDetailResto extends StatefulWidget {
  BodyDetailResto(this.resto
      //  this._cart
      );
  Restaurant resto;
  // FlutterCart _cart;

  @override
  _BodyDetailRestoState createState() => _BodyDetailRestoState(
      // _cart
      );
}

class _BodyDetailRestoState extends State<BodyDetailResto> {
  _BodyDetailRestoState(
      // this._cart
      );

  // FlutterCart _cart;
  int nbreProduit = 0;
  final storage = FlutterSecureStorage();

  void augmenteQte() {
    setState(() {
      nbreProduit = nbreProduit + 1;
    });
  }

  bool visible = false;
  void diminueQte() {
    setState(() {
      nbreProduit = nbreProduit - 1;
    });
  }

  void changeVisible() {
    setState(() {
      visible = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    Produit produit;
    nbreProduit = cart.getCartItemCount();
    // nbreProduit = 0;
    if (cart.getCartItemCount() == 0) {
      visible = false;
    }

    return Stack(children: [
      CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            iconTheme: themeIcon,
            pinned: true,
            snap: false,
            floating: false,
            expandedHeight: size.height * 0.25,
            flexibleSpace: FlexibleSpaceBar(
              // title: Row(
              //   mainAxisAlignment: MainAxisAlignment.start,
              //   children: [
              //     Text(
              //       "${widget.resto.name}",
              //       style: TextStyle(
              //         fontWeight: FontWeight.bold,
              //         fontFamily: font,
              //         fontSize: 20,
              //       ),
              //     ),
              //   ],
              // ),
              background: Stack(children: [
                Center(
                  child: AjanuwImage(
                    // image: AjanuwNetworkImage(
                    //     "https://pbs.twimg.com/media/DNjWneUX4AIXUMW.jpg"),
                    image: AjanuwNetworkImage(widget.resto.photo),
                    fit: BoxFit.scaleDown,
                    width: size.width,
                    height: size.height / 3,
                    loadingWidget: Container(
                      height: size.height / 3,
                      child: Center(
                        child: Image.asset(
                          "assets/images/restoAwait.png",
                          width: 100,
                          height: 100,
                          // color: Colors.red,
                        ),
                      ),
                    ),
                    // loadingBuilder: AjanuwImage.defaultLoadingBuilder,
                    errorBuilder: immageErreurChargement,
                  ),
                ),
                Positioned(
                    child: Container(
                  height: size.height,
                  width: size.width,
                  decoration: BoxDecoration(
                      gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [
                      Colors.transparent,
                      Colors.black.withOpacity(0.2),
                    ],
                  )),
                ))
              ]),
            ),
          ),
          SliverToBoxAdapter(
              child: Column(
            children: [
              Column(children: <Widget>[
                ListTile(
                  title: Text(
                    "${widget.resto.name}",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontFamily: font,
                      fontSize: 22,
                    ),
                  ),
                  subtitle: Text(
                    "${widget.resto.description}",
                    style: text5nn,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(left: kDefaultPadding),
                  child: Row(
                    children: <Widget>[
                      Text("4.16", style: text6nb),
                      RatingBar.builder(
                        itemSize: 20.0,
                        initialRating: 0,
                        minRating: 1,
                        direction: Axis.horizontal,
                        allowHalfRating: true,
                        itemCount: 1,
                        itemBuilder: (context, _) => Icon(
                          Icons.star,
                          color: kPrimaryColor,
                        ),
                        onRatingUpdate: (rating) {
                          print(rating);
                        },
                      ),
                      Text(
                        "(2)",
                        style: text6nb,
                      ),
                    ],
                  ),
                ),
                Padding(padding: const EdgeInsets.only(top: 25)),
                TitleWithMoreBtn(title: 'Au Menu Chez nous'),
                FutureBuilder(
                  future: AfficheProduit(widget.resto.userId.toString())
                      .AfficheToutProduit(),
                  builder: (context, snapshot) {
                    if (!snapshot.hasData) {
                      return Container(
                        // height: size.height,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Center(
                                child: Transform.scale(
                              scale: 1,
                              child: Center(
                                  child: Image.asset(
                                "assets/images/donut.gif",
                                width: 150,
                                height: 150,
                              )),
                            )),
                          ],
                        ),
                      );
                    } else {
                      if (snapshot.data.length != 0) {
                        return Stack(
                          children: [
                            Container(
                              // height: size.height * 0.8,
                              // child: SingleChildScrollView(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Container(
                                    // padding: EdgeInset.only(
                                    //     left: kDefaultPadding,
                                    //     right: kDefaultPadding),
                                    child: Column(
                                      children: [
                                        ListView.builder(
                                            scrollDirection: Axis.vertical,
                                            shrinkWrap: true,
                                            physics:
                                                NeverScrollableScrollPhysics(),
                                            itemCount: snapshot.data.length,
                                            itemBuilder: (context, index) {
                                              String message = snapshot
                                                  .data[index].designation;

                                              return InkWell(
                                                onTap: () {
                                                  produit =
                                                      snapshot.data[index];
                                                  Navigator.of(context).push(
                                                      new MaterialPageRoute(
                                                          builder: (context) =>
                                                              SingleProduct(
                                                                  produit,
                                                                  // _cart,
                                                                  nbreProduit,
                                                                  produit
                                                                      .prixUnitaire
                                                                      .toDouble(),
                                                                  widget
                                                                      .resto)));
                                                },
                                                child: Container(
                                                  decoration: BoxDecoration(
                                                      border: Border(
                                                    top: BorderSide(
                                                        color: Colors
                                                            .grey.shade300),
                                                  )),
                                                  // padding: EdgeInsets.only(bottom: kDefaultPadding - 15),
                                                  child: Column(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceBetween,
                                                    children: [
                                                      ListTile(
                                                        contentPadding:
                                                            EdgeInsets.all(
                                                                kDefaultPadding *
                                                                    0.5),
                                                        leading: Container(
                                                          width: 60,
                                                          child: CircleAvatar(
                                                            radius: 56,
                                                            backgroundImage:
                                                                NetworkImage(
                                                                    snapshot
                                                                        .data[
                                                                            index]
                                                                        .image),
                                                          ),
                                                        ),
                                                        title: Text(
                                                            snapshot.data[index]
                                                                .designation,
                                                            style: text5nb),
                                                        subtitle: Column(
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .start,
                                                          children: <Widget>[
                                                            Text(
                                                                snapshot
                                                                        .data[
                                                                            index]
                                                                        .prixUnitaire
                                                                        .hashCode
                                                                        .toString() +
                                                                    " XAF",
                                                                style: text6rn),
                                                            RatingBar.builder(
                                                              itemSize: 10.0,
                                                              initialRating: 4,
                                                              minRating: 1,
                                                              direction: Axis
                                                                  .horizontal,
                                                              allowHalfRating:
                                                                  true,
                                                              itemCount: 5,
                                                              itemBuilder: (
                                                                context,
                                                                _,
                                                              ) =>
                                                                  Icon(
                                                                Icons.star,
                                                                color:
                                                                    kPrimaryColor,
                                                              ),
                                                              onRatingUpdate:
                                                                  (rating) {
                                                                print(rating);
                                                              },
                                                            ),
                                                          ],
                                                        ),
                                                        trailing: SizedBox(
                                                          width: size.width / 8,
                                                          child: TextButton(
                                                            style: boutonRouge,
                                                            onPressed: () {
                                                              augmenteQte();
                                                              changeVisible();
                                                              final snackBar =
                                                                  SnackBar(
                                                                content: Text(message
                                                                        .toString() +
                                                                    " a ete ajouter au panier"),
                                                                backgroundColor:
                                                                    kPrimaryColor,
                                                                elevation: 55,
                                                                duration:
                                                                    Duration(
                                                                        seconds:
                                                                            2),
                                                                // action: SnackBarAction(
                                                                //   label: 'Undo',
                                                                //   onPressed: () {
                                                                //     // Some code to undo the change.
                                                                //   },

                                                                // )
                                                              );
                                                              ScaffoldMessenger
                                                                      .of(
                                                                          context)
                                                                  .showSnackBar(
                                                                      snackBar);
                                                              cart.addToCart(
                                                                  productId: snapshot
                                                                      .data[
                                                                          index]
                                                                      .produitId,
                                                                  unitPrice: snapshot
                                                                      .data[
                                                                          index]
                                                                      .prixUnitaire,
                                                                  productName: snapshot
                                                                      .data[
                                                                          index]
                                                                      .designation,
                                                                  quantity: 1,
                                                                  productDetailsObject:
                                                                      snapshot
                                                                          .data[
                                                                              index]
                                                                          .image);
                                                            },
                                                            child: Icon(
                                                              Icons
                                                                  .shopping_cart_outlined,
                                                              color:
                                                                  Colors.white,
                                                              size: 20,
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              );
                                            }),
                                        Visibility(
                                          visible: visible,
                                          child: Container(
                                            height: size.height * 0.15,
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                              // ),
                            ),
                          ],
                        );
                      } else {
                        return Container(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text(
                                "Ce restaurant ne possède pas  encore de produits !!!",
                                style: TextStyle(
                                  fontFamily: 'DayRoman',
                                  fontSize: 20,
                                ),
                                textAlign: TextAlign.center,
                              )
                            ],
                          ),
                        );
                      }
                    }
                  },
                )
              ])
            ],
          )),
        ],
      ),
      Positioned(
        bottom: 0,
        child: Visibility(
          visible: visible,
          child: Container(
              height: size.height * 0.15,
              width: size.width,
              padding: EdgeInsets.all(size.height * 0.02),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(30),
                  topRight: Radius.circular(30),
                ),
                boxShadow: [
                  BoxShadow(
                      color: Colors.grey.shade400,
                      offset: Offset(10, 10),
                      blurRadius: 20,
                      spreadRadius: 9),
                ],
              ),
              child: Column(
                children: [
                  Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                            "Produits (" +
                                cart.getCartItemCount().toString() +
                                ")",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 15,
                              fontFamily: 'DayRoman',
                            )),
                        Spacer(),
                        Text(cart.getTotalAmount().toString() + " FCFA",
                            style: TextStyle(
                              fontWeight: FontWeight.normal,
                              fontSize: 13,
                              fontFamily: 'DayRoman',
                            )),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: kDefaultPadding, vertical: 10),
                  ),
                  SizedBox(
                    width: size.width * 0.7,
                    height: 40,
                    child: TextButton(
                      style: boutonRouge,
                      onPressed: () {
                        Navigator.of(context).push(
                            new MaterialPageRoute(builder: (context) => YouCart(
                                // cart,
                                widget.resto)));
                      },
                      child: Text(
                        "Commander",
                        style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'DayRoman',
                          fontWeight: FontWeight.bold,
                          fontSize: 12,
                        ),
                      ),
                    ),
                  ),
                ],
              )),
        ),
        //bottomNavigationBar: Footer_all(),
      ),
    ]);
  }
}
