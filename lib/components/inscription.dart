import 'package:flutter/material.dart';
import 'package:rapidos_mobile/components/acceuil.dart';
import '../constants.dart';
import 'body_inscrition.dart';

class Inscription extends StatelessWidget {
  Inscription();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(context),
      body: BodyInscription(),
      //bottomNavigationBar: Footer_all(),
    );
  }

  AppBar buildAppBar(BuildContext context) {
    return AppBar(
      centerTitle: true,
      iconTheme: IconThemeData(color: Colors.black),
      title: Text(
        "Inscription",
        style: TextStyle(
            color: Colors.black, fontWeight: FontWeight.bold, fontSize: 18),
      ),
      // leading: IconButton(
      //     color: kPrimaryColor,
      //     onPressed: () {
      //       Navigator.of(context)
      //           .push(new MaterialPageRoute(builder: (context) => Acceuil()));
      //     },
      //     icon: Icon(
      //       Icons.arrow_back_ios_sharp,
      //       size: 30,
      //     )),
    );
  }
}
