import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:rapidos_mobile/components/inscription.dart';
import 'package:rapidos_mobile/components/restaurePassword.dart';
import 'package:rapidos_mobile/entities/login.dart';
import '../constants.dart';
import '../services/loginService.dart';
import 'home_page.dart';

displayDialog(BuildContext context, String text) {
  Size size = MediaQuery.of(context).size;
  showDialog(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
          child: Container(
            constraints: BoxConstraints(maxHeight: 180),
            child: Padding(
              padding: const EdgeInsets.all(kDefaultPadding),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      InkWell(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: CircleAvatar(
                          child: Icon(
                            Icons.close,
                            color: Colors.white,
                            size: 14,
                          ),
                          backgroundColor: kPrimaryColor,
                          radius: 13,
                        ),
                      ),
                    ],
                  ),
                  // Text(
                  //   "Asso’o, Connecte toi !",
                  //   style: TextStyle(
                  //     fontFamily: font,
                  //     fontSize: 20,
                  //     fontWeight: FontWeight.bold,
                  //     fontStyle: FontStyle.normal,
                  //   ),
                  //   textAlign: TextAlign.left,
                  // ),
                  Text(
                    "Erreur",
                    style: TextStyle(
                      fontFamily: font,
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      fontStyle: FontStyle.normal,
                    ),
                    textAlign: TextAlign.left,
                  ),
                  // Text(
                  //   "de ways",
                  //   style: TextStyle(
                  //     fontFamily: font,
                  //     fontSize: 20,
                  //     fontWeight: FontWeight.bold,
                  //     fontStyle: FontStyle.normal,
                  //   ),
                  //   textAlign: TextAlign.left,
                  // ),
                  Padding(padding: EdgeInsets.all(5)),
                  Text(
                    text,
                    style: TextStyle(
                      fontFamily: font,
                      fontSize: 13,
                      // fontWeight: FontWeight.bold,
                      fontStyle: FontStyle.normal,
                    ),
                    textAlign: TextAlign.left,
                  ),
                  Padding(padding: EdgeInsets.all(5)),
                ],
              ),
            ),
          ),
        );
      });
}

class BodyConnexion extends StatefulWidget {
  BodyConnexion();

  @override
  _BodyConnexionState createState() => _BodyConnexionState();
}

class _BodyConnexionState extends State<BodyConnexion> {
  final storage = FlutterSecureStorage();
  bool visible = true;

  final TextEditingController _nameController = TextEditingController();

  final TextEditingController _passwordController = TextEditingController();

  void rendVisble() {
    setState(() {
      if (visible == true) {
        visible = false;
      } else {
        visible = true;
      }
    });
  }

  Widget wi = Text(
    "Valider",
    style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
  );

  void changeTextToLoader() {
    setState(() {
      wi = CircularProgressIndicator(
        color: Colors.white,
      );
    });
  }

  void changeLoaderTextTo() {
    setState(() {
      wi = Text(
        "Valider",
        style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
      );
    });
  }

  // void displayDialog(BuildContext context, String title, String text) =>
  //     showDialog(
  //       context: context,
  //       builder: (context) =>
  //           AlertDialog(title: Text(title), content: Text(text)),
  //     );

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return SingleChildScrollView(
        child: Container(
            width: size.width - (kDefaultPadding * 2),
            //color: Colors.amber,
            padding: EdgeInsets.only(top: kDefaultPadding),
            margin:
                EdgeInsets.only(left: kDefaultPadding, right: kDefaultPadding),
            child:
                Column(mainAxisAlignment: MainAxisAlignment.start, children: <
                    Widget>[
              // Padding(
              //   padding: EdgeInsets.symmetric(
              //       horizontal: 20, vertical: kDefaultPadding),
              // ),
              // SvgPicture.asset(
              //   "assets/images/login.svg",
              //   width: size.width * 0.6,
              //   //width: 185,
              // ),
              Container(
                // width: size.width,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      "Asso’o, bon retour au",
                      style: TextStyle(
                        fontFamily: font,
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        fontStyle: FontStyle.normal,
                      ),
                      textAlign: TextAlign.left,
                    ),
                    Text(
                      " restaurant!",
                      style: TextStyle(
                        fontFamily: font,
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        fontStyle: FontStyle.normal,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ],
                ),
              ),
              // Padding(
              //   padding: EdgeInsets.symmetric(
              //       horizontal: 20, vertical: kDefaultPadding),
              // ),
              Form(
                key: _formKey,
                child: Column(
                  children: [
                    Container(
                        margin: EdgeInsets.only(top: kDefaultPadding),
                        child: Column(children: <Widget>[
                          TextFieldContainer(
                            child: TextFormField(
                              validator: (value) {
                                if (value == null || value.isEmpty) {
                                  displayDialog(context,
                                      "Veillez renseigner votre nom d'utilisateur");
                                  return 'Entrer Votre username ';
                                }
                                return null;
                              },
                              controller: _nameController,
                              onChanged: (value) {},
                              decoration: InputDecoration(
                                icon: Icon(Icons.person),
                                labelText: "Login (Nom d'utilisateur)",
                                labelStyle: TextStyle(
                                    fontFamily: 'DayRoman', fontSize: 15),
                                border: InputBorder.none,
                              ),
                            ),
                          ),
                        ])),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 5, vertical: 0),
                    ),
                    Container(
                        margin: EdgeInsets.only(top: kDefaultPadding - 10),
                        child: Column(children: <Widget>[
                          TextFieldContainer(
                            child: TextFormField(
                              validator: (value) {
                                if (value == null || value.isEmpty) {
                                  displayDialog(context,
                                      "Veillez renseigner votre nom d'utilisateur");
                                  return 'Entrer votre mot de passe ';
                                }
                                return null;
                              },
                              controller: _passwordController,
                              obscureText: visible,
                              onChanged: (value) {},
                              decoration: InputDecoration(
                                icon: Icon(Icons.lock),
                                labelText: "Mot de passe",
                                labelStyle: TextStyle(
                                    fontFamily: 'DayRoman', fontSize: 14),
                                suffixIcon: IconButton(
                                  onPressed: () {
                                    rendVisble();
                                  },
                                  icon: Icon(Icons.visibility),
                                  color: Color.fromRGBO(250, 25, 47, 1),
                                ),
                                border: InputBorder.none,
                              ),
                            ),
                          ),
                        ])),
                    TextButton(
                      style: TextButton.styleFrom(
                        textStyle: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      onPressed: () {
                        Navigator.of(context).push(new MaterialPageRoute(
                            builder: (context) => RestaurePassword()));
                      },
                      child: Center(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            const Text(
                              'Mot de passe oublié?',
                              style: TextStyle(
                                  fontStyle: FontStyle.normal,
                                  decoration: TextDecoration.underline,
                                  fontSize: 12,
                                  color: Colors.black,
                                  fontWeight: FontWeight.w300,
                                  fontFamily: 'roboto'),
                              textAlign: TextAlign.center,
                            ),
                          ],
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: 5, vertical: kDefaultPadding),
                    ),
                    Container(
                      decoration: BoxDecoration(
                        color: kPrimaryColor,
                        border: Border.all(
                          width: 3,
                          color: kPrimaryColor,
                        ),
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      child: Material(
                        color: kPrimaryColor,
                        borderRadius: BorderRadius.circular(10.0),
                        // button color
                        child: InkWell(
                          splashColor: Colors
                              .grey, // Couleur qui "envahit" le bouton lors du focus
                          child: SizedBox(
                            width: size.width - (kDefaultPadding * 2),
                            height: 40,
                            child: Center(
                              child: wi,
                            ),
                          ),
                          onTap: () async {
                            if (_formKey.currentState.validate()) {
                              changeTextToLoader();
                              var username = _nameController.text
                                  .replaceAll(" ", "")
                                  .toLowerCase();
                              var password = _passwordController.text;
                              var jwt = await LoginService()
                                  .infoClient(username, password);
                              Login log = new Login();
                              if (jwt.toString() == "401") {
                                changeLoaderTextTo();
                                displayDialog(context,
                                    "Pas de compte trouvé. Verifier votre login et mot de passe.");
                                return null;
                              } else if (jwt.runtimeType == log.runtimeType) {
                                if (jwt.userRole == "ROLE_CLIEN") {
                                  changeLoaderTextTo();
                                  displayDialog(context,
                                      "Vous n'etes pas autorise a acceder a cette application. veillez aller vers celui reserve au client");
                                } else {
                                  var str = jwt.accessToken.split(".");
                                  var payload = json.decode(ascii.decode(
                                      base64.decode(base64.normalize(str[1]))));
                                  storage.write(
                                      key: "jwt", value: jwt.accessToken);
                                  storage.write(
                                      key: "username", value: username);
                                  storage.write(
                                      key: "role", value: jwt.userRole);
                                  storage.write(
                                      key: "idClient", value: payload["sub"]);
                                  storage.write(
                                      key: "idUser",
                                      value: payload['sub'].toString());
                                  changeLoaderTextTo();
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => HomePage()));
                                }

                                return null;
                              } else if (jwt.toString() == "500") {
                                changeLoaderTextTo();
                                displayDialog(context,
                                    "Désolé une erreur au niveau du serveur Nos ingénieurs travaillent dessus");
                                return null;
                              } else {
                                changeLoaderTextTo();
                                displayDialog(
                                    context, "Pas de connexion internet");
                                return null;
                              }
                            }
                          },
                        ),
                      ),
                    )
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 5, vertical: 10),
              ),
              Center(
                child: TextButton(
                  onPressed: () => Navigator.push(context,
                      MaterialPageRoute(builder: (context) => Inscription())),
                  child: Text(
                    "J'ai pas un compte !!!",
                    style: TextStyle(
                        fontFamily: 'DayRoman', fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 5, vertical: 10),
              ),
            ])));
  }
}

class RoundedInputField extends StatelessWidget {
  final String hintText;
  final IconData icon;
  final ValueChanged<String> onChange;
  const RoundedInputField({
    Key key,
    this.hintText,
    this.icon = Icons.person,
    this.onChange,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFieldContainer(
      child: TextField(
        onChanged: onChange,
        decoration: InputDecoration(
          icon: Icon(icon),
          labelText: hintText,
          border: InputBorder.none,
        ),
      ),
    );
  }
}

class PRoundedInputField extends StatelessWidget {
  final String hintText;
  final IconData icon;
  final ValueChanged<String> onChange;
  const PRoundedInputField({
    Key key,
    this.hintText,
    this.icon = Icons.person,
    this.onChange,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFieldContainer(
      child: TextField(
        obscureText: true,
        onChanged: onChange,
        decoration: InputDecoration(
          icon: Icon(icon),
          labelText: hintText,
          suffixIcon: Icon(
            Icons.visibility,
            color: Color.fromRGBO(250, 25, 47, 1),
          ),
          border: InputBorder.none,
        ),
      ),
    );
  }
}

class TextFieldContainer extends StatelessWidget {
  final Widget child;
  const TextFieldContainer({
    Key key,
    this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 2),
      width: size.width - (kDefaultPadding * 2),
      decoration: BoxDecoration(
        border: Border.all(
          width: 2,
          color: Colors.grey,
        ),
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
      ),
      child: child,
    );
  }
}
// class TextFieldContainer extends StatelessWidget {
//   final Widget child;
//   const TextFieldContainer({
//     Key key,
//     this.child,
//   }) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     Size size = MediaQuery.of(context).size;
//     return Container(
//       margin: EdgeInsets.symmetric(vertical: 10),
//       padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
//       width: size.width * 0.8,
//       decoration: BoxDecoration(
//         color: Colors.grey.shade300,
//         borderRadius: BorderRadius.circular(29),
//       ),
//       child: child,
//     );
//   }
// }
