import 'package:flutter/material.dart';
import 'package:rapidos_mobile/components/body_detail_du_compte.dart';

import '../constants.dart';

class DetailCompte extends StatelessWidget {
  DetailCompte();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey.shade100,
      appBar: buildAppBar(context),
      body: BodyDetailCompte(),
      floatingActionButton: Text(
        "Version 2.2.0",
        style: TextStyle(color: Colors.black, letterSpacing: 1),
      ),
    );
  }

  AppBar buildAppBar(BuildContext context) {
    return AppBar(
      centerTitle: true,
      iconTheme: IconThemeData(color: Colors.black),
      title: Text(
        "Détails du Compte",
        style: TextStyle(
            fontWeight: FontWeight.bold,
            fontFamily: font,
            fontSize: 15,
            color: Colors.black),
      ),
      // actions: <Widget>[
      //   IconButton(
      //       color: Colors.red,
      //       onPressed: () {},
      //       icon: Icon(
      //         Icons.search,
      //         size: 30,
      //       )),
      //   IconButton(
      //       color: Colors.red,
      //       onPressed: () {
      //         Navigator.of(context)
      //             .push(new MaterialPageRoute(builder: (context) => YouCart()));
      //       },
      //       icon: Icon(
      //         Icons.shopping_cart_outlined,
      //         size: 30,
      //       ))
      // ],
    );
  }
}
