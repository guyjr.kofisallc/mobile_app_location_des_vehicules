import 'package:flutter/material.dart';
import 'package:rapidos_mobile/components/body_detail_cuisine.dart';

import '../constants.dart';

class DetailCuisine extends StatelessWidget {
  DetailCuisine(this.title);
  final String title;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // drawer: (ClientDrawer()),
      appBar: buildAppBar(context, title),
      body: BodyDetailCuisine("$title"),
      // bottomNavigationBar: Footer_all(),
    );
  }

  AppBar buildAppBar(BuildContext context, String text) {
    return AppBar(
      iconTheme: themeIcon,
      centerTitle: true,
      elevation: 1,
      title: Text(
        text,
        style: text3nb,
      ),
    );
  }
}
