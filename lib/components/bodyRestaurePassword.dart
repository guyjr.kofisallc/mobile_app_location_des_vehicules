import 'dart:math';

import 'package:flutter/material.dart';
import 'package:rapidos_mobile/components/verificationNumber.dart';
import 'package:rapidos_mobile/services/mailServices.dart';
import 'package:rapidos_mobile/services/passwordVerify.dart';
import '../constants.dart';
import 'bodyNewPassword.dart';

class BodyRestaurePassword extends StatefulWidget {
  const BodyRestaurePassword({Key key}) : super(key: key);

  @override
  State<BodyRestaurePassword> createState() => _BodyRestaurePasswordState();
}

class _BodyRestaurePasswordState extends State<BodyRestaurePassword> {
  _BodyRestaurePasswordState();

  void changeTextToLoader() {
    setState(() {
      wi = CircularProgressIndicator(
        color: Colors.white,
      );
    });
  }

  Widget wi = Text(
    "Recuperer",
    style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
  );
  void changeLoaderTextTo() {
    setState(() {
      wi = Text(
        "Valider",
        style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
      );
    });
  }

  displayDialog(
    BuildContext context,
    String text,
  ) {
    Size size = MediaQuery.of(context).size;
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0)),
            child: Container(
              constraints: BoxConstraints(maxHeight: 200),
              child: Padding(
                padding: const EdgeInsets.all(kDefaultPadding),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        InkWell(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: CircleAvatar(
                            child: Icon(
                              Icons.close,
                              color: Colors.white,
                              size: 14,
                            ),
                            backgroundColor: kPrimaryColor,
                            radius: 13,
                          ),
                        ),
                      ],
                    ),
                    // Text(
                    //   "Asso’o, Connecte toi !",
                    //   style: TextStyle(
                    //     fontFamily: font,
                    //     fontSize: 20,
                    //     fontWeight: FontWeight.bold,
                    //     fontStyle: FontStyle.normal,
                    //   ),
                    //   textAlign: TextAlign.left,
                    // ),
                    Text(
                      "Erreur",
                      style: TextStyle(
                        fontFamily: font,
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        fontStyle: FontStyle.normal,
                      ),
                      textAlign: TextAlign.left,
                    ),
                    // Text(
                    //   "de ways",
                    //   style: TextStyle(
                    //     fontFamily: font,
                    //     fontSize: 20,
                    //     fontWeight: FontWeight.bold,
                    //     fontStyle: FontStyle.normal,
                    //   ),
                    //   textAlign: TextAlign.left,
                    // ),
                    Padding(padding: EdgeInsets.all(5)),
                    Text(
                      text,
                      style: TextStyle(
                        fontFamily: font,
                        fontSize: 13,
                        color: Colors.black,
                        // fontWeight: FontWeight.bold,
                        fontStyle: FontStyle.normal,
                      ),
                      textAlign: TextAlign.left,
                    ),
                    Padding(padding: EdgeInsets.all(5)),
                  ],
                ),
              ),
            ),
          );
        });
  }

  final TextEditingController _usernameController = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      height: size.height,
      child: SingleChildScrollView(
        padding: EdgeInsets.all(kDefaultPadding),
        child: Column(
          children: [
            Form(
              key: _formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Récupérer votre",
                    style: TextStyle(
                      fontFamily: font,
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      fontStyle: FontStyle.normal,
                    ),
                    textAlign: TextAlign.left,
                  ),
                  Text(
                    "mot de passe",
                    style: TextStyle(
                      fontFamily: font,
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      fontStyle: FontStyle.normal,
                    ),
                    textAlign: TextAlign.left,
                  ),
                  Padding(padding: EdgeInsets.all(5)),
                  Text(
                    "Veillez entrer votre username ou email enfin de changer mots de passe",
                    style: TextStyle(
                      fontFamily: font,
                      fontSize: 13,
                      // fontWeight: FontWeight.bold,
                      fontStyle: FontStyle.normal,
                    ),
                    textAlign: TextAlign.left,
                  ),
                  Padding(padding: EdgeInsets.all(kDefaultPadding)),
                  Text(
                    "Adresse mail ou nom d'utilisateur",
                    style: TextStyle(
                      fontFamily: font,
                      fontSize: 13,
                      // fontWeight: FontWeight.bold,
                      fontStyle: FontStyle.normal,
                    ),
                    textAlign: TextAlign.left,
                  ),
                  TextFieldContainer(
                    child: TextFormField(
                      keyboardType: TextInputType.emailAddress,
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Entrer Votre email ou nom d\'utilisateur ';
                        }
                        return null;
                      },
                      controller: _usernameController,
                      onChanged: (value) {},
                      decoration: InputDecoration(
                        fillColor: Colors.grey,
                        labelStyle: TextStyle(
                          fontFamily: 'roboto',
                        ),
                        errorStyle: TextStyle(
                          fontFamily: 'roboto',
                        ),
                        counterStyle: TextStyle(
                          fontFamily: 'roboto',
                        ),
                        hintStyle: TextStyle(
                          fontFamily: 'roboto',
                        ),
                        helperStyle: TextStyle(
                          fontFamily: 'roboto',
                        ),
                        // suffixIcon: IconButton(
                        //   onPressed: () {
                        //     rendVisble();
                        //   },
                        //   icon: Icon(Icons.visibility),
                        //   color: Color.fromRGBO(250, 25, 47, 1),
                        // ),
                        border: InputBorder.none,
                      ),
                    ),
                  ),
                  Padding(padding: EdgeInsets.all(kDefaultPadding)),
                  Container(
                    decoration: BoxDecoration(
                      color: kPrimaryColor,
                      border: Border.all(
                        width: 3,
                        color: kPrimaryColor,
                      ),
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    child: Material(
                      color: kPrimaryColor,
                      borderRadius: BorderRadius.circular(10.0),
                      // button color
                      child: InkWell(
                        splashColor: Colors
                            .grey, // Couleur qui "envahit" le bouton lors du focus
                        child: SizedBox(
                          width: size.width - (kDefaultPadding * 2),
                          height: 40,
                          child: Center(
                            child: wi,
                          ),
                        ),

                        onTap: () async {
                          var rng = new Random();
                          var verifyPass = rng.nextInt(100000).toString();
                          // Navigator.of(context).push(new MaterialPageRoute(
                          //     builder: (context) => VerificationNumber(
                          //         "btagakou@gmai.com",
                          //         rng.nextInt(100000).toString())));
                          // _sendingSMS();
                          // await FlutterEmailSender.send(send_email);
                          if (_formKey.currentState.validate()) {
                            changeTextToLoader();
                            var response = await PasswordVerify()
                                .verifie(_usernameController.text);
                            if (response.toString() == "500") {
                              changeLoaderTextTo();
                              displayDialog(
                                context,
                                "Probleme avec le serveur , veillez Essayer plus tard",
                              );
                              changeLoaderTextTo();
                            }
                            if (response.toString() == "406") {
                              changeLoaderTextTo();
                              displayDialog(
                                context,
                                "Votre Compte n'existe pas veillez vous inscrire s'il vous plait",
                              );
                              changeLoaderTextTo();
                            } else if (response.toString() == "600") {
                              changeLoaderTextTo();
                              displayDialog(
                                context,
                                "Vous n'etes pas connecter a internet , veillez Essayer plus tard",
                              );
                            } else {
                              print(verifyPass);
                              var responseMail = await MailService()
                                  .messageFogetPassword(
                                      response.toString(), verifyPass);
                              if (responseMail.toString() == "200") {
                                changeLoaderTextTo();

                                Navigator.of(context).push(
                                    new MaterialPageRoute(
                                        builder: (context) =>
                                            VerificationNumber(
                                                response.toString(),
                                                verifyPass)));
                              } else if (responseMail.toString() == "600") {
                                changeLoaderTextTo();
                                displayDialog(
                                  context,
                                  "Vous n'etes pas connecter a internet , veillez Essayer plus tard",
                                );
                              } else {
                                changeLoaderTextTo();
                                displayDialog(
                                  context,
                                  "Soucis avec le serveur lors de l'envoie du mail , veillez Essayer plus tard",
                                );
                              }

                              return null;
                            }
                          }
                        },
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
