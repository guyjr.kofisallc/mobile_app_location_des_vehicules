import 'package:flutter/material.dart';
import 'package:rapidos_mobile/components/command_final.dart';
import 'package:rapidos_mobile/entities/Payement.dart';

import '../constants.dart';
import '../entities/CommandeSend.dart';
import '../entities/Panier.dart';
import '../services/payementservice.dart';

displayDialogPay(
    BuildContext context,
    String numero,
    String token,
    String _adresseClient,
    FlutterCart cart,
    PositionCart positionCart,
    int livraison,
    String nom,
    String idResto) {
  Size size = MediaQuery.of(context).size;
  showDialog(
      context: context,
      builder: (BuildContext context) {
        return FutureBuilder(
            future: PayementService().sendPayement(
                (cart.getTotalAmount() + livraison).toString(),
                numero,
                token,
                "bien"),
            builder: (context, snapshot) {
              if (!snapshot.hasData) {
                return Container(
                  height: size.height,
                  color: Colors.transparent,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Center(
                          child: Image.asset(
                        "assets/images/donut.gif",
                        width: 150,
                        height: 150,
                      )),
                    ],
                  ),
                );
              } else {
                PayementEntities f = new PayementEntities();
                if (snapshot.data.runtimeType == f.runtimeType) {
                  storage.write(key: "statutPayement", value: "PENDING");
                  storage.write(
                      key: "reference", value: snapshot.data.reference);
                  String decisionPayement;
                  print(numero);
                  if (numero.startsWith("7", 4) || numero.startsWith("8", 4)) {
                    decisionPayement = "MTN Mobile Money";
                  } else if (numero.startsWith("5", 4)) {
                    print(int.parse(numero[6]));
                    if (int.parse(numero[6]) < 5) {
                      decisionPayement = "MTN Mobile Money";
                    } else {
                      decisionPayement = "Orange Money";
                    }
                  } else {
                    decisionPayement = "Orange Money";
                  }
                  var param = {
                    "referencePayement": snapshot.data.reference,
                    "description": "Nothing",
                    "address": _adresseClient,
                    "total": cart.getTotalAmount().hashCode,
                    "livraison": livraison.toDouble(),
                    "position": {
                      "latitude": positionCart.latitude,
                      "longitude": positionCart.longitude
                    },
                    "idResto": idResto,
                    "numero": numero
                  };

                  return CommandeFinal(
                      _adresseClient,
                      cart,
                      nom,
                      decisionPayement,
                      idResto,
                      param,
                      numero,
                      snapshot.data.reference,
                      livraison);
                } else {
                  ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(
                        backgroundColor: kPrimaryColor,
                        content: Text(
                          'Numero invalide',
                          style: TextStyle(
                              fontFamily: 'DayRoman',
                              fontSize: 10,
                              fontWeight: FontWeight.bold),
                        )),
                  );
                  Navigator.pop(context);
                  return null;
                }
              }
            });
      });
}
