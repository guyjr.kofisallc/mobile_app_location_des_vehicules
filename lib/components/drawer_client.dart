import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:rapidos_mobile/components/FAQ.dart';
import 'package:rapidos_mobile/components/connexion.dart';
import 'package:rapidos_mobile/components/contactez_nous.dart';
import 'package:rapidos_mobile/components/mon_profil.dart';
import 'package:rapidos_mobile/components/listCommands.dart';
import '../constants.dart';
import 'Detail_du_compte.dart';
import 'cuisine.dart';

final storage = FlutterSecureStorage();
Future<String> get jwtOrEmpty async {
  var jwt = await storage.read(key: "username");
  if (jwt == null) return "";
  return jwt;
}

void displayDialog(BuildContext context, String title, String text) =>
    showDialog(
      context: context,
      builder: (context) =>
          AlertDialog(title: Text(title), content: Text(text)),
    );

class ClientDrawer extends StatelessWidget {
  ClientDrawer({
    Key key,
  }) : super(key: key);
  @override
  Widget build(
    BuildContext context,
  ) {
    print(storage.read(key: "username"));
    Future<String> jwt = storage.read(key: "username");
    print(jwt);
    return Drawer(
        child: ListView(padding: EdgeInsets.only(top: 5), children: [
      DrawerHeader(
        child: Container(
          padding: const EdgeInsets.all(8.0),
          child: Center(
            child: FutureBuilder(
              future: jwtOrEmpty,
              builder: (context, snapshot) {
                if (!snapshot.hasData)
                  return Center(
                      child: Image.asset(
                    "assets/images/donut.gif",
                    width: 150,
                    height: 150,
                  ));
                var str = snapshot.data;
                var now = new DateTime.now();
                if (now.hour <= 14) {
                  return Text(
                    "Bonjour, " + str,
                    style: text1bb,
                  );
                } else {
                  return Text(
                    "Bonsoir, " + str,
                    style: text1bb,
                  );
                }
              },
            ),
          ),
        ),
        decoration: BoxDecoration(
          color: kPrimaryColor,
        ),
      ),
      CustumList(Icons.home, "Acceuil", () => {}),
      CustumList(
          Icons.outdoor_grill,
          "Cuisine",
          () => {
                Navigator.of(context).push(
                    new MaterialPageRoute(builder: (context) => Cuisine()))
              }),
      CustumList(
          Icons.list,
          "Vos Commandes",
          () => {
                Navigator.of(context).push(
                    new MaterialPageRoute(builder: (context) => ListCommande()))
              }),
      CustumList(
          Icons.person,
          "Mon profil",
          () => {
                Navigator.of(context).push(
                    new MaterialPageRoute(builder: (context) => DetailCompte()))
              }),
      // CustumList(Icons.map, "Ma Commande", () => {}),
      CustumList(Icons.phone_iphone_rounded, "Contactez-nous", () {
        Navigator.of(context)
            .push(new MaterialPageRoute(builder: (context) => ContactezNous()));
      }),
      CustumList(Icons.question_mark, "FAQs", () {
        Navigator.of(context)
            .push(new MaterialPageRoute(builder: (context) => FAQ()));
      }),
      CustumList(Icons.logout, "Se déconnecter", () async {
        storage.delete(key: "jwt");
        storage.delete(key: "idUser");
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => Connexion()));
      })
    ]));
  }
}

class CustumList extends StatelessWidget {
  IconData icon;
  String text;
  Function onTap;

  CustumList(this.icon, this.text, this.onTap);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(8.0, 0, 8.0, 0),
      child: Container(
        padding: EdgeInsets.only(left: kDefaultPadding),
        decoration: BoxDecoration(
            // color: Colors.red,
            ),
        child: InkWell(
          onTap: onTap,
          splashColor: kPrimaryColor,
          child: Container(
            height: 60,
            child: Row(
              children: <Widget>[
                Icon(
                  icon,
                  color: Colors.black,
                  size: 20,
                ),
                Padding(
                  padding: EdgeInsets.only(left: kDefaultPadding),
                  child: Text(
                    text,
                    style: text5nn,
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
