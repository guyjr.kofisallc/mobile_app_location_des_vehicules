import 'package:flutter/material.dart';
import 'package:rapidos_mobile/components/restaurePassword.dart';

import '../constants.dart';
import 'facturePage.dart';
import 'newPassword.dart';

class BodyVerificationNumber extends StatefulWidget {
  BodyVerificationNumber(this.email, this.number_verify);
  String email, number_verify;

  @override
  State<BodyVerificationNumber> createState() => _BodyVerificationNumberState();
}

class _BodyVerificationNumberState extends State<BodyVerificationNumber> {
  final TextEditingController _codeController1 = TextEditingController();
  final TextEditingController _codeController2 = TextEditingController();
  final TextEditingController _codeController3 = TextEditingController();
  final TextEditingController _codeController4 = TextEditingController();
  final TextEditingController _codeController5 = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  void _loadingtoText() {
    setState(() {
      setState(() {
        _widget = Text(
          "Recuperer",
          style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
        );
      });
    });
  }

  void _textToLoading() {
    setState(() {
      _widget = Center(
        child: CircularProgressIndicator(
          strokeWidth: 2,
          color: Colors.white,
        ),
      );
    });
  }

  Widget _widget = Text(
    "Recuperer",
    style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
  );
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      height: size.height,
      child: SingleChildScrollView(
        padding: EdgeInsets.all(kDefaultPadding),
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Vérifier adresse ",
                    style: TextStyle(
                      fontFamily: font,
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      fontStyle: FontStyle.normal,
                    ),
                    textAlign: TextAlign.left,
                  ),
                  Text(
                    "Mail",
                    style: TextStyle(
                      fontFamily: font,
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      fontStyle: FontStyle.normal,
                    ),
                    textAlign: TextAlign.left,
                  ),
                  Padding(padding: EdgeInsets.all(5)),
                  Text(
                    "Nous avons envoyer un mail a l'adresse suivant",
                    style: TextStyle(
                      fontFamily: font,
                      fontSize: 13,
                      // fontWeight: FontWeight.bold,
                      fontStyle: FontStyle.normal,
                    ),
                    textAlign: TextAlign.left,
                  ),
                  Padding(padding: EdgeInsets.all(5)),
                  Text(
                    widget.email,
                    style: TextStyle(
                      fontFamily: font,
                      fontSize: 15,
                      fontWeight: FontWeight.bold,
                      fontStyle: FontStyle.normal,
                    ),
                    textAlign: TextAlign.left,
                  ),
                  Padding(padding: EdgeInsets.all(kDefaultPadding)),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      TextFieldContainermini(
                        child: TextFormField(
                          keyboardType: TextInputType.number,
                          maxLength: 1,
                          controller: _codeController1,
                          validator: (value) {},
                          // controller: _passwordController,
                          // obscureText: visible,
                          onChanged: (value) {},
                          decoration: InputDecoration(
                            fillColor: Colors.grey,
                            counterText: "",
                            // suffixIcon: IconButton(
                            //   onPressed: () {
                            //     rendVisble();
                            //   },
                            //   icon: Icon(Icons.visibility),
                            //   color: Color.fromRGBO(250, 25, 47, 1),
                            // ),
                            border: InputBorder.none,
                          ),
                        ),
                      ),
                      TextFieldContainermini(
                        child: TextFormField(
                          keyboardType: TextInputType.number,
                          maxLength: 1,
                          controller: _codeController2,
                          validator: (value) {},
                          // controller: _passwordController,
                          // obscureText: visible,
                          onChanged: (value) {},
                          decoration: InputDecoration(
                            fillColor: Colors.grey,
                            counterText: "",
                            // suffixIcon: IconButton(
                            //   onPressed: () {
                            //     rendVisble();
                            //   },
                            //   icon: Icon(Icons.visibility),
                            //   color: Color.fromRGBO(250, 25, 47, 1),
                            // ),
                            border: InputBorder.none,
                          ),
                        ),
                      ),
                      TextFieldContainermini(
                        child: TextFormField(
                          keyboardType: TextInputType.number,
                          maxLength: 1,
                          controller: _codeController3,
                          validator: (value) {},
                          // controller: _passwordController,
                          // obscureText: visible,
                          onChanged: (value) {},
                          decoration: InputDecoration(
                            fillColor: Colors.grey,
                            counterText: "",
                            // suffixIcon: IconButton(
                            //   onPressed: () {
                            //     rendVisble();
                            //   },
                            //   icon: Icon(Icons.visibility),
                            //   color: Color.fromRGBO(250, 25, 47, 1),
                            // ),
                            border: InputBorder.none,
                          ),
                        ),
                      ),
                      TextFieldContainermini(
                        child: TextFormField(
                          keyboardType: TextInputType.number,
                          maxLength: 1,
                          controller: _codeController4,
                          validator: (value) {},
                          // controller: _passwordController,
                          // obscureText: visible,
                          onChanged: (value) {},
                          decoration: InputDecoration(
                            fillColor: Colors.grey,
                            counterText: "",
                            // suffixIcon: IconButton(
                            //   onPressed: () {
                            //     rendVisble();
                            //   },
                            //   icon: Icon(Icons.visibility),
                            //   color: Color.fromRGBO(250, 25, 47, 1),
                            // ),
                            border: InputBorder.none,
                          ),
                        ),
                      ),
                      TextFieldContainermini(
                        child: TextFormField(
                          keyboardType: TextInputType.number,
                          maxLength: 1,
                          controller: _codeController5,
                          validator: (value) {},
                          // controller: _passwordController,
                          // obscureText: visible,
                          onChanged: (value) {},
                          decoration: InputDecoration(
                            fillColor: Colors.grey,
                            counterText: "",
                            // suffixIcon: IconButton(
                            //   onPressed: () {
                            //     rendVisble();
                            //   },
                            //   icon: Icon(Icons.visibility),
                            //   color: Color.fromRGBO(250, 25, 47, 1),
                            // ),
                            border: InputBorder.none,
                          ),
                        ),
                      ),
                    ],
                  ),
                  Padding(padding: EdgeInsets.all(kDefaultPadding)),
                  Container(
                    decoration: BoxDecoration(
                      color: kPrimaryColor,
                      border: Border.all(
                        width: 3,
                        color: kPrimaryColor,
                      ),
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    child: Material(
                      color: kPrimaryColor,
                      borderRadius: BorderRadius.circular(10.0),
                      // button color
                      child: InkWell(
                        splashColor: Colors
                            .grey, // Couleur qui "envahit" le bouton lors du focus
                        child: SizedBox(
                          width: size.width - (kDefaultPadding * 2),
                          height: 40,
                          child: Center(
                            child: _widget,
                          ),
                        ),
                        onTap: () {
                          if (_formKey.currentState.validate()) {
                            var code = _codeController1.text +
                                _codeController2.text +
                                _codeController3.text +
                                _codeController4.text +
                                _codeController5.text;
                            setState(() {
                              _widget = Center(
                                child: CircularProgressIndicator(
                                  strokeWidth: 2,
                                  color: Colors.white,
                                ),
                              );
                            });
                            print(widget.number_verify.toString());
                            if (code.toString() ==
                                widget.number_verify.toString()) {
                              Navigator.of(context).push(new MaterialPageRoute(
                                  builder: (context) =>
                                      NewPassword(widget.email)));
                            } else {
                              _loadingtoText();
                              displayDialog(
                                context,
                                "",
                                "Le code $code ne correspond pas a celui envoyer sur le mail " +
                                    widget.email,
                              );
                            }
                          }
                        },
                      ),
                    ),
                  ),
                  Row(
                    children: [
                      Text(
                        "0 : 30",
                        style: TextStyle(
                          fontFamily: font,
                          fontSize: 13,
                          // fontWeight: FontWeight.bold,
                          fontStyle: FontStyle.normal,
                        ),
                        textAlign: TextAlign.left,
                      ),
                      Spacer(),
                      TextButton(
                        style: TextButton.styleFrom(
                          textStyle: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        onPressed: () {},
                        child: Center(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              TextButton(
                                onPressed: () {
                                  Navigator.of(context).push(
                                      new MaterialPageRoute(
                                          builder: (context) =>
                                              RestaurePassword()));
                                },
                                child: const Text(
                                  'Renvoyer le code ?',
                                  style: TextStyle(
                                      fontStyle: FontStyle.normal,
                                      decoration: TextDecoration.underline,
                                      fontSize: 12,
                                      color: kPrimaryColor,
                                      fontWeight: FontWeight.w300,
                                      fontFamily: 'roboto'),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}

class Num extends StatelessWidget {
  Num(this.usernameController);
  TextEditingController usernameController;

  @override
  Widget build(BuildContext context) {
    return TextFieldContainermini(
      child: TextFormField(
        keyboardType: TextInputType.number,
        maxLength: 1,
        controller: usernameController,
        validator: (value) {},
        // controller: _passwordController,
        // obscureText: visible,
        onChanged: (value) {},
        decoration: InputDecoration(
          fillColor: Colors.grey,
          counterText: "",
          // suffixIcon: IconButton(
          //   onPressed: () {
          //     rendVisble();
          //   },
          //   icon: Icon(Icons.visibility),
          //   color: Color.fromRGBO(250, 25, 47, 1),
          // ),
          border: InputBorder.none,
        ),
      ),
    );
  }
}

class TextFieldContainermini extends StatelessWidget {
  final Widget child;

  const TextFieldContainermini({
    Key key,
    this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      padding: EdgeInsets.only(left: 14, bottom: 5),
      width: size.width * 0.1,
      height: size.width * 0.1,
      decoration: BoxDecoration(
        color: Colors.grey.withOpacity(0.2),
        borderRadius: BorderRadius.circular(10),
      ),
      child: child,
    );
  }
}
