import 'package:flutter/material.dart';
import 'package:rapidos_mobile/constants.dart';
import 'package:rapidos_mobile/entities/restaurants.dart';
import 'body_detail_resto.dart';

class DetailResto extends StatefulWidget {
  DetailResto(this.resto);
  Restaurant resto;

  @override
  State<DetailResto> createState() => _DetailRestoState();
}

class _DetailRestoState extends State<DetailResto> {
  // FlutterCart _cart;
  @override
  Widget build(BuildContext context) {
    bool shouldPop = true;
    return WillPopScope(
      onWillPop: () async {
        cart.deleteAllCart();
        Navigator.of(context).pop(null);
        return shouldPop;
      },
      child: Scaffold(
        extendBodyBehindAppBar: true,
        body: BodyDetailResto(widget.resto
            //  _cart
            ),
        // bottomNavigationBar: Footer_all(),
      ),
    );
  }

  AppBar buildAppBar(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.transparent,
      iconTheme: IconThemeData(color: Colors.black),
      elevation: 0,
      centerTitle: true,
      actions: <Widget>[],
    );
  }
}
