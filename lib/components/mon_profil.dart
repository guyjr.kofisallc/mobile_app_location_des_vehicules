import 'package:flutter/material.dart';
import 'body_mon_profil.dart';

class MonProfil extends StatelessWidget {
  MonProfil();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey.shade100,
      // drawer: (ClientDrawer()),
      appBar: buildAppBar(context),
      body: BodyMonProfil(),
      floatingActionButton: Text(
        "Version 2.2.0",
        style: TextStyle(color: Colors.black, letterSpacing: 1),
      ),
    );
  }

  AppBar buildAppBar(BuildContext context) {
    return AppBar(
      centerTitle: true,
      iconTheme: IconThemeData(color: Colors.black),
      title: Text(
        "Mon profil",
        style: TextStyle(
          color: Colors.black,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }
}
