import 'package:flutter/material.dart';

import 'BodyVerificationNumber.dart';

class VerificationNumber extends StatefulWidget {
  VerificationNumber(this.email, this.number_verify);
  String email, number_verify;

  @override
  State<VerificationNumber> createState() => _VerificationNumberState();
}

class _VerificationNumberState extends State<VerificationNumber> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(context),
      body: BodyVerificationNumber(widget.email, widget.number_verify),
    );
  }

  AppBar buildAppBar(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.grey.shade50,
      centerTitle: true,
      // title: Text(
      //   "Connexion",
      //   style: TextStyle(
      //     color: Colors.black,
      //     fontWeight: FontWeight.bold,
      //   ),
      // ),
      shadowColor: Colors.transparent,
      iconTheme: IconThemeData(color: Colors.black),
      // leading: Container(
      //   width: 20,
      //   height: 35,
      //   decoration: BoxDecoration(
      //     color: kPrimaryColorgrey,
      //     borderRadius: BorderRadius.circular(60.0),
      //   ),
      //   child: IconButton(
      //       color: Colors.black,
      //       onPressed: () {},
      //       icon: Icon(
      //         Icons.arrow_back_ios_sharp,
      //         size: 30,
      //       )),
      // ),
    );
  }
}
