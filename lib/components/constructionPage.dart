import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:flutter/material.dart';
import 'package:rapidos_mobile/constants.dart';

displayMessageConstruction(
  BuildContext context,
) {
  Size size = MediaQuery.of(context).size;
  showDialog(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
          child: Container(
            constraints: BoxConstraints(maxHeight: 350),
            child: Padding(
              padding: EdgeInsets.fromLTRB(
                  kDefaultPadding, kDefaultPadding, kDefaultPadding, 0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      InkWell(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: CircleAvatar(
                          child: Icon(
                            Icons.close,
                            color: Colors.white,
                            size: 14,
                          ),
                          backgroundColor: kPrimaryColor,
                          radius: 13,
                        ),
                      ),
                    ],
                  ),
                  Text(
                    "Bientot disponible sur RapidosEat",
                    style: TextStyle(
                      fontFamily: font,
                      fontSize: 15,
                      fontWeight: FontWeight.bold,
                      fontStyle: FontStyle.normal,
                    ),
                    textAlign: TextAlign.left,
                  ),
                  AnimatedTextKit(
                    animatedTexts: [
                      TypewriterAnimatedText(
                          'Nos ingenieurs travaillent dessus'),
                    ],
                    onTap: () {
                      print("Tap Event");
                    },
                  ),
                  Center(
                    child: Container(
                      // color: Colors.amber,
                      padding: EdgeInsets.all(8),
                      child: Image.asset(
                        'assets/images/pirate.gif',
                        width: 190,
                        height: 190,
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  Spacer(),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Text(
                        "Hammel brayan",
                        style: TextStyle(
                          fontFamily: font,
                          fontSize: 13,
                          // fontWeight: FontWeight.bold,
                          fontStyle: FontStyle.italic,
                        ),
                        textAlign: TextAlign.left,
                      ),
                    ],
                  ),
                  Padding(padding: EdgeInsets.all(5)),
                ],
              ),
            ),
          ),
        );
      });
}

displayMessageproduit(
  BuildContext context,
) {
  Size size = MediaQuery.of(context).size;
  showDialog(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
          child: Container(
            constraints: BoxConstraints(maxHeight: 300),
            child: Padding(
              padding: const EdgeInsets.fromLTRB(
                  kDefaultPadding, kDefaultPadding, kDefaultPadding, 0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      InkWell(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: CircleAvatar(
                          child: Icon(
                            Icons.close,
                            color: Colors.white,
                            size: 14,
                          ),
                          backgroundColor: kPrimaryColor,
                          radius: 13,
                        ),
                      ),
                    ],
                  ),
                  Text(
                    "Désolé",
                    style: TextStyle(
                      fontFamily: font,
                      fontSize: 15,
                      fontWeight: FontWeight.bold,
                      fontStyle: FontStyle.normal,
                    ),
                    textAlign: TextAlign.left,
                  ),
                  AnimatedTextKit(
                    animatedTexts: [
                      TypewriterAnimatedText('Ce restaurant est  fermé'),
                    ],
                    onTap: () {
                      print("Tap Event");
                    },
                  ),
                  Center(
                    child: Container(
                      // color: Colors.amber,
                      padding: EdgeInsets.all(8),
                      child: Image.asset(
                        'assets/images/chips.png',
                        color: Colors.red,
                        width: 130,
                        height: 130,
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  Padding(padding: EdgeInsets.all(1)),
                  Spacer(),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Text(
                        "Hammel brayan",
                        style: TextStyle(
                          fontFamily: font,
                          fontSize: 13,
                          // fontWeight: FontWeight.bold,
                          fontStyle: FontStyle.italic,
                        ),
                        textAlign: TextAlign.left,
                      ),
                    ],
                  ),
                  Padding(padding: EdgeInsets.all(5)),
                ],
              ),
            ),
          ),
        );
      });
}
