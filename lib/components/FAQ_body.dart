import 'package:flutter/material.dart';
import 'package:rapidos_mobile/entities/messageData.dart';
import 'package:rapidos_mobile/services/currentUser.dart';
import 'package:rapidos_mobile/services/messageServices.dart';

import '../constants.dart';
import '../services/message2Services.dart';
import 'drawer_client.dart';

class FAQBody extends StatefulWidget {
  const FAQBody({Key key}) : super(key: key);

  @override
  State<FAQBody> createState() => _FAQBodyState();
}

class _FAQBodyState extends State<FAQBody> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController _messageController = TextEditingController();
  bool bool1 = false, bool2 = true;
  void changeIconToLoader() {
    setState(() {
      bool1 = true;
      bool2 = false;
    });
  }

  void changeLoaderToIcon() {
    setState(() {
      bool1 = false;
      bool2 = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      height: size.height,
      width: size.width,
      child: Stack(children: [
        AfficheMessage(size: size),
        Positioned(
            bottom: 0,
            child: Container(
                // height: size.height * 0.1,
                width: size.width,
                padding: EdgeInsets.all(size.height * 0.02),
                decoration: BoxDecoration(
                  color: Colors.grey.shade200,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(30),
                    topRight: Radius.circular(30),
                  ),
                  boxShadow: [
                    BoxShadow(
                        color: Colors.grey.shade400,
                        offset: Offset(10, 10),
                        blurRadius: 20,
                        spreadRadius: 9),
                  ],
                ),
                child: Container(
                  child: Row(children: [
                    FutureBuilder(
                      future: jwtOrEmpty,
                      builder: (context, snapshot) {
                        if (!snapshot.hasData) {
                          return Center(
                              child: Image.asset(
                            "assets/images/donut.gif",
                            width: 150,
                            height: 150,
                          ));
                        } else {
                          var str = snapshot.data;
                          return CircleAvatar(
                            child: Text(str[0],
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 25)),
                            radius: 25,
                          );
                        }
                      },
                    ),
                    Form(
                      key: _formKey,
                      child: Row(
                        children: [
                          Container(
                              child: Column(children: <Widget>[
                            TextFieldContainer(
                              child: TextFormField(
                                validator: (value) {
                                  if (value == null || value.isEmpty) {
                                    return "Posez votre question";
                                  }
                                  return null;
                                },
                                controller: _messageController,
                                keyboardType: TextInputType.multiline,
                                onChanged: (value) {},
                                decoration: InputDecoration(
                                  // suffixIcon: Icon(Icons.person),
                                  // labelText: "Login (Nom d'utilisateur)",
                                  hintText: "Posez votre question . . . .",
                                  hintStyle: TextStyle(
                                      fontFamily: 'DayRoman', fontSize: 12),
                                  border: InputBorder.none,
                                ),
                              ),
                            ),
                          ])),
                          Row(
                            children: [
                              Visibility(
                                  visible: bool1,
                                  child: CircularProgressIndicator(
                                    color: kPrimaryColor,
                                  )),
                              Visibility(
                                visible: bool2,
                                child: IconButton(
                                    onPressed: () async {
                                      if (_formKey.currentState.validate()) {
                                        var message = _messageController.text;
                                        var requete = await MessageServices2()
                                            .envoyerMessage(message);
                                        changeIconToLoader();
                                        if (requete.toString() == "201") {
                                          changeLoaderToIcon();
                                          AfficheMessage(
                                            size: size,
                                          );
                                          setState(() {
                                            _messageController.clear();
                                          });
                                        } else if (requete.toString() ==
                                            "600") {
                                          changeLoaderToIcon();
                                          displayDialog(context, "Erreur",
                                              "Probleme de connexion a internet");
                                        } else {
                                          changeLoaderToIcon();
                                          displayDialog(context, "Erreur",
                                              "Probleme au niveau du serveur, Nos ingenieurs travaille dessus");
                                        }
                                      }
                                    },
                                    icon: Icon(
                                      Icons.send,
                                      size: 25,
                                    )),
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                  ]),
                )
                //bottomNavigationBar: Footer_all(),
                ))
      ]),
    );
  }
}

class AfficheMessage extends StatelessWidget {
  const AfficheMessage({
    Key key,
    @required this.size,
  }) : super(key: key);

  final Size size;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(bottom: kDefaultPadding * 3),
      // height: size.height,
      width: size.width,
      child: SingleChildScrollView(
        child: FutureBuilder(
          future: MessageServices().AfficheMessage(),
          builder: (context, snapshot) {
            if (!snapshot.hasData) {
              return Container(
                // height: size.height,
                color: Colors.white,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Center(
                        child: Image.asset(
                      "assets/images/donut.gif",
                      width: 150,
                      height: 150,
                    )),
                  ],
                ),
              );
            } else if (snapshot.hasError) {
              return Column(
                children: [
                  Center(
                      child: Image.asset(
                    "assets/images/erreur.png",
                    width: 100,
                    height: 100,
                  )),
                  Text("Probleme de Serveur veillez ressayer plus tard"),
                ],
              );
            } else if (snapshot.data == "600") {
              return Column(
                children: [
                  Center(
                    child: Image.asset(
                      "assets/images/erreur.png",
                      width: 100,
                      height: 100,
                    ),
                  ),
                  Text("pas de connexion a internet"),
                ],
              );
            } else if (snapshot.data == "500") {
              return Column(
                children: [
                  Center(
                      child: Image.asset(
                    "assets/images/erreur.png",
                    width: 100,
                    height: 100,
                  )),
                  Text("Probleme de Serveur veillez ressayer plus tard"),
                ],
              );
            } else {
              return Container(
                width: size.width,
                padding: EdgeInsets.only(
                    left: kDefaultPadding,
                    top: kDefaultPadding,
                    bottom: kDefaultPadding),
                child: ListView.builder(
                  scrollDirection: Axis.vertical,
                  shrinkWrap: true,
                  reverse: true,
                  physics: NeverScrollableScrollPhysics(),
                  itemCount: snapshot.data.length,
                  itemBuilder: (context, index) {
                    return SingleMessage(snapshot.data[index]);
                  },
                ),
              );
            }
          },
        ),
      ),
    );
  }
}

class SingleMessage extends StatefulWidget {
  SingleMessage(this.messageData);
  MessageData messageData;

  @override
  State<SingleMessage> createState() => _SingleMessageState();
}

class _SingleMessageState extends State<SingleMessage> {
  bool visible = false;
  String text = "Afficher les reponses";
  void changeVisible() {
    if (visible == false) {
      setState(() {
        visible = true;
        text = "Masquer les reponses";
      });
    } else {
      setState(() {
        visible = false;
        text = "Afficher les reponses";
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return FutureBuilder(
      future:
          CurrentUser().findUserByid(widget.messageData.createdBy.toString()),
      builder: (context, snapshot) {
        if (!snapshot.hasData) {
          return Container(
            // height: size.height,
            color: Colors.white,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Center(
                    child: Image.asset(
                  "assets/images/donut.gif",
                  width: 10,
                  height: 10,
                )),
              ],
            ),
          );
        } else if (snapshot.hasError) {
          return Column(
            children: [
              Center(
                  child: Image.asset(
                "assets/images/erreur.png",
                width: 100,
                height: 100,
              )),
              Text("Probleme de Serveur veillez ressayer plus tard"),
            ],
          );
        } else if (snapshot.data == "600") {
          return Column(
            children: [
              Center(
                child: Image.asset(
                  "assets/images/erreur.png",
                  width: 100,
                  height: 100,
                ),
              ),
              Text("pas de connexion a internet"),
            ],
          );
        } else if (snapshot.data == "500") {
          return Column(
            children: [
              Center(
                  child: Image.asset(
                "assets/images/erreur.png",
                width: 100,
                height: 100,
              )),
              Text("Probleme de Serveur veillez ressayer plus tard"),
            ],
          );
        } else {
          return Column(
            children: [
              Container(
                margin: EdgeInsets.only(top: kDefaultPadding),
                child: Row(
                  children: [
                    CircleAvatar(
                      child: Text(
                          snapshot.data == "400" ? "?" : snapshot.data.name[0],
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: 25)),
                      radius: 20,
                    ),
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: 10),
                      padding: EdgeInsets.all(10),
                      width: size.width * 0.6,
                      decoration: BoxDecoration(
                        border: Border.all(
                          width: 0.9,
                          color: Colors.grey.shade200,
                        ),
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            snapshot.data == "400"
                                ? ". . . . "
                                : snapshot.data.name,
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 10),
                          ),
                          Padding(padding: EdgeInsets.only(top: 5)),
                          Text(
                            widget.messageData.message,
                            textAlign: TextAlign.justify,
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
              widget.messageData.reponses.length != 0
                  ? Container(
                      margin: EdgeInsets.only(top: 0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          TextButton(
                              onPressed: () {
                                changeVisible();
                              },
                              child: Text(
                                text,
                                style: TextStyle(
                                    color: Colors.black, fontSize: 10),
                              )),
                          // Text("jdkd"),
                          Visibility(
                              visible: visible,
                              child: ListView.builder(
                                scrollDirection: Axis.vertical,
                                shrinkWrap: true,
                                reverse: true,
                                physics: NeverScrollableScrollPhysics(),
                                itemCount: widget.messageData.reponses.length,
                                itemBuilder: (context, index) {
                                  return SingleReponse(
                                      widget.messageData.reponses[index]);
                                },
                              ))
                        ],
                      ),
                    )
                  : Container(
                      height: 0,
                    )
            ],
          );
        }
      },
    );
  }
}

class SingleReponse extends StatelessWidget {
  SingleReponse(this.messageData);
  Reponses messageData;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.only(left: size.width * 0.25),
      child: Row(
        children: [
          Container(
            margin: EdgeInsets.symmetric(horizontal: 10),
            padding: EdgeInsets.all(10),
            width: size.width * 0.6,
            decoration: BoxDecoration(
              border: Border.all(
                width: 0.9,
                color: Colors.grey.shade200,
              ),
              color: Colors.pink.shade200,
              borderRadius: BorderRadius.circular(10),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Rapidos Cameroun",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 10),
                ),
                Padding(padding: EdgeInsets.only(top: 5)),
                Text(
                  messageData.reponse,
                  textAlign: TextAlign.justify,
                ),
              ],
            ),
          ),
          // CircleAvatar(
          //   backgroundColor: Colors.pink.shade200,
          //   child: Text("R",
          //       style: TextStyle(
          //           color: Colors.white,
          //           fontWeight: FontWeight.bold,
          //           fontSize: 25)),
          //   radius: 20,
          // ),
        ],
      ),
    );
  }
}

class TextFieldContainer extends StatelessWidget {
  final Widget child;
  const TextFieldContainer({
    Key key,
    this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 10),
      padding: EdgeInsets.only(left: 10),
      width: size.width * 0.6,
      decoration: BoxDecoration(
        border: Border.all(
          width: 0.2,
          color: Colors.grey.shade100,
        ),
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
      ),
      child: child,
    );
  }
}
