import 'package:flutter/material.dart';
import 'package:rapidos_mobile/components/pdf/model/invoice.dart';
import 'package:rapidos_mobile/components/pdf/widget/button_widget.dart';
import 'package:rapidos_mobile/components/pdf/widget/title_widget.dart';

class Test5 extends StatefulWidget {
  Test5();

  @override
  State<Test5> createState() => _Test5State();
}

class _Test5State extends State<Test5> {
  String nomResto, idResto;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        title: Text('test pdf', style: Theme.of(context).textTheme.headline6),
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: Container(
        padding: EdgeInsets.all(32),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              TitleWidget(
                icon: Icons.picture_as_pdf,
                text: 'Generate Invoice',
              ),
              const SizedBox(height: 48),
              ButtonWidget(
                text: 'Invoice PDF',
                onClicked: () async {
                  final date = DateTime.now();
                  final dueDate = date.add(Duration(days: 7));

                  final invoice = Invoice(
                    // supplier: Supplier(
                    //   name: 'Sarah Field',
                    //   address: 'Sarah Street 9, Beijing, China',
                    //   paymentInfo: 'https://paypal.me/sarahfieldzz',
                    // ),
                    // customer: Customer(
                    //   name: 'Apple Inc.',
                    //   address: 'Apple Street, Cupertino, CA 95014',
                    // ),
                    // info: InvoiceInfo(
                    //   date: date,
                    //   dueDate: dueDate,
                    //   description: 'My description...',
                    //   number: '${DateTime.now().year}-9999',
                    // ),
                    items: [
                      // InvoiceItem(
                      //   description: 'Coffee',
                      //   date: DateTime.now(),
                      //   quantity: 3,
                      //   vat: 0.19,
                      //   unitPrice: 5.99,
                      // ),
                      // InvoiceItem(
                      //   description: 'Water',
                      //   date: DateTime.now(),
                      //   quantity: 8,
                      //   vat: 0.19,
                      //   unitPrice: 0.99,
                      // ),
                      // InvoiceItem(
                      //   description: 'Orange',
                      //   date: DateTime.now(),
                      //   quantity: 3,
                      //   vat: 0.19,
                      //   unitPrice: 2.99,
                      // ),
                      // InvoiceItem(
                      //   description: 'Apple',
                      //   date: DateTime.now(),
                      //   quantity: 8,
                      //   vat: 0.19,
                      //   unitPrice: 3.99,
                      // ),
                      // InvoiceItem(
                      //   description: 'Mango',
                      //   date: DateTime.now(),
                      //   quantity: 1,
                      //   vat: 0.19,
                      //   unitPrice: 1.59,
                      // ),
                      // InvoiceItem(
                      //   description: 'Blue Berries',
                      //   date: DateTime.now(),
                      //   quantity: 5,
                      //   vat: 0.19,
                      //   unitPrice: 0.99,
                      // ),
                      // InvoiceItem(
                      //   description: 'Lemon',
                      //   date: DateTime.now(),
                      //   quantity: 4,
                      //   vat: 0.19,
                      //   unitPrice: 1.29,
                      // ),
                    ],
                  );

                  // final pdfFile = await PdfInvoiceApi.generate(invoice);

                  // PdfApi.openFile(pdfFile);x
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  //           ));
  // }

  AppBar buildAppBar(BuildContext context) {
    return AppBar(
      centerTitle: true,
      title: Text("map ooo"),
      // leading: IconButton(
      //     color: Colors.red,
      //     onPressed: () {
      //       Navigator.of(context)
      //           .push(new MaterialPageRoute(builder: (context) => YouCart()));
      //     },
      //     icon: Icon(
      //       Icons.arrow_back_ios_sharp,
      //       size: 30,
      //     )),
    );
  }
}

class TestSliverAppBar extends StatelessWidget {
  const TestSliverAppBar({
    Key key,
    @required bool pinned,
    @required bool snap,
    @required bool floating,
  })  : _pinned = pinned,
        _snap = snap,
        _floating = floating,
        super(key: key);

  final bool _pinned;
  final bool _snap;
  final bool _floating;

  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      slivers: <Widget>[
        SliverAppBar(
          pinned: _pinned,
          snap: _snap,
          floating: _floating,
          expandedHeight: 160.0,
          flexibleSpace: const FlexibleSpaceBar(
            title: Text('SliverAppBar'),
            background: FlutterLogo(),
          ),
        ),
        const SliverToBoxAdapter(
          child: SizedBox(
            height: 20,
            child: Center(
              child: Text('Scroll to see the SliverAppBar in effect.'),
            ),
          ),
        ),
        SliverList(
          delegate: SliverChildBuilderDelegate(
            (BuildContext context, int index) {
              return Container(
                color: index.isOdd ? Colors.white : Colors.black12,
                height: 100.0,
                child: Center(
                  child: Text('$index', textScaleFactor: 5),
                ),
              );
            },
            childCount: 20,
          ),
        ),
      ],
    );
  }
}
