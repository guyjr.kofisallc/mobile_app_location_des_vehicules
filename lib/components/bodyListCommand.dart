import 'package:flutter/material.dart';
import 'package:flutter_imagenetwork/flutter_imagenetwork.dart';
import 'package:rapidos_mobile/components/command_final.dart';
import 'package:rapidos_mobile/components/detail_list_commande.dart';

import '../constants.dart';
import '../entities/listeCommandeModel.dart';
import '../services/listCommandeServices.dart';

class BodyListCommande extends StatefulWidget {
  const BodyListCommande({
    Key key,
  }) : super(key: key);

  @override
  State<BodyListCommande> createState() => _BodyListCommandeState();
}

class _BodyListCommandeState extends State<BodyListCommande> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      height: size.height,
      padding: EdgeInsets.all(kDefaultPadding),
      child: SingleChildScrollView(
          child: Column(
        children: [
          // SingleCommande(size: size),
          Container(
            color: Colors.white,
            child: FutureBuilder(
              future: ListCommandeServices().afficheListCommande(),
              builder: (context, snapshot) {
                if (!snapshot.hasData) {
                  return Container(
                    // height: size.height,
                    color: Colors.white,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Center(
                            child: Image.asset(
                          "assets/images/donut.gif",
                          width: 150,
                          height: 150,
                        )),
                      ],
                    ),
                  );
                } else if (snapshot.hasError) {
                  return Column(
                    children: [
                      Center(
                          child: Image.asset(
                        "assets/images/erreur.png",
                        width: 100,
                        height: 100,
                      )),
                      Text("Probleme de Serveur veillez ressayer plus tard"),
                    ],
                  );
                } else if (snapshot.data == "600") {
                  return Column(
                    children: [
                      Center(
                        child: Image.asset(
                          "assets/images/erreur.png",
                          width: 100,
                          height: 100,
                        ),
                      ),
                      Text("pas de connexion a internet"),
                    ],
                  );
                } else if (snapshot.data == "500") {
                  return Column(
                    children: [
                      Center(
                          child: Image.asset(
                        "assets/images/erreur.png",
                        width: 100,
                        height: 100,
                      )),
                      Text("Probleme de Serveur veillez ressayer plus tard"),
                    ],
                  );
                } else {
                  if (snapshot.data.length == 0) {
                    return Text("Vous n'avez passer aucune commande!!!");
                  } else {
                    return Container(
                      child: ListView.separated(
                        separatorBuilder: (_, m) => Divider(),
                        scrollDirection: Axis.vertical,
                        reverse: true,
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        itemCount: snapshot.data.length,
                        itemBuilder: (context, index) {
                          return SingleCommande(snapshot.data[index]);
                        },
                      ),
                    );
                  }
                }
              },
            ),
          )
        ],
      )),
    );
  }
}

class SingleCommande extends StatelessWidget {
  SingleCommande(this.commande);
  ListCommandeModel commande;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return InkWell(
      onTap: () {
        Navigator.of(context).push(new MaterialPageRoute(
            builder: (context) => DetailListCommande(commande)));
      },
      child: Stack(
        children: [
          ClipRRect(
            //un peu comme un conteneur mais avec border radius
            borderRadius: BorderRadius.circular(9.0),
            child: AjanuwImage(
              image: AjanuwNetworkImage(commande.resto["photo"]),
              width: size.width - (kDefaultPadding * 2),
              height: 170,
              fit: BoxFit.cover,
              loadingWidget: Center(
                child: Image.asset(
                  "assets/images/restoAwait.png",
                  width: 50,
                  height: 50,
                  // color: Colors.red,
                ),
              ),
              // loadingBuilder: AjanuwImage.defaultLoadingBuilder,
              errorBuilder: AjanuwImage.defaultErrorBuilder,
            ),
          ),
          Container(
            padding: EdgeInsets.only(
                left: kDefaultPadding, right: kDefaultPadding, bottom: 10),
            width: size.width - (kDefaultPadding * 2),
            height: 170,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(9),
              color: Colors.black.withOpacity(0.6),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  // "Raphaelo",
                  commande.resto["name"],
                  style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.bold,
                      color: Colors.white),
                  textAlign: TextAlign.center,
                ),
                Spacer(),
                RowElement(
                    "Date",
                    DateTime.fromMillisecondsSinceEpoch(
                            commande.createdAt.toInt() * 1000)
                        .toString()),
                RowElement("Adresse de livraison", "A ma position"),
                Padding(padding: EdgeInsets.only(top: 2)),
                RowElement("Numero de Payement", "+ ${commande.numero}"),
                Padding(padding: EdgeInsets.only(top: 5)),
                RowElement("Etat de livraison", commande.etatLivraison),
                Padding(padding: EdgeInsets.only(top: 5)),
                RowElement(
                    "Nombre de Produit", commande.cartItems.length.toString()),
                Spacer(),
                RowElement("Prix total",
                    "${commande.total + commande.livraison} FCFA"),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class RowElement extends StatelessWidget {
  RowElement(this.title, this.subtitle);
  String title, subtitle;
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Text(
          title,
          style: TextStyle(
              fontSize: 10, fontWeight: FontWeight.bold, color: Colors.white),
          textAlign: TextAlign.center,
        ),
        Spacer(),
        Text(
          subtitle == null ? "ok" : subtitle,
          style: TextStyle(
              fontSize: 10, fontWeight: FontWeight.w400, color: Colors.white),
          textAlign: TextAlign.center,
        ),
      ],
    );
  }
}
