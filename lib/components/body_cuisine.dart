import 'package:flutter/material.dart';
import 'package:flutter_imagenetwork/flutter_imagenetwork.dart';
import 'package:rapidos_mobile/components/cuisine_detail.dart';
import 'package:rapidos_mobile/services/afficheCat.dart';
import 'package:rapidos_mobile/constants.dart';

class BodyCuisine extends StatelessWidget {
  BodyCuisine();

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      child: Column(
        children: [
          Expanded(
              child: FutureBuilder(
            future: AfficheCategorie().fetchNewArticle(),
            builder: (context, snapshot) {
              if (!snapshot.hasData) {
                return Container(
                  height: size.height,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Center(
                          child: Image.asset(
                        "assets/images/donut.gif",
                        width: 150,
                        height: 150,
                      )),
                    ],
                  ),
                );
              } else {
                return Container(
                  padding: EdgeInsets.all(kDefaultPadding),
                  child: GridView.builder(
                      gridDelegate:
                          const SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisSpacing: 10,
                        mainAxisSpacing: 8,
                        crossAxisCount: 2,
                      ),
                      itemCount: snapshot.data.length,
                      itemBuilder: (context, index) {
                        String img, name;
                        if (snapshot.data[index].image == "" ||
                            snapshot.data[index].image == "sdasda") {
                          img =
                              "https://assets.afcdn.com/recipe/20190704/94666_w1024h768c1cx2689cy1920.jpg";
                          name = "no name";
                        } else {
                          // img = snapshot.data[index].image;
                          img = snapshot.data[index].image;
                          name = snapshot.data[index].libelle;
                        }
                        return InkWell(
                            onTap: () {
                              Navigator.of(context).push(new MaterialPageRoute(
                                  builder: (context) => DetailCuisine(
                                      snapshot.data[index].libelle)));
                            },
                            child: Container(
                              height: size.height,
                              width: size.width / 2,
                              child: Stack(
                                children: [
                                  ClipRRect(
                                    //un peu comme un conteneur mais avec border radius
                                    borderRadius: BorderRadius.circular(9.0),
                                    child: AjanuwImage(
                                      image: AjanuwNetworkImage(img),
                                      height: size.height,
                                      width: size.width / 2,
                                      fit: BoxFit.cover,
                                      loadingWidget: Container(
                                        height: size.height,
                                        width: size.width / 2,
                                        child: Center(
                                          child: Image.asset(
                                            "assets/images/cloche.png",
                                            width: 50,
                                            height: 50,
                                            // color: Colors.red,
                                          ),
                                        ),
                                      ),
                                      // loadingBuilder: AjanuwImage.defaultLoadingBuilder,
                                      errorBuilder:
                                          AjanuwImage.defaultErrorBuilder,
                                    ),
                                  ),
                                  Container(
                                    height: size.height,
                                    width: size.width / 2,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(9),
                                      color: Colors.black.withOpacity(0.5),
                                    ),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Text(
                                          name,
                                          style: text4bb,
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ));

                        // return  ListTile(
                        // title: Text(snapshot.data[index].libelle),
                        //
                      }),
                );
              }
            },
          ))
        ],
      ),
    );
  }
}
