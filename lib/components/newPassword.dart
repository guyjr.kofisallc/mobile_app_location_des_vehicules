import 'package:flutter/material.dart';

import 'bodyNewPassword.dart';

class NewPassword extends StatelessWidget {
  NewPassword(this.email);
  String email;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(context),
      body: BodyNewPassword(email),
    );
  }

  AppBar buildAppBar(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.grey.shade50,
      centerTitle: true,
      // title: Text(
      //   "Connexion",
      //   style: TextStyle(
      //     color: Colors.black,
      //     fontWeight: FontWeight.bold,
      //   ),
      // ),
      shadowColor: Colors.transparent,
      iconTheme: IconThemeData(color: Colors.black),
      // leading: Container(
      //   width: 20,
      //   height: 35,
      //   decoration: BoxDecoration(
      //     color: kPrimaryColorgrey,
      //     borderRadius: BorderRadius.circular(60.0),
      //   ),
      //   child: IconButton(
      //       color: Colors.black,
      //       onPressed: () {},
      //       icon: Icon(
      //         Icons.arrow_back_ios_sharp,
      //         size: 30,
      //       )),
      // ),
    );
  }
}
