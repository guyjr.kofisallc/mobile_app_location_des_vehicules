import 'package:flutter/material.dart';
import 'package:flutter_cart/flutter_cart.dart';
import 'package:rapidos_mobile/constants.dart';
import 'package:rapidos_mobile/entities/allProduit.dart';
import 'package:rapidos_mobile/entities/restaurants.dart';
import 'body_single_product.dart';

class SingleProduct extends StatelessWidget {
  SingleProduct(
      this.produit,
      //this.cart,
      this.nbreProduit,
      this.totalQte,
      this.resto);
  Produit produit;
  Restaurant resto;
  int nbreProduit;
  double totalQte;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey.shade300,
      extendBodyBehindAppBar: true,
      body: BodySingleProduct(
          produit,
          //cart,
          nbreProduit,
          totalQte,
          resto),
      appBar: buildAppBar(context),
      //bottomNavigationBar: FooterSingleProduct(),
    );
  }

  AppBar buildAppBar(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.transparent,
      elevation: 0,
      centerTitle: true,
      iconTheme: themeIcon,
      // title: Text(
      //   "Adresse de livraison",
      //   style: TextStyle(
      //     color: Colors.black,
      //     fontWeight: FontWeight.bold,
      //   ),
      // ),
    );
  }
}
