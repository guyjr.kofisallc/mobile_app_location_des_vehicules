import 'package:flutter/material.dart';
import 'package:rapidos_mobile/components/connexion.dart';
import 'package:rapidos_mobile/components/verificationNumber.dart';
import 'package:rapidos_mobile/services/passwordVerify.dart';

import '../constants.dart';
import 'drawer_client.dart';

class BodyNewPassword extends StatefulWidget {
  BodyNewPassword(this.email);
  String email;

  @override
  _BodyNewPasswordState createState() => _BodyNewPasswordState();
}

class _BodyNewPasswordState extends State<BodyNewPassword> {
  bool visible = true;
  bool isChecked = false;
  void rendVisble() {
    setState(() {
      if (visible == true) {
        visible = false;
      } else {
        visible = true;
      }
    });
  }

  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _passwordConfirmController =
      TextEditingController();
  final _formKey = GlobalKey<FormState>();
  void _loadingtoText() {
    setState(() {
      setState(() {
        _widget = Text(
          "Recuperer",
          style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
        );
      });
    });
  }

  void _textToLoading() {
    setState(() {
      _widget = Center(
        child: CircularProgressIndicator(
          strokeWidth: 2,
          color: Colors.white,
        ),
      );
    });
  }

  Widget _widget = Text(
    "Recuperer",
    style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
  );

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      height: size.height,
      child: SingleChildScrollView(
        padding: EdgeInsets.all(kDefaultPadding),
        child: Column(
          children: [
            Form(
              key: _formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Choisir son nouveau",
                    style: TextStyle(
                      fontFamily: font,
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      fontStyle: FontStyle.normal,
                    ),
                    textAlign: TextAlign.left,
                  ),
                  Text(
                    "mot de passe",
                    style: TextStyle(
                      fontFamily: font,
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      fontStyle: FontStyle.normal,
                    ),
                    textAlign: TextAlign.left,
                  ),
                  Padding(padding: EdgeInsets.all(5)),
                  Text(
                    " Veillez Entrer Votre mots de passe",
                    style: TextStyle(
                      fontFamily: font,
                      fontSize: 13,
                      // fontWeight: FontWeight.bold,
                      fontStyle: FontStyle.normal,
                    ),
                    textAlign: TextAlign.left,
                  ),
                  Padding(padding: EdgeInsets.all(kDefaultPadding)),
                  Text(
                    "Mot de passe",
                    style: TextStyle(
                      fontFamily: font,
                      fontSize: 13,
                      // fontWeight: FontWeight.bold,
                      fontStyle: FontStyle.normal,
                    ),
                    textAlign: TextAlign.left,
                  ),
                  TextFieldContainer(
                    child: TextFormField(
                      validator: (value) {},
                      controller: _passwordController,
                      obscureText: visible,
                      onChanged: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Entrer Votre mot de passe ';
                        }
                        return null;
                      },
                      decoration: InputDecoration(
                        fillColor: Colors.grey,
                        suffixIcon: IconButton(
                          onPressed: () {
                            rendVisble();
                          },
                          icon: Icon(
                            Icons.visibility,
                            color: kPrimaryColor,
                            size: 20,
                          ),
                          color: Color.fromRGBO(250, 25, 47, 1),
                        ),
                        border: InputBorder.none,
                      ),
                    ),
                  ),
                  Text(
                    "Confirmer mot de passe",
                    style: TextStyle(
                      fontFamily: font,
                      fontSize: 13,
                      // fontWeight: FontWeight.bold,
                      fontStyle: FontStyle.normal,
                    ),
                    textAlign: TextAlign.left,
                  ),
                  TextFieldContainer(
                    child: TextFormField(
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Entrer Votre verification mot de passe ';
                        }
                        return null;
                      },
                      controller: _passwordConfirmController,
                      obscureText: visible,
                      onChanged: (value) {},
                      decoration: InputDecoration(
                        fillColor: Colors.grey,
                        suffixIcon: IconButton(
                          onPressed: () {
                            rendVisble();
                          },
                          icon: Icon(
                            Icons.visibility,
                            color: kPrimaryColor,
                            size: 20,
                          ),
                          color: Color.fromRGBO(250, 25, 47, 1),
                        ),
                        border: InputBorder.none,
                      ),
                    ),
                  ),
                  Padding(padding: EdgeInsets.all(kDefaultPadding)),
                  Container(
                    decoration: BoxDecoration(
                      color: kPrimaryColor,
                      border: Border.all(
                        width: 3,
                        color: kPrimaryColor,
                      ),
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    child: Material(
                      color: kPrimaryColor,
                      borderRadius: BorderRadius.circular(10.0),
                      // button color
                      child: InkWell(
                        splashColor: Colors
                            .grey, // Couleur qui "envahit" le bouton lors du focus
                        child: SizedBox(
                          width: size.width - (kDefaultPadding * 2),
                          height: 40,
                          child: Center(child: _widget),
                        ),
                        onTap: () async {
                          if (_formKey.currentState.validate()) {
                            if (_passwordConfirmController.text ==
                                _passwordController.text) {
                              _textToLoading();
                              var response = await PasswordVerify()
                                  .modifyPassword(
                                      widget.email, _passwordController.text);
                              if (response.toString() == "500") {
                                _loadingtoText();
                                displayDialog(
                                    context,
                                    "Probleme avec le serveur , veillez Essayer plus tard",
                                    "");
                              }
                              if (response.toString() == "406") {
                                _loadingtoText();
                                displayDialog(
                                    context,
                                    "Votre Compte n'existe pas veillez vous inscrire s'il vous plait",
                                    "");
                              } else if (response.toString() == "600") {
                                _loadingtoText();
                                displayDialog(
                                    context,
                                    "Vous n'etes pas connecter a internet , veillez Essayer plus tard",
                                    "");
                              } else if (response.toString() == "200") {
                                _loadingtoText();
                                Navigator.of(context).push(
                                    new MaterialPageRoute(
                                        builder: (context) => Connexion()));
                                return null;
                              } else {
                                _loadingtoText();
                                displayDialog(
                                    context,
                                    "une erreur c'est produite , veillez Essayer plus tard",
                                    "");
                              }
                            } else {
                              _loadingtoText();
                              displayDialog(context,
                                  "Erreur de conformation du mot de passe", "");
                            }
                          }
                        },
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

class TextFieldContainer extends StatelessWidget {
  final Widget child;
  const TextFieldContainer({
    Key key,
    this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 2),
      width: size.width - (kDefaultPadding * 2),
      decoration: BoxDecoration(
        border: Border.all(
          width: 2,
          color: Colors.grey,
        ),
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
      ),
      child: child,
    );
  }
}
