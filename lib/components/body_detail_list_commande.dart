import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:barcode_widget/barcode_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:rapidos_mobile/entities/listeCommandeModel.dart';
import '../constants.dart';
import 'conteur_rebout.dart';
import 'single_element_commade_final.dart';

class BodyDetailListCommande extends StatefulWidget {
  BodyDetailListCommande(this.commande);
  ListCommandeModel commande;

  @override
  State<BodyDetailListCommande> createState() => _BodyDetailListestoState();
}

class _BodyDetailListestoState extends State<BodyDetailListCommande> {
  // String formattedDate = formatter.format(now);
  @override
  Widget build(BuildContext context) {
    var now = new DateTime.now();
    Size size = MediaQuery.of(context).size;
    Duration ajout = new Duration(minutes: 40);
    String temps = DateTime.fromMillisecondsSinceEpoch(
                widget.commande.createdAt.toInt() * 1000)
            .add(ajout)
            .hour
            .toString() +
        "h " +
        now.add(ajout).minute.toString() +
        "min";
    String lieu = widget.commande.address;

    return SingleChildScrollView(
      child: Container(
        color: Colors.grey.shade200,
        child: Column(children: <Widget>[
          //Padding(padding: EdgeInsets.only(top: 0)),
          //TitleWithMoreBtn(title: 'Votre Commande'),
          Padding(padding: EdgeInsets.only(top: 5)),
          widget.commande.etatLivraison == "EN_COURS"
              ? Column(
                  children: [
                    SingleElement(
                      size: size,
                      titre: temps,
                      sousTitre: "Estimation d'arrivée",
                    ),
                    CompteRebour(),
                    Padding(padding: EdgeInsets.only(top: 20)),
                    ListTile(
                      title: AnimatedTextKit(
                        animatedTexts: [
                          TypewriterAnimatedText(
                            'Préparation en cours....',
                            textStyle: const TextStyle(
                              fontSize: 18.0,
                              fontFamily: 'DayRoman',
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                        totalRepeatCount: 5200,
                        pause: const Duration(milliseconds: 1000),
                        displayFullTextOnTap: true,
                        stopPauseOnTap: true,
                      ),
                      subtitle: Text(
                        "Arrivée au plus tard dans 40 min",
                        style: TextStyle(
                          fontFamily: 'DayRoman',
                        ),
                      ),
                    ),
                    SvgPicture.asset(
                      "assets/images/cook.svg",
                      height: 185,
                      width: 185,
                    ),
                  ],
                )
              : Text("Commande est ${widget.commande.etatLivraison}"),
          Padding(padding: EdgeInsets.only(top: 20)),
          Container(
            color: Colors.grey.shade200,
            child: Column(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(
                      left: kDefaultPadding,
                      right: kDefaultPadding,
                      top: kDefaultPadding,
                      bottom: kDefaultPadding),
                  color: Colors.white,
                  child: Column(
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text("Détails de livraison",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 18,
                                fontFamily: 'DayRoman',
                              )),
                        ],
                      ),
                      Padding(padding: EdgeInsets.only(top: 5)),
                      Row(
                        children: [
                          Column(
                            children: [
                              TitrePlusDetail(
                                titre: "Adresse :",
                                detail: lieu,
                              ),
                              Padding(padding: EdgeInsets.only(top: 15)),
                              TitrePlusDetail(
                                titre: "Types",
                                detail: "Livraison à Domicile",
                              ),
                              Padding(padding: EdgeInsets.only(top: 15)),
                              TitrePlusDetail(
                                titre: "Payement",
                                detail: "+ ${widget.commande.numero}",
                              ),
                            ],
                          ),
                          Spacer(),
                          Container(
                              height: 90,
                              width: 90,
                              child: BarcodeWidget(
                                barcode: Barcode.qrCode(),
                                data: widget.commande.referencePayement,
                                errorBuilder: (context, error) =>
                                    Center(child: Text(error)),
                              )),
                        ],
                      )
                    ],
                  ),
                ),
                Container()
              ],
            ),
          ),
          Padding(padding: EdgeInsets.only(top: 10)),
          Container(
            color: Colors.grey.shade200,
            child: Column(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(
                    left: kDefaultPadding,
                    right: kDefaultPadding,
                    top: kDefaultPadding,
                    bottom: kDefaultPadding,
                  ),
                  color: Colors.white,
                  child: Column(
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text("Récapitulatif de la commande",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 15,
                                color: Colors.red,
                                fontFamily: 'DayRoman',
                              )),
                        ],
                      ),
                      Padding(padding: EdgeInsets.only(top: 5)),
                      TitrePlusDetail(
                        titre: widget.commande.resto["name"],
                      ),
                      Padding(padding: EdgeInsets.only(top: 15)),
                      Container(
                        child: ListView.builder(
                            itemCount: widget.commande.cartItems.length,
                            scrollDirection: Axis.vertical,
                            shrinkWrap: true,
                            physics: NeverScrollableScrollPhysics(),
                            itemBuilder: (context, index) {
                              dynamic totalQte =
                                  widget.commande.cartItems[index].price;
                              return Container(
                                color: Colors.grey.shade200,
                                child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    ListTile(
                                      contentPadding:
                                          EdgeInsets.only(left: 5, right: 5),
                                      // leading: Container(
                                      //   width: 95,
                                      //   child: ClipRRect(
                                      //       //un peu comme un conteneur mais avec border radius
                                      //       borderRadius:
                                      //           BorderRadius.circular(9.0),
                                      //       child: Image.network(
                                      //         cart.cartItem[index]
                                      //             .productDetails,
                                      //         fit: BoxFit.cover,
                                      //       )),
                                      // ),
                                      title: Text(
                                        widget.commande.cartItems[index]
                                            .produit["designation"]
                                            .toString(),
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontWeight: FontWeight.bold),
                                      ),
                                      minVerticalPadding: 0,
                                      subtitle: Row(
                                        children: <Widget>[
                                          Text(
                                            widget.commande.cartItems[index]
                                                .quantite
                                                .toString(),
                                            style: TextStyle(
                                                color: kPrimaryColor,
                                                fontSize: 20),
                                          ),
                                        ],
                                      ),
                                      trailing: Text(
                                        widget.commande.cartItems[index].price
                                                .toString() +
                                            " XAF",
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            color: kPrimaryColor),
                                      ),
                                    ),
                                    Padding(
                                        padding: const EdgeInsets.only(top: 1)),
                                  ],
                                ),
                              );
                            }),
                      ),
                      ListTile(
                        contentPadding: EdgeInsets.only(left: 5, right: 5),
                        title: Text(
                          "Livraison",
                          style: TextStyle(
                              color: Colors.black, fontWeight: FontWeight.bold),
                        ),
                        minVerticalPadding: 0,
                        // subtitle: Row(
                        //   children: <Widget>[
                        //     Text(
                        //       cart.cartItem[index].quantity
                        //           .toString(),
                        //       style: TextStyle(
                        //           color: Colors.red,
                        //           fontSize: 20),
                        //     ),
                        //   ],
                        // ),
                        trailing: Text(
                          " ${widget.commande.livraison} XAF",
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: kPrimaryColor),
                        ),
                      ),
                      Padding(padding: EdgeInsets.only(top: 20)),
                      Container(
                        decoration: BoxDecoration(
                            border: Border(
                          top: BorderSide(
                            color: Colors.black,
                          ),
                        )),
                        padding: const EdgeInsets.only(
                          top: 15,
                        ),
                        child: Row(
                          children: <Widget>[
                            Container(
                              padding:
                                  const EdgeInsets.only(left: kDefaultPadding),
                              width: (size.width / 2) - kDefaultPadding,
                              child: Text(
                                "TOTAL : ",
                                style: TextStyle(
                                    fontFamily: 'DayRoman',
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 20),
                              ),
                            ),
                            //Padding(padding: const EdgeInsets.only(left: kDefaultPadding)),
                            Container(
                                width: (size.width / 2) - kDefaultPadding,
                                padding: const EdgeInsets.only(
                                    right: kDefaultPadding),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  verticalDirection: VerticalDirection.down,
                                  children: [
                                    Text(
                                      "${widget.commande.total + widget.commande.livraison}" +
                                          " XAF",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          // fontFamily: 'DayRoman',
                                          color: kPrimaryColor),
                                    ),
                                  ],
                                )),
                          ],
                        ),
                      ),
                      Padding(padding: EdgeInsets.only(top: 35)),
                    ],
                  ),
                ),
              ],
            ),
          )
        ]),
      ),
    );
  }
}

class SingleProduitFactureFinal extends StatelessWidget {
  SingleProduitFactureFinal(
      {this.image, this.nom, this.qte, this.totalPrixQte});
  String nom, image;
  int qte, totalPrixQte;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.grey.shade200,
      padding: EdgeInsets.only(bottom: kDefaultPadding - 10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          ListTile(
            contentPadding: EdgeInsets.all(kDefaultPadding),
            leading: Container(
              width: 95,
              child: ClipRRect(
                //un peu comme un conteneur mais avec border radius
                borderRadius: BorderRadius.circular(9.0),
                child: Image.asset(
                  image,
                  fit: BoxFit.cover,
                ),
              ),
            ),
            title: Text(
              "$nom",
              style:
                  TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
            ),
            minVerticalPadding: 0,
            subtitle: Row(
              children: <Widget>[
                Padding(padding: const EdgeInsets.only(top: 25)),
                Text(
                  "$qte",
                  style: TextStyle(color: kPrimaryColor, fontSize: 18),
                ),
              ],
            ),
            trailing: Text(
              "$totalPrixQte XAF",
              style:
                  TextStyle(fontWeight: FontWeight.bold, color: kPrimaryColor),
            ),
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                  padding: EdgeInsets.only(
                left: kDefaultPadding,
              )),
            ],
          ),
        ],
      ),
    );
  }
}

class TitrePlusDetail extends StatelessWidget {
  String titre, detail;

  TitrePlusDetail({
    this.detail = '',
    this.titre,
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Text("$titre",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 15,
                    fontFamily: 'DayRoman',
                  )),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                width: MediaQuery.of(context).size.width * 0.6,
                child: Text("$detail",
                    style: TextStyle(
                      fontWeight: FontWeight.normal,
                      fontSize: 13,
                      fontFamily: 'DayRoman',
                    )),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
