import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:rapidos_mobile/components/localisation.dart';
import 'package:url_launcher/url_launcher.Dart' as UrlLauncher;
import '../constants.dart';
import 'body_contactez_nous.dart';

class ContactezNous extends StatefulWidget {
  ContactezNous();

  @override
  _ContactezNousState createState() => _ContactezNousState();
}

class _ContactezNousState extends State<ContactezNous> {
  static const platform =
      const MethodChannel('flutter_contacts/launch_contacts');

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey.shade100,
      appBar: buildAppBar(context),
      body: BodyContactezNous(),
      //bottomNavigationBar: Footer_all(),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () async => UrlLauncher.launch("tel:+237680401646"),
        backgroundColor: kPrimaryColor,
        label: Text('Appelez-nous'),
        icon: Icon(Icons.phone),
      ),
    );
  }

  AppBar buildAppBar(BuildContext context) {
    return AppBar(
      centerTitle: true,
      iconTheme: IconThemeData(color: Colors.black),
      title: Text(
        "Contactez-nous",
        style: text3nb,
      ),
      actions: <Widget>[
        IconButton(
            color: kPrimaryColor,
            onPressed: () {
              Navigator.of(context)
                  .push(new MaterialPageRoute(builder: (context) => Map()));
            },
            icon: Icon(
              Icons.map_outlined,
              size: 30,
            )),
      ],
    );
  }

  void launchContacts() async {
    try {
      await platform.invokeMethod('launch');
    } on PlatformException catch (e) {
      print("Failed to launch contacts: ${e.message}");
    }
    setState(() {});
  }
}
