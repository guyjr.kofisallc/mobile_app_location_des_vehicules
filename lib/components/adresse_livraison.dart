import 'package:flutter/material.dart';
import 'package:rapidos_mobile/components/body_adresse_livraison.dart';
import 'package:rapidos_mobile/constants.dart';
import 'package:rapidos_mobile/entities/restaurants.dart';

// ignore: must_be_immutable
class AdresseLivraison extends StatelessWidget {
  AdresseLivraison(this.restaurant);
  // FlutterCart cart;
  Restaurant restaurant;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //drawer: (ClientDrawer()),
      appBar: buildAppBar(context),
      body: BodyAdresseLivraison(restaurant),
      //bottomNavigationBar: Footer_all(),
    );
  }

  AppBar buildAppBar(BuildContext context) {
    return AppBar(
      iconTheme: IconThemeData(color: Colors.black),
      centerTitle: true,
      title: Text(
        "Adresse de livraison",
        style: TextStyle(
          fontWeight: FontWeight.bold,
          fontFamily: font,
          color: Colors.black,
          fontSize: 16,
        ),
      ),
      // leading: IconButton(
      //     color: Colors.red,
      //     onPressed: () {
      //       Navigator.of(context)
      //           .push(new MaterialPageRoute(builder: (context) => YouCart()));
      //     },
      //     icon: Icon(
      //       Icons.arrow_back_ios_sharp,
      //       size: 30,
      //     )),
    );
  }
}
