import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:rapidos_mobile/constants.dart';
import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:rapidos_mobile/components/pdf/api/pdf_api.dart';
import 'package:rapidos_mobile/components/pdf/api/pdf_invoice_api.dart';
import 'package:rapidos_mobile/components/pdf/model/customer.dart';
import 'package:rapidos_mobile/components/pdf/model/invoice.dart';
import 'package:rapidos_mobile/components/pdf/model/supplier.dart';
import '../entities/Panier.dart';
import '../entities/currentUserData.dart';
import '../services/currentUser.dart';
import 'body_checkout_page.dart';
import 'conteur_rebout.dart';

class BodyCommandeFinal extends StatefulWidget {
  BodyCommandeFinal(this._adresseClient, this.cart, this.nomResto,
      this.payement, this.numero, this.reference, this.livraison);
  String _adresseClient;
  FlutterCart cart;
  int livraison;
  String nomResto, payement, numero, reference;

  @override
  State<BodyCommandeFinal> createState() => _BodyCommandeFinalState();
}

class _BodyCommandeFinalState extends State<BodyCommandeFinal> {
  Widget wi = Text(
    "Voir la facture(pdf)",
    style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
  );

  void changeTextToLoader() {
    setState(() {
      wi = CircularProgressIndicator(
        color: Colors.white,
      );
    });
  }

  void changeLoaderTextTo() {
    setState(() {
      wi = Text(
        "Valider",
        style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
      );
    });
  }

  // String formattedDate = formatter.format(now);
  @override
  Widget build(BuildContext context) {
    var now = new DateTime.now();
    Size size = MediaQuery.of(context).size;
    Duration ajout = new Duration(minutes: 40);
    String temps = now.add(ajout).hour.toString() +
        "h " +
        now.add(ajout).minute.toString() +
        "min";
    String lieu = widget._adresseClient.toString();

    double montantTotalCommande =
        widget.cart.getTotalAmount() + widget.livraison;

    return SingleChildScrollView(
      child: Container(
        color: Colors.grey.shade200,
        child: Column(children: <Widget>[
          //Padding(padding: EdgeInsets.only(top: 0)),
          //TitleWithMoreBtn(title: 'Votre Commande'),
          Padding(padding: EdgeInsets.only(top: 5)),
          SingleElement(
            size: size,
            titre: temps,
            sousTitre: "Estimation d'arrivée",
          ),
          CompteRebour(),
          Padding(padding: EdgeInsets.only(top: 20)),
          ListTile(
            title: AnimatedTextKit(
              animatedTexts: [
                TypewriterAnimatedText(
                  'Préparation en cours....',
                  textStyle: const TextStyle(
                    fontSize: 20.0,
                    fontFamily: 'DayRoman',
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
              totalRepeatCount: 5200,
              pause: const Duration(milliseconds: 1000),
              displayFullTextOnTap: true,
              stopPauseOnTap: true,
            ),
            subtitle: Text(
              "Arrivée au plus tard dans 40 min",
              style: TextStyle(
                fontFamily: 'DayRoman',
              ),
            ),
          ),
          SvgPicture.asset(
            "assets/images/cook.svg",
            height: 185,
            width: 185,
          ),
          Padding(padding: EdgeInsets.only(top: 20)),
          Container(
            color: Colors.grey.shade200,
            child: Column(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(
                      left: kDefaultPadding,
                      right: kDefaultPadding,
                      top: kDefaultPadding,
                      bottom: kDefaultPadding),
                  color: Colors.white,
                  child: Column(
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text("Détails de livraison",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 23,
                                fontFamily: 'DayRoman',
                              )),
                        ],
                      ),
                      Padding(padding: EdgeInsets.only(top: 5)),
                      TitrePlusDetail(
                        titre: "Adresse :",
                        detail: lieu,
                      ),
                      Padding(padding: EdgeInsets.only(top: 15)),
                      TitrePlusDetail(
                        titre: "Types",
                        detail: "Livraison à Domicile",
                      ),
                      Padding(padding: EdgeInsets.only(top: 15)),
                      TitrePlusDetail(
                        titre: "Payement",
                        detail: widget.payement,
                      ),
                      // NewWidget(widget.payement),
                      // Container(
                      //   height: 90,
                      //   child: Column(
                      //     crossAxisAlignment: CrossAxisAlignment.center,
                      //     mainAxisAlignment: MainAxisAlignment.center,
                      //     children: [
                      //       Center(
                      //         child: AnimatedTextKit(
                      //           animatedTexts: [
                      //             TyperAnimatedText(
                      //               "Veillez valider la transaction",
                      //               textStyle: TextStyle(
                      //                 fontFamily: font,
                      //                 fontSize: 15,
                      //                 color: Colors.black,
                      //                 fontStyle: FontStyle.italic,
                      //               ),
                      //               textAlign: TextAlign.center,
                      //             )
                      //           ],
                      //         ),
                      //       ),
                      //     ],
                      //   ),
                      // )
                    ],
                  ),
                ),
                Container()
              ],
            ),
          ),
          Padding(padding: EdgeInsets.only(top: 10)),
          Container(
            color: Colors.grey.shade200,
            child: Column(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(
                    left: kDefaultPadding,
                    right: kDefaultPadding,
                    top: kDefaultPadding,
                    bottom: kDefaultPadding,
                  ),
                  color: Colors.white,
                  child: Column(
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text("Récapitulatif de la commande",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 15,
                                color: Colors.red,
                                fontFamily: 'DayRoman',
                              )),
                        ],
                      ),
                      Padding(padding: EdgeInsets.only(top: 5)),
                      TitrePlusDetail(
                        titre: widget.nomResto,
                      ),
                      Padding(padding: EdgeInsets.only(top: 15)),
                      Container(
                        child: ListView.builder(
                            itemCount: widget.cart.cartItem.length,
                            scrollDirection: Axis.vertical,
                            shrinkWrap: true,
                            physics: NeverScrollableScrollPhysics(),
                            itemBuilder: (context, index) {
                              dynamic totalQte =
                                  widget.cart.cartItem[index].unitPrice;
                              return Container(
                                color: Colors.grey.shade200,
                                child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    ListTile(
                                      contentPadding:
                                          EdgeInsets.only(left: 5, right: 5),
                                      // leading: Container(
                                      //   width: 95,
                                      //   child: ClipRRect(
                                      //       //un peu comme un conteneur mais avec border radius
                                      //       borderRadius:
                                      //           BorderRadius.circular(9.0),
                                      //       child: Image.network(
                                      //         cart.cartItem[index]
                                      //             .productDetails,
                                      //         fit: BoxFit.cover,
                                      //       )),
                                      // ),
                                      title: Text(
                                        widget.cart.cartItem[index].productName
                                            .toString(),
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontWeight: FontWeight.bold),
                                      ),
                                      minVerticalPadding: 0,
                                      subtitle: Row(
                                        children: <Widget>[
                                          Text(
                                            widget.cart.cartItem[index].quantity
                                                .toString(),
                                            style: TextStyle(
                                                color: kPrimaryColor,
                                                fontSize: 20),
                                          ),
                                        ],
                                      ),
                                      trailing: Text(
                                        widget.cart.cartItem[index].subTotal
                                                .toString() +
                                            " XAF",
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            color: kPrimaryColor),
                                      ),
                                    ),
                                    Padding(
                                        padding: const EdgeInsets.only(top: 1)),
                                  ],
                                ),
                              );
                            }),
                      ),
                      ListTile(
                        contentPadding: EdgeInsets.only(left: 5, right: 5),
                        title: Text(
                          "Livraison",
                          style: TextStyle(
                              color: Colors.black, fontWeight: FontWeight.bold),
                        ),
                        minVerticalPadding: 0,
                        // subtitle: Row(
                        //   children: <Widget>[
                        //     Text(
                        //       cart.cartItem[index].quantity
                        //           .toString(),
                        //       style: TextStyle(
                        //           color: Colors.red,
                        //           fontSize: 20),
                        //     ),
                        //   ],
                        // ),
                        trailing: Text(
                          " ${widget.livraison} XAF",
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: kPrimaryColor),
                        ),
                      ),
                      Padding(padding: EdgeInsets.only(top: 20)),
                      Container(
                        decoration: BoxDecoration(
                            border: Border(
                          top: BorderSide(
                            color: Colors.black,
                          ),
                        )),
                        padding: const EdgeInsets.only(
                          top: 15,
                        ),
                        child: Row(
                          children: <Widget>[
                            Container(
                              padding:
                                  const EdgeInsets.only(left: kDefaultPadding),
                              width: (size.width / 2) - kDefaultPadding,
                              child: Text(
                                "TOTAL : ",
                                style: TextStyle(
                                    fontFamily: 'DayRoman',
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 20),
                              ),
                            ),
                            //Padding(padding: const EdgeInsets.only(left: kDefaultPadding)),
                            Container(
                                width: (size.width / 2) - kDefaultPadding,
                                padding: const EdgeInsets.only(
                                    right: kDefaultPadding),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  verticalDirection: VerticalDirection.down,
                                  children: [
                                    Text(
                                      "$montantTotalCommande" + " XAF",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          // fontFamily: 'DayRoman',
                                          color: kPrimaryColor),
                                    ),
                                  ],
                                )),
                          ],
                        ),
                      ),
                      Padding(padding: EdgeInsets.only(top: 10)),
                      Container(
                        decoration: BoxDecoration(
                          color: kPrimaryColor,
                          border: Border.all(
                            width: 3,
                            color: kPrimaryColor,
                          ),
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        child: Material(
                          color: kPrimaryColor,
                          borderRadius: BorderRadius.circular(10.0),
                          // button color
                          child: InkWell(
                            splashColor: Colors
                                .grey, // Couleur qui "envahit" le bouton lors du focus
                            child: SizedBox(
                              width: size.width - (kDefaultPadding * 2),
                              height: 40,
                              child: Center(
                                child: wi,
                              ),
                            ),
                            onTap: () async {
                              changeTextToLoader();
                              final date = DateTime.now();
                              var users = await CurrentUser().infoClient();
                              print(users.toString());

                              final invoice = Invoice(
                                  supplier: Supplier(
                                    name: users.name,
                                    address: users.email,
                                    mail: users.address,
                                  ),
                                  customer: Customer(
                                    name: 'Apple Inc.',
                                    address:
                                        'Apple Street, Cupertino, CA 95014',
                                  ),
                                  info: InvoiceInfo(
                                      date: date,
                                      modePayement: widget.payement,
                                      number: widget.numero,
                                      reference: widget.reference,
                                      position: widget._adresseClient,
                                      nomResto: widget.nomResto,
                                      livraison: widget.livraison),
                                  items: widget.cart.cartItem);

                              final pdfFile = await PdfInvoiceApi.generate(
                                  invoice, widget.cart);

                              PdfApi.openFile(pdfFile);
                              changeLoaderTextTo();
                            },
                          ),
                        ),
                      ),
                      Padding(padding: EdgeInsets.only(top: 35)),
                    ],
                  ),
                ),
              ],
            ),
          )
        ]),
      ),
    );
  }
}

class SingleProduitFactureFinal extends StatelessWidget {
  SingleProduitFactureFinal(
      {this.image, this.nom, this.qte, this.totalPrixQte});
  String nom, image;
  int qte, totalPrixQte;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.grey.shade200,
      padding: EdgeInsets.only(bottom: kDefaultPadding - 10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          ListTile(
            contentPadding: EdgeInsets.all(kDefaultPadding),
            leading: Container(
              width: 95,
              child: ClipRRect(
                //un peu comme un conteneur mais avec border radius
                borderRadius: BorderRadius.circular(9.0),
                child: Image.asset(
                  image,
                  fit: BoxFit.cover,
                ),
              ),
            ),
            title: Text(
              "$nom",
              style:
                  TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
            ),
            minVerticalPadding: 0,
            subtitle: Row(
              children: <Widget>[
                Padding(padding: const EdgeInsets.only(top: 25)),
                Text(
                  "$qte",
                  style: TextStyle(color: kPrimaryColor, fontSize: 18),
                ),
              ],
            ),
            trailing: Text(
              "$totalPrixQte XAF",
              style:
                  TextStyle(fontWeight: FontWeight.bold, color: kPrimaryColor),
            ),
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                  padding: EdgeInsets.only(
                left: kDefaultPadding,
              )),
            ],
          ),
        ],
      ),
    );
  }
}

class TitrePlusDetail extends StatelessWidget {
  String titre, detail;

  TitrePlusDetail({
    this.detail = '',
    this.titre,
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Text("$titre",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 15,
                    fontFamily: 'DayRoman',
                  )),
            ],
          ),
          Text("$detail",
              style: TextStyle(
                fontWeight: FontWeight.normal,
                fontSize: 13,
                fontFamily: 'DayRoman',
              )),
        ],
      ),
    );
  }
}
