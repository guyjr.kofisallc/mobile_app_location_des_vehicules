import 'package:flutter/material.dart';
import 'package:flutter_cart/flutter_cart.dart';
import 'package:rapidos_mobile/entities/restaurants.dart';
import 'body_you_cart.dart';

class YouCart extends StatelessWidget {
  YouCart(
    this.restaurant,
  );
  Restaurant restaurant;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // drawer: ClientDrawer(),
      appBar: buildAppBar(),
      body: BodyCart(restaurant),
      // bottomNavigationBar: Footer_all(),
    );
  }

  AppBar buildAppBar() {
    return AppBar(
      iconTheme: IconThemeData(color: Colors.black),
      centerTitle: true,
      title: Text(
        "Panier",
        style: TextStyle(
          color: Colors.black,
          fontWeight: FontWeight.bold,
        ),
      ),
      // leading: Icon(
      //   Icons.fastfood,
      //   size: 30,
      // ),
    );
  }
}
