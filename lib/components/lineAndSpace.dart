import 'package:flutter/material.dart';

Container line() {
  return Container(
    decoration: BoxDecoration(
        border: Border(
            bottom: BorderSide(width: 1.0, color: Colors.grey.shade400))),
  );
}

Padding smallSpace(double space) {
  return Padding(
      padding: EdgeInsets.only(
    top: space,
  ));
}
