import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_imagenetwork/flutter_imagenetwork.dart';
import 'package:rapidos_mobile/constants.dart';

class CarousselMeilleurOffre extends StatefulWidget {
  const CarousselMeilleurOffre({
    Key key,
    @required this.size,
  }) : super(key: key);

  final Size size;

  @override
  _CarousselMeilleurOffreState createState() => _CarousselMeilleurOffreState();
}

class _CarousselMeilleurOffreState extends State<CarousselMeilleurOffre> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    final List<String> imagesList = [
      'https://imagesrapidoseat.s3.amazonaws.com/publicite/pub2.jpg',
      'https://imagesrapidoseat.s3.amazonaws.com/publicite/pub3.jpg',
      'https://imagesrapidoseat.s3.amazonaws.com/publicite/pub4.jpg',
      // 'https://imagesrapidoseat.s3.amazonaws.com/publicite/pub6.jpg',
      // 'https://imagesrapidoseat.s3.amazonaws.com/publicite/pub7.jpg'
    ];
    int _currentIndex = 0;
    return Column(
      children: [
        CarouselSlider(
          options: CarouselOptions(
            autoPlay: true,
            enlargeCenterPage: false,
            disableCenter: false,
            height: size.height * 0.20,
            scrollDirection: Axis.horizontal,
            // onPageChanged: (index, reason) {
            //   setState(
            //     () {
            //       _currentIndex = index;
            //     },
            //   );
            // },
          ),
          items: imagesList
              .map(
                (item) => Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Card(
                    margin: EdgeInsets.only(
                      top: 10.0,
                      bottom: 10.0,
                    ),
                    elevation: 6.0,
                    shadowColor: Colors.grey,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    child: ClipRRect(
                      borderRadius: BorderRadius.all(
                        Radius.circular(3.0),
                      ),
                      child: Stack(
                        children: <Widget>[
                          AjanuwImage(
                            image: AjanuwNetworkImage(item),
                            fit: BoxFit.fill,
                            width: size.width,
                            height: size.height * 0.2,
                            loadingWidget: Center(
                              child: Image.asset(
                                "assets/images/restoAwait.png",
                                width: 50,
                                height: 50,
                                // color: Colors.red,
                              ),
                            ),
                            // loadingBuilder: AjanuwImage.defaultLoadingBuilder,
                            errorBuilder: immageErreurChargement,
                          ),
                          Container(
                            height: size.height * 0.20,
                            decoration: BoxDecoration(
                                //color: Colors.black.withOpacity(0.5),
                                gradient: LinearGradient(
                                    begin: Alignment.centerLeft,
                                    end: Alignment.centerRight,
                                    colors: [
                                  Colors.black.withOpacity(0.0),
                                  Colors.white.withOpacity(0.0)
                                ])),
                          ),
                          // Center(
                          //   child: Text(
                          //     '${titles[_currentIndex]}',
                          //     style: text1bb,
                          //   ),
                          // ),
                        ],
                      ),
                    ),
                  ),
                ),
              )
              .toList(),
        )
      ],
    );
  }
}

class Single_carrousel_offre extends StatelessWidget {
  Single_carrousel_offre({
    this.text,
    this.image,
    Key key,
  }) : super(key: key);

  String text;
  String image;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Stack(
      children: <Widget>[
        Container(
          height: size.height * 0.3,
          width: size.width,
          child: ClipRRect(
            child: Image.asset(
              image,
              fit: BoxFit.cover,
            ),
          ),
        ),
        Positioned(
          left: size.width / 3,
          top: size.width / 3,
          child: Text(
            text,
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 25,
                color: Colors.white,
                fontFamily: 'DayRoman'),
          ),
        )
      ],
    );
  }
}
