import 'package:flutter/material.dart';
import 'package:rapidos_mobile/components/FAQ_body.dart';

import '../constants.dart';

class FAQ extends StatelessWidget {
  const FAQ({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(context),
      body: FAQBody(),
      // bottomNavigationBar: Footer_all(),
    );
  }

  AppBar buildAppBar(BuildContext context) {
    return AppBar(
      centerTitle: true,
      iconTheme: themeIcon,
      elevation: 0.5,
      title: Text(
        "FAQs",
        style: text3nb,
      ),
    );
  }
}
