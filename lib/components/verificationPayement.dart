// import 'dart:async';

// import 'package:animated_text_kit/animated_text_kit.dart';
// import 'package:flutter/material.dart';
// import 'package:fluttericon/linearicons_free_icons.dart';
// import 'package:rapidos_mobile/components/facturePage.dart';

// import '../constants.dart';
// import '../services/payementservice.dart';

// class NewWidget extends StatefulWidget {
//   NewWidget(this.decisionPayement);
//   String decisionPayement;
//   @override
//   State<NewWidget> createState() => _NewWidgetState();
// }

// class _NewWidgetState extends State<NewWidget> {
//   Timer timer;
//   // @override
//   // void initState() {
//   //   super.initState();
//   //   timer = Timer.periodic(
//   //       Duration(seconds: 5),
//   //       (Timer t) => () {
//   //             addValue();
//   //             widget.createState();
//   //             print("actualisation");
//   //           });
//   // }
//   @override
//   void initState() {
//     super.initState();
//     // timer = Timer.periodic(Duration(minutes: 2), (Timer t) => setState(() {}));
//   }

//   void addValue() {
//     setState(() {
//       counter++;
//     });
//   }

//   int counter = 0;

//   // void addValue() {
//   //   print("actualisation");
//   //   setState(() {
//   //     counter = FutureBuilder(
//   //         future: PayementService().verifierPayement(),
//   //         builder: (context, snapshot) {
//   //           if (!snapshot.hasData) {
//   //             return Container(
//   //               height: 90,
//   //               child: Column(
//   //                 crossAxisAlignment: CrossAxisAlignment.center,
//   //                 mainAxisAlignment: MainAxisAlignment.center,
//   //                 children: [
//   //                   Center(
//   //                     child: AnimatedTextKit(
//   //                       animatedTexts: [
//   //                         TyperAnimatedText(
//   //                           "Veillez valider la transaction",
//   //                           textStyle: TextStyle(
//   //                             fontFamily: font,
//   //                             fontSize: 15,
//   //                             color: Colors.black,
//   //                             fontStyle: FontStyle.italic,
//   //                           ),
//   //                           textAlign: TextAlign.center,
//   //                         )
//   //                       ],
//   //                     ),
//   //                   ),
//   //                 ],
//   //               ),
//   //             );
//   //           } else if (snapshot.data == "400") {
//   //             return displayDialog(
//   //                 context, "Veillez renseigner un numero valide");
//   //           } else {
//   //             return Container(
//   //                 child: Center(
//   //               child: Text(
//   //                   "votre statut est :" + snapshot.data.status.toString()),
//   //             ));
//   //           }
//   //         });
//   //   });
//   // }

//   @override
//   void dispose() {
//     timer?.cancel();
//     super.dispose();
//   }

//   @override
//   Widget build(BuildContext context) {
//     print("acue");
//     return Container(
//         child: FutureBuilder(
//             future: PayementService().verifierPayement(),
//             builder: (context, snapshot) {
//               if (!snapshot.hasData) {
//                 return Container(
//                   height: 90,
//                   child: Column(
//                     crossAxisAlignment: CrossAxisAlignment.center,
//                     mainAxisAlignment: MainAxisAlignment.center,
//                     children: [
//                       Center(
//                         child: AnimatedTextKit(
//                           animatedTexts: [
//                             TyperAnimatedText(
//                               "Veillez valider la transaction",
//                               textStyle: TextStyle(
//                                 fontFamily: font,
//                                 fontSize: 15,
//                                 color: Colors.black,
//                                 fontStyle: FontStyle.italic,
//                               ),
//                               textAlign: TextAlign.center,
//                             )
//                           ],
//                         ),
//                       ),
//                     ],
//                   ),
//                 );
//               } else if (snapshot.data == "400") {
//                 return displayDialog(
//                     context, "OUPS", "Veillez renseigner un numero valide");
//               } else {
//                 if (snapshot.data.status.toString() == "SUCCESSFUL") {
//                   return Container(
//                     child: Column(
//                       children: <Widget>[
//                         Row(
//                           mainAxisAlignment: MainAxisAlignment.start,
//                           children: [
//                             Text(widget.decisionPayement,
//                                 style: TextStyle(
//                                   fontWeight: FontWeight.bold,
//                                   fontSize: 15,
//                                   fontFamily: 'DayRoman',
//                                 )),
//                           ],
//                         ),
//                         Row(
//                           mainAxisAlignment: MainAxisAlignment.spaceAround,
//                           children: [
//                             Text(widget.decisionPayement,
//                                 style: TextStyle(
//                                   fontWeight: FontWeight.normal,
//                                   fontSize: 13,
//                                   fontFamily: 'DayRoman',
//                                 )),
//                             Spacer(),
//                             Container(
//                                 child: Row(
//                               children: [
//                                 Text(snapshot.data.status.toString()),
//                                 Padding(padding: EdgeInsets.only(left: 5)),
//                                 Icon(
//                                   Icons.check_circle_outline,
//                                   color: Colors.green,
//                                   size: 15,
//                                 )
//                               ],
//                             )),
//                           ],
//                         ),
//                       ],
//                     ),
//                   );
//                 } else if (snapshot.data.status.toString() == "FAILED") {
//                   return Container(
//                     child: Column(
//                       children: <Widget>[
//                         Row(
//                           mainAxisAlignment: MainAxisAlignment.start,
//                           children: [
//                             Text("Payement",
//                                 style: TextStyle(
//                                   fontWeight: FontWeight.bold,
//                                   fontSize: 15,
//                                   fontFamily: 'DayRoman',
//                                 )),
//                           ],
//                         ),
//                         Row(
//                           mainAxisAlignment: MainAxisAlignment.spaceAround,
//                           children: [
//                             Text(widget.decisionPayement,
//                                 style: TextStyle(
//                                   fontWeight: FontWeight.normal,
//                                   fontSize: 13,
//                                   fontFamily: 'DayRoman',
//                                 )),
//                             Spacer(),
//                             Container(
//                                 child: Row(
//                               children: [
//                                 Text(snapshot.data.status.toString()),
//                                 Padding(padding: EdgeInsets.only(left: 5)),
//                                 Icon(
//                                   LineariconsFree.cross_circle,
//                                   color: Colors.red,
//                                   size: 15,
//                                 )
//                               ],
//                             )),
//                           ],
//                         ),
//                       ],
//                     ),
//                   );
//                 } else {
//                   return Container(
//                     child: Column(
//                       children: <Widget>[
//                         Row(
//                           mainAxisAlignment: MainAxisAlignment.start,
//                           children: [
//                             Text("Payement",
//                                 style: TextStyle(
//                                   fontWeight: FontWeight.bold,
//                                   fontSize: 15,
//                                   fontFamily: 'DayRoman',
//                                 )),
//                           ],
//                         ),
//                         Row(
//                           mainAxisAlignment: MainAxisAlignment.spaceAround,
//                           children: [
//                             Text("Orange Money",
//                                 style: TextStyle(
//                                   fontWeight: FontWeight.normal,
//                                   fontSize: 13,
//                                   fontFamily: 'DayRoman',
//                                 )),
//                             Spacer(),
//                             Center(
//                               child: Transform.scale(
//                                 scale: 0.5,
//                                 child: CircularProgressIndicator(
//                                   strokeWidth: 2,
//                                 ),
//                               ),
//                             ),
//                           ],
//                         ),
//                       ],
//                     ),
//                   );
//                 }
//               }
//             }));
//   }
// }
