class Categorie {
  Categorie({
    this.createdAt,
    this.updateAt,
    this.createdBy,
    this.updatedBy,
    this.categorieId,
    this.libelle,
    this.image,
    this.description,
    this.produits,
  });

  double createdAt;
  double updateAt;
  int createdBy;
  int updatedBy;
  int categorieId;
  String libelle;
  String image;
  String description;
  List<dynamic> produits;

  factory Categorie.fromJson(Map<String, dynamic> json) => Categorie(
        createdAt: json["createdAt"].toDouble(),
        updateAt: json["updateAt"].toDouble(),
        createdBy: json["createdBy"],
        updatedBy: json["updatedBy"],
        categorieId: json["categorie_id"],
        libelle: json["libelle"],
        image: json["image"],
        description: json["description"],
      );

  // Map<String, dynamic> toJson() => {
  //     "createdAt": createdAt,
  //     "updateAt": updateAt,
  //     "createdBy": createdBy,
  //     "updatedBy": updatedBy,
  //     "categorie_id": categorieId,
  //     "libelle": libelle,
  //     "image": image,
  //     "description": description,
  //     "produits": List<dynamic>.from(produits.map((x) => x)),
  // };
}
