class CartItemCommand {
  CartItemCommand({
    this.createdAt,
    this.updateAt,
    this.createdBy,
    this.updatedBy,
    this.id,
    this.produit,
    this.quantite,
    this.price,
    this.cartId,
  });

  double createdAt;
  double updateAt;
  int createdBy;
  int updatedBy;
  int id;
  Produit produit;
  int quantite;
  int price;
  dynamic cartId;

  factory CartItemCommand.fromJson(Map<String, dynamic> json) =>
      CartItemCommand(
        createdAt: json["createdAt"].toDouble(),
        updateAt: json["updateAt"].toDouble(),
        createdBy: json["createdBy"],
        updatedBy: json["updatedBy"],
        id: json["id"],
        produit: Produit.fromJson(json["produit"]),
        quantite: json["quantite"],
        price: json["price"],
        cartId: json["cart_id"],
      );

  Map<String, dynamic> toJson() => {
        "createdAt": createdAt,
        "updateAt": updateAt,
        "createdBy": createdBy,
        "updatedBy": updatedBy,
        "id": id,
        "produit": produit.toJson(),
        "quantite": quantite,
        "price": price,
        "cart_id": cartId,
      };
}

class Produit {
  Produit({
    this.produitId,
    this.designation,
  });

  int produitId;
  String designation;

  factory Produit.fromJson(Map<String, dynamic> json) => Produit(
        produitId: json["produit_id"],
        designation: json["designation"],
      );

  Map<String, dynamic> toJson() => {
        "produit_id": produitId,
        "designation": designation,
      };
}
