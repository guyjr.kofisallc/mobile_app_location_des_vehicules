class PubProduitData {
  double createdAt;
  double updateAt;
  int createdBy;
  int updatedBy;
  int idPub;
  int pub;

  PubProduitData(
      {this.createdAt,
      this.updateAt,
      this.createdBy,
      this.updatedBy,
      this.idPub,
      this.pub});

  PubProduitData.fromJson(Map<String, dynamic> json) {
    createdAt = json['createdAt'];
    updateAt = json['updateAt'];
    createdBy = json['createdBy'];
    updatedBy = json['updatedBy'];
    idPub = json['id_pub'];
    pub = json['pub'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['createdAt'] = this.createdAt;
    data['updateAt'] = this.updateAt;
    data['createdBy'] = this.createdBy;
    data['updatedBy'] = this.updatedBy;
    data['id_pub'] = this.idPub;
    data['pub'] = this.pub;
    return data;
  }
}
