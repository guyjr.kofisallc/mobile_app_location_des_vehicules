class Adresse {
  Adresse({
    this.ville,
    this.quartier,
    this.emplacement,
    this.numero,
    this.rue,
    this.appartement,
  });

  String ville;
  String quartier;
  String emplacement;
  String numero;
  String rue;
  String appartement;
}
