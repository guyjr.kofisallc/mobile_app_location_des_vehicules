// // Copyright 2019 The Flutter team. All rights reserved.
// // Use of this source code is governed by a BSD-style license that can be
// // found in the LICENSE file.

// import 'package:flutter/foundation.dart';

// import 'allProduit.dart';

// class CartModel extends ChangeNotifier {
//   /// The private field backing [catalog].
//   Produit _catalog;

//   /// Internal, private state of the cart. Stores the ids of each item.
//   final List<int> _itemIds = [];

//   /// The current catalog. Used to construct items from numeric ids.
//   Produit get catalog => _catalog;

//   set catalog(Produit newCatalog) {
//     _catalog = newCatalog;
//     // Notify listeners, in case the new catalog provides information
//     // different from the previous one. For example, availability of an item
//     // might have changed.
//     notifyListeners();
//   }

//   /// List of items in the cart.
//   List<Produit> get items =>
//       _itemIds.map((id) => _catalog.getById(id)).toList();

//   /// The current total price of all items.
//   int get totalPrice =>
//       items.fold(0, (total, current) => total + current.prixUnitaire.toInt());

//   /// Adds [item] to cart. This is the only way to modify the cart from outside.
//   void add(Produit item) {
//     _itemIds.add(item.produitId);
//     // This line tells [Model] that it should rebuild the widgets that
//     // depend on it.
//     notifyListeners();
//   }

//   void remove(Produit item) {
//     _itemIds.remove(item.produitId);
//     // Don't forget to tell dependent widgets to rebuild _every time_
//     // you change the model.
//     notifyListeners();
//   }
// }
