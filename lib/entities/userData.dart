class UserData {
  double createdAt;
  double updateAt;
  int userId;
  String name;
  String username;
  String email;
  String phone;
  String address;
  String password;
  String photo;
  String heureOuverture;
  String heureFermerture;
  String referencement;
  Position position;
  String description;
  List<Roles> roles;

  UserData(
      {this.createdAt,
      this.updateAt,
      this.userId,
      this.name,
      this.username,
      this.email,
      this.phone,
      this.address,
      this.password,
      this.photo,
      this.heureOuverture,
      this.heureFermerture,
      this.referencement,
      this.position,
      this.description,
      this.roles});

  UserData.fromJson(Map<String, dynamic> json) {
    createdAt = json['createdAt'];
    updateAt = json['updateAt'];
    userId = json['user_id'];
    name = json['name'];
    username = json['username'];
    email = json['email'];
    phone = json['phone'];
    address = json['address'];
    password = json['password'];
    photo = json['photo'];
    heureOuverture = json['heureOuverture'];
    heureFermerture = json['heureFermerture'];
    referencement = json['referencement'];
    position = json['position'] != null
        ? new Position.fromJson(json['position'])
        : null;
    description = json['description'];
    if (json['roles'] != null) {
      roles = new List<Roles>();
      json['roles'].forEach((v) {
        roles.add(new Roles.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['createdAt'] = this.createdAt;
    data['updateAt'] = this.updateAt;
    data['user_id'] = this.userId;
    data['name'] = this.name;
    data['username'] = this.username;
    data['email'] = this.email;
    data['phone'] = this.phone;
    data['address'] = this.address;
    data['password'] = this.password;
    data['photo'] = this.photo;
    data['heureOuverture'] = this.heureOuverture;
    data['heureFermerture'] = this.heureFermerture;
    data['referencement'] = this.referencement;
    if (this.position != null) {
      data['position'] = this.position.toJson();
    }
    data['description'] = this.description;
    if (this.roles != null) {
      data['roles'] = this.roles.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Position {
  int positionId;
  double latitude;
  double longitude;

  Position({this.positionId, this.latitude, this.longitude});

  Position.fromJson(Map<String, dynamic> json) {
    positionId = json['position_id'];
    latitude = json['latitude'];
    longitude = json['longitude'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['position_id'] = this.positionId;
    data['latitude'] = this.latitude;
    data['longitude'] = this.longitude;
    return data;
  }
}

class Roles {
  int roleId;
  String name;

  Roles({this.roleId, this.name});

  Roles.fromJson(Map<String, dynamic> json) {
    roleId = json['role_id'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['role_id'] = this.roleId;
    data['name'] = this.name;
    return data;
  }
}
