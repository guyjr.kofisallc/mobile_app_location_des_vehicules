class MessageData {
  double createdAt;
  double updateAt;
  int createdBy;
  int updatedBy;
  int messageId;
  String message;
  bool visible;
  List<Reponses> reponses;

  MessageData(
      {this.createdAt,
      this.updateAt,
      this.createdBy,
      this.updatedBy,
      this.messageId,
      this.message,
      this.visible,
      this.reponses});

  MessageData.fromJson(Map<String, dynamic> json) {
    createdAt = json['createdAt'];
    updateAt = json['updateAt'];
    createdBy = json['createdBy'];
    updatedBy = json['updatedBy'];
    messageId = json['message_id'];
    message = json['message'];
    visible = json['visible'];
    if (json['reponses'] != null) {
      reponses = <Reponses>[];
      json['reponses'].forEach((v) {
        reponses.add(new Reponses.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['createdAt'] = this.createdAt;
    data['updateAt'] = this.updateAt;
    data['createdBy'] = this.createdBy;
    data['updatedBy'] = this.updatedBy;
    data['message_id'] = this.messageId;
    data['message'] = this.message;
    data['visible'] = this.visible;
    if (this.reponses != null) {
      data['reponses'] = this.reponses.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Reponses {
  double createdAt;
  double updateAt;
  int createdBy;
  int updatedBy;
  int reponseId;
  String reponse;
  bool visible;

  Reponses(
      {this.createdAt,
      this.updateAt,
      this.createdBy,
      this.updatedBy,
      this.reponseId,
      this.reponse,
      this.visible});

  Reponses.fromJson(Map<String, dynamic> json) {
    createdAt = json['createdAt'];
    updateAt = json['updateAt'];
    createdBy = json['createdBy'];
    updatedBy = json['updatedBy'];
    reponseId = json['reponse_id'];
    reponse = json['reponse'];
    visible = json['visible'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['createdAt'] = this.createdAt;
    data['updateAt'] = this.updateAt;
    data['createdBy'] = this.createdBy;
    data['updatedBy'] = this.updatedBy;
    data['reponse_id'] = this.reponseId;
    data['reponse'] = this.reponse;
    data['visible'] = this.visible;
    return data;
  }
}
