class NotificationData {
  double createdAt;
  double updateAt;
  int createdBy;
  int updatedBy;
  int notificationId;
  String notification;
  String image;

  NotificationData(
      {this.createdAt,
      this.updateAt,
      this.createdBy,
      this.updatedBy,
      this.notificationId,
      this.notification,
      this.image});

  NotificationData.fromJson(Map<String, dynamic> json) {
    createdAt = json['createdAt'];
    updateAt = json['updateAt'];
    createdBy = json['createdBy'];
    updatedBy = json['updatedBy'];
    notificationId = json['notification_id'];
    notification = json['notification'];
    image = json['image'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['createdAt'] = this.createdAt;
    data['updateAt'] = this.updateAt;
    data['createdBy'] = this.createdBy;
    data['updatedBy'] = this.updatedBy;
    data['notification_id'] = this.notificationId;
    data['notification'] = this.notification;
    data['image'] = this.image;
    return data;
  }
}
