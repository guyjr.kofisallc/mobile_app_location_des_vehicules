class TokenPayement {
  String token;
  int expiresIn;

  TokenPayement({this.token, this.expiresIn});

  TokenPayement.fromJson(Map<String, dynamic> json) {
    token = json['token'];
    expiresIn = json['expires_in'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['token'] = this.token;
    data['expires_in'] = this.expiresIn;
    return data;
  }
}
