class CommandeSend {
  double createdAt;
  double updateAt;
  int createdBy;
  int updatedBy;
  int cartId;
  String referencePayement;
  String description;
  String address;
  int total;
  int idResto;
  String numero;
  String etatLivraison;
  String etatCommande;
  PositionCart positionCart;

  CommandeSend(
      {this.createdAt,
      this.updateAt,
      this.createdBy,
      this.updatedBy,
      this.cartId,
      this.referencePayement,
      this.description,
      this.address,
      this.total,
      this.idResto,
      this.numero,
      this.etatLivraison,
      this.etatCommande,
      this.positionCart});

  CommandeSend.fromJson(Map<String, dynamic> json) {
    createdAt = json['createdAt'];
    updateAt = json['updateAt'];
    createdBy = json['createdBy'];
    updatedBy = json['updatedBy'];
    cartId = json['cartId'];
    referencePayement = json['referencePayement'];
    description = json['description'];
    address = json['address'];
    total = json['total'];
    idResto = json['idResto'];
    numero = json['numero'];
    etatLivraison = json['etatLivraison'];
    etatCommande = json['etatCommande'];
    positionCart = json['position_cart'] != null
        ? new PositionCart.fromJson(json['position_cart'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['createdAt'] = this.createdAt;
    data['updateAt'] = this.updateAt;
    data['createdBy'] = this.createdBy;
    data['updatedBy'] = this.updatedBy;
    data['cartId'] = this.cartId;
    data['referencePayement'] = this.referencePayement;
    data['description'] = this.description;
    data['address'] = this.address;
    data['total'] = this.total;
    data['idResto'] = this.idResto;
    data['numero'] = this.numero;
    data['etatLivraison'] = this.etatLivraison;
    data['etatCommande'] = this.etatCommande;
    if (this.positionCart != null) {
      data['position_cart'] = this.positionCart.toJson();
    }
    return data;
  }
}

class PositionCart {
  double latitude;
  double longitude;

  PositionCart({this.latitude, this.longitude});

  PositionCart.fromJson(Map<String, dynamic> json) {
    latitude = json['latitude'];
    longitude = json['longitude'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['latitude'] = this.latitude;
    data['longitude'] = this.longitude;
    return data;
  }
}
