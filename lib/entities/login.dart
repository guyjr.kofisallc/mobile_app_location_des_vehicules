class Login {
  String accessToken;
  String tokenType;
  String userRole;

  Login({this.accessToken, this.tokenType, this.userRole});

  Login.fromJson(Map<String, dynamic> json) {
    accessToken = json['accessToken'];
    tokenType = json['tokenType'];
    userRole = json['userRole'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['accessToken'] = accessToken;
    data['tokenType'] = tokenType;
    data['userRole'] = userRole;
    return data;
  }
}
