// To parse this JSON data, do
//
//     final CurentUserData = CurentUserDataFromJson(jsonString);
class CurentUserData {
  CurentUserData({
    this.id,
    this.username,
    this.name,
    this.phone,
    this.address,
    this.email,
  });

  int id;
  String username;
  String name;
  String phone;
  String address;
  String email;

  factory CurentUserData.fromJson(Map<String, dynamic> json) => CurentUserData(
        id: json["id"],
        username: json["username"],
        name: json["name"],
        phone: json["phone"],
        address: json["address"],
        email: json["email"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "username": username,
        "name": name,
        "phone": phone,
        "address": address,
        "email": email,
      };
}
