class LigneCommande {
  Cart cart = new Cart();
  int price;
  int quantite;
  ProduitCommande produit;

  LigneCommande({this.cart, this.price, this.quantite, this.produit});

  LigneCommande.fromJson(Map<String, dynamic> json) {
    cart = json['cart'] != null ? new Cart.fromJson(json['cart']) : null;
    price = json['price'];
    quantite = json['quantite'];
    produit = json['produit'] != null
        ? new ProduitCommande.fromJson(json['produit'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.cart != null) {
      data['cart'] = this.cart.toJson();
    }
    data['price'] = this.price;
    data['quantite'] = this.quantite;
    if (this.produit != null) {
      data['produit'] = this.produit.toJson();
    }
    return data;
  }
}

class Cart {
  int cartId = 1;

  Cart({this.cartId});

  Cart.fromJson(Map<String, dynamic> json) {
    cartId = json['cartId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['cartId'] = this.cartId;
    return data;
  }
}

class ProduitCommande {
  int produitId;

  ProduitCommande({this.produitId});

  ProduitCommande.fromJson(Map<String, dynamic> json) {
    produitId = json['produit_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['produit_id'] = this.produitId;
    return data;
  }
}
