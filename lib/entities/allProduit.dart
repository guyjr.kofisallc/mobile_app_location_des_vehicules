import 'dart:convert';

List<Produit> ProduitFromJson(String str) =>
    List<Produit>.from(json.decode(str).map((x) => Produit.fromJson(x)));

String ProduitToJson(List<Produit> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Produit {
  Produit({
    this.createdAt,
    this.updateAt,
    this.createdBy,
    this.updatedBy,
    this.produitId,
    this.designation,
    this.refProduit,
    this.prixUnitaire,
    this.details,
    this.total,
    this.seuil,
    this.prixAchatMoyen,
    this.restant,
    this.pourcentageVente,
    this.prix,
    this.photo,
    this.image,
    this.categorie,
    this.statut,
  });

  double createdAt;
  double updateAt;
  int createdBy;
  dynamic updatedBy;
  int produitId;
  String designation;
  String refProduit;
  double prixUnitaire;
  String details;
  dynamic total;
  dynamic seuil;
  double prixAchatMoyen;
  dynamic restant;
  dynamic pourcentageVente;
  dynamic prix;
  dynamic photo;
  String image;
  dynamic categorie;
  dynamic statut;

  factory Produit.fromJson(Map<String, dynamic> json) => Produit(
        createdAt: json["createdAt"].toDouble(),
        updateAt: json["updateAt"].toDouble(),
        createdBy: json["createdBy"],
        updatedBy: json["updatedBy"],
        produitId: json["produit_id"],
        designation: json["designation"],
        refProduit: json["refProduit"],
        prixUnitaire: json["prixUnitaire"],
        details: json["details"],
        total: json["total"],
        seuil: json["seuil"],
        prixAchatMoyen: json["prixAchatMoyen"],
        restant: json["restant"],
        pourcentageVente: json["pourcentageVente"],
        prix: json["prix"],
        photo: json["photo"],
        image: json["image"],
        categorie: json["categorie"],
        statut: json["statut"],
      );

  Map<String, dynamic> toJson() => {
        "createdAt": createdAt,
        "updateAt": updateAt,
        "createdBy": createdBy,
        "updatedBy": updatedBy,
        "produit_id": produitId,
        "designation": designation,
        "refProduit": refProduit,
        "prixUnitaire": prixUnitaire,
        "details": details,
        "total": total,
        "seuil": seuil,
        "prixAchatMoyen": prixAchatMoyen,
        "restant": restant,
        "pourcentageVente": pourcentageVente,
        "prix": prix,
        "photo": photo,
        "image": image,
        "categorie": categorie,
        "statut": statut,
      };
}
