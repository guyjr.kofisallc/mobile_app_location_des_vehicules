class PayementEntities {
  String reference;
  String ussdCode;
  String operator;

  PayementEntities({this.reference, this.ussdCode, this.operator});

  PayementEntities.fromJson(Map<String, dynamic> json) {
    reference = json['reference'];
    ussdCode = json['ussd_code'];
    operator = json['operator'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['reference'] = this.reference;
    data['ussd_code'] = this.ussdCode;
    data['operator'] = this.operator;
    return data;
  }
}
