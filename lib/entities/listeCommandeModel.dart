class ListCommandeModel {
  double createdAt;
  double updateAt;
  int createdBy;
  int updatedBy;
  int cartId;
  String referencePayement;
  String description;
  String address;
  double total;
  double livraison;
  int idResto;
  int idLivreur;
  String numero;
  String etatLivraison;
  String etatCommande;
  List<CartItems> cartItems;
  PositionCart positionCart;
  Map<String, dynamic> resto;
  Map<String, dynamic> livreur;

  ListCommandeModel(
      {this.createdAt,
      this.updateAt,
      this.createdBy,
      this.updatedBy,
      this.cartId,
      this.referencePayement,
      this.description,
      this.address,
      this.total,
      this.livraison,
      this.idResto,
      this.idLivreur,
      this.numero,
      this.etatLivraison,
      this.etatCommande,
      this.cartItems,
      this.positionCart,
      this.resto,
      this.livreur});

  ListCommandeModel.fromJson(Map<String, dynamic> json) {
    createdAt = json['createdAt'];
    updateAt = json['updateAt'];
    createdBy = json['createdBy'];
    updatedBy = json['updatedBy'];
    cartId = json['cartId'];
    referencePayement = json['referencePayement'];
    description = json['description'];
    address = json['address'];
    total = json['total'];
    livraison = json['livraison'];
    idResto = json['idResto'];
    idLivreur = json['idLivreur'];
    numero = json['numero'];
    etatLivraison = json['etatLivraison'];
    etatCommande = json['etatCommande'];
    cartItems = <CartItems>[];
    json['cartItems'].forEach((v) {
      cartItems.add(new CartItems.fromJson(v));
    });
    // positionCart = json['position_cart'];
    resto = json['resto'];
    // livreur = json['livreur'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['createdAt'] = this.createdAt;
    data['updateAt'] = this.updateAt;
    data['createdBy'] = this.createdBy;
    data['updatedBy'] = this.updatedBy;
    data['cartId'] = this.cartId;
    data['referencePayement'] = this.referencePayement;
    data['description'] = this.description;
    data['address'] = this.address;
    data['total'] = this.total;
    data['livraison'] = this.livraison;
    data['idResto'] = this.idResto;
    data['idLivreur'] = this.idLivreur;
    data['numero'] = this.numero;
    data['etatLivraison'] = this.etatLivraison;
    data['etatCommande'] = this.etatCommande;
    if (this.cartItems = null) {
      // data['cartItems'] = this.cartItems.map((v) => v.toJson()).toList();
    }
    if (this.positionCart = null) {
      data['position_cart'] = this.positionCart.toJson();
    }
    if (this.resto = null) {
      // data['resto'] = this.resto.toJson();
    }
    data['livreur'] = this.livreur;
    return data;
  }
}

class CartItems {
  double createdAt;
  double updateAt;
  int createdBy;
  int updatedBy;
  int cartItemId;
  double price;
  int quantite;
  Map<String, dynamic> produit;

  CartItems(
      {this.createdAt,
      this.updateAt,
      this.createdBy,
      this.updatedBy,
      this.cartItemId,
      this.price,
      this.quantite,
      this.produit});

  CartItems.fromJson(Map<String, dynamic> json) {
    createdAt = json['createdAt'];
    updateAt = json['updateAt'];
    createdBy = json['createdBy'];
    updatedBy = json['updatedBy'];
    cartItemId = json['cartItemId'];
    price = json['price'];
    quantite = json['quantite'];
    produit = json['produit'];
  }

  // Map<String, dynamic> toJson() {
  //   final Map<String, dynamic> data = new Map<String, dynamic>();
  //   data['createdAt'] = this.createdAt;
  //   data['updateAt'] = this.updateAt;
  //   data['createdBy'] = this.createdBy;
  //   data['updatedBy'] = this.updatedBy;
  //   data['cartItemId'] = this.cartItemId;
  //   data['price'] = this.price;
  //   data['quantite'] = this.quantite;
  //   if (this.produit = null) {
  //     data['produit'] = this.produit.toJson();
  //   }
  //   return data;
  // }
}

class Produit {
  double createdAt;
  double updateAt;
  int createdBy;
  int updatedBy;
  int produitId;
  String designation;
  String refProduit;
  int prixUnitaire;
  String details;
  Null total;
  Null seuil;
  int prixAchatMoyen;
  Null restant;
  Null pourcentageVente;
  Null prix;
  Null photo;
  String image;
  Categorie categorie;
  Null statut;

  Produit(
      {this.createdAt,
      this.updateAt,
      this.createdBy,
      this.updatedBy,
      this.produitId,
      this.designation,
      this.refProduit,
      this.prixUnitaire,
      this.details,
      this.total,
      this.seuil,
      this.prixAchatMoyen,
      this.restant,
      this.pourcentageVente,
      this.prix,
      this.photo,
      this.image,
      this.categorie,
      this.statut});

  Produit.fromJson(Map<String, dynamic> json) {
    createdAt = json['createdAt'];
    updateAt = json['updateAt'];
    createdBy = json['createdBy'];
    updatedBy = json['updatedBy'];
    produitId = json['produit_id'];
    designation = json['designation'];
    refProduit = json['refProduit'];
    prixUnitaire = json['prixUnitaire'];
    details = json['details'];
    total = json['total'];
    seuil = json['seuil'];
    prixAchatMoyen = json['prixAchatMoyen'];
    restant = json['restant'];
    pourcentageVente = json['pourcentageVente'];
    prix = json['prix'];
    photo = json['photo'];
    image = json['image'];
    categorie = json['categorie'];
    statut = json['statut'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['createdAt'] = this.createdAt;
    data['updateAt'] = this.updateAt;
    data['createdBy'] = this.createdBy;
    data['updatedBy'] = this.updatedBy;
    data['produit_id'] = this.produitId;
    data['designation'] = this.designation;
    data['refProduit'] = this.refProduit;
    data['prixUnitaire'] = this.prixUnitaire;
    data['details'] = this.details;
    data['total'] = this.total;
    data['seuil'] = this.seuil;
    data['prixAchatMoyen'] = this.prixAchatMoyen;
    data['restant'] = this.restant;
    data['pourcentageVente'] = this.pourcentageVente;
    data['prix'] = this.prix;
    data['photo'] = this.photo;
    data['image'] = this.image;
    if (this.categorie = null) {
      data['categorie'] = this.categorie.toJson();
    }
    data['statut'] = this.statut;
    return data;
  }
}

class Categorie {
  double createdAt;
  double updateAt;
  int createdBy;
  int updatedBy;
  int categorieId;
  String libelle;
  String image;
  String description;

  Categorie(
      {this.createdAt,
      this.updateAt,
      this.createdBy,
      this.updatedBy,
      this.categorieId,
      this.libelle,
      this.image,
      this.description});

  Categorie.fromJson(Map<String, dynamic> json) {
    createdAt = json['createdAt'];
    updateAt = json['updateAt'];
    createdBy = json['createdBy'];
    updatedBy = json['updatedBy'];
    categorieId = json['categorie_id'];
    libelle = json['libelle'];
    image = json['image'];
    description = json['description'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['createdAt'] = this.createdAt;
    data['updateAt'] = this.updateAt;
    data['createdBy'] = this.createdBy;
    data['updatedBy'] = this.updatedBy;
    data['categorie_id'] = this.categorieId;
    data['libelle'] = this.libelle;
    data['image'] = this.image;
    data['description'] = this.description;
    return data;
  }
}

class PositionCart {
  int positionId;
  double latitude;
  double longitude;

  PositionCart({this.positionId, this.latitude, this.longitude});

  PositionCart.fromJson(Map<String, dynamic> json) {
    positionId = json['position_id'];
    latitude = json['latitude'];
    longitude = json['longitude'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['position_id'] = this.positionId;
    data['latitude'] = this.latitude;
    data['longitude'] = this.longitude;
    return data;
  }
}

class Resto {
  double createdAt;
  double updateAt;
  int userId;
  String name;
  String username;
  String email;
  String phone;
  String address;
  String password;
  String photo;
  // PositionCart position;
  // Null vehicule;
  // Null active;
  // Null tempes;
  String description;
  List<Roles> roles;

  Resto(
      {this.createdAt,
      this.updateAt,
      this.userId,
      this.name,
      this.username,
      this.email,
      this.phone,
      this.address,
      this.password,
      this.photo,
      // this.position,
      // this.vehicule,
      // this.active,
      // this.tempes,
      this.description,
      this.roles});

  Resto.fromJson(Map<String, dynamic> json) {
    createdAt = json['createdAt'];
    updateAt = json['updateAt'];
    userId = json['user_id'];
    name = json['name'];
    username = json['username'];
    email = json['email'];
    phone = json['phone'];
    address = json['address'];
    password = json['password'];
    photo = json['photo'];
    // position = json['position'];
    // vehicule = json['vehicule'];
    // active = json['active'];
    // tempes = json['tempes'];
    description = json['description'];
    if (json['roles'] = null) {
      roles = <Roles>[];
      json['roles'].forEach((v) {
        roles.add(new Roles.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['createdAt'] = this.createdAt;
    data['updateAt'] = this.updateAt;
    data['user_id'] = this.userId;
    data['name'] = this.name;
    data['username'] = this.username;
    data['email'] = this.email;
    data['phone'] = this.phone;
    data['address'] = this.address;
    data['password'] = this.password;
    data['photo'] = this.photo;
    // if (this.position = null) {
    //   data['position'] = this.position.toJson();
    // }
    // data['vehicule'] = this.vehicule;
    // data['active'] = this.active;
    // data['tempes'] = this.tempes;
    data['description'] = this.description;
    if (this.roles = null) {
      data['roles'] = this.roles.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Roles {
  int roleId;
  String name;

  Roles({this.roleId, this.name});

  Roles.fromJson(Map<String, dynamic> json) {
    roleId = json['role_id'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['role_id'] = this.roleId;
    data['name'] = this.name;
    return data;
  }
}
