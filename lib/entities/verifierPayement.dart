class VerifierPayement {
  String reference;
  String externalReference;
  String status;
  double amount;
  String currency;
  String operator;
  String code;
  String operatorReference;
  String description;
  String externalUser;
  Null reason;

  VerifierPayement(
      {this.reference,
      this.externalReference,
      this.status,
      this.amount,
      this.currency,
      this.operator,
      this.code,
      this.operatorReference,
      this.description,
      this.externalUser,
      this.reason});

  VerifierPayement.fromJson(Map<String, dynamic> json) {
    reference = json['reference'];
    externalReference = json['external_reference'];
    status = json['status'];
    amount = json['amount'];
    currency = json['currency'];
    operator = json['operator'];
    code = json['code'];
    operatorReference = json['operator_reference'];
    description = json['description'];
    externalUser = json['external_user'];
    // if (json['external_user'] == null) {
    //   reason = json['description'];
    // } else {
    //   reason = json['reason'];
    // }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['reference'] = this.reference;
    data['external_reference'] = this.externalReference;
    data['status'] = this.status;
    data['amount'] = this.amount;
    data['currency'] = this.currency;
    data['operator'] = this.operator;
    data['code'] = this.code;
    data['operator_reference'] = this.operatorReference;
    data['description'] = this.description;
    data['external_user'] = this.externalUser;
    data['reason'] = this.reason;
    return data;
  }
}
