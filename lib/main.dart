import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:loading_overlay_pro/animations/bouncing_line.dart';
import 'package:rapidos_mobile/components/acceuil.dart';
import 'package:rapidos_mobile/components/connexion.dart';
import 'package:rapidos_mobile/components/home_page.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:splashscreen/splashscreen.dart';
import 'dart:convert' show json, base64, ascii;

import 'constants.dart';

final storage = FlutterSecureStorage();
Future<String> get jwtOrEmpty async {
  var jwt = await storage.read(key: "jwt");
  if (jwt == null) return "";
  return jwt;
}

// ******Tagakou********//
//*******hammel brayan*********//
//*******+237 680401646*********//
//*******btagakou@gmail.com********//
void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    // Size size = MediaQuery.of(context).size;
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Rapidos eat',
      theme: ThemeData(
        fontFamily: 'DayRoman',
        scaffoldBackgroundColor: kBackgroundColor,
        iconTheme: IconThemeData(
          color: Colors.black,
        ),
        appBarTheme: AppBarTheme(
            elevation: 0,
            backgroundColor: kBackgroundColor,
            actionsIconTheme: IconThemeData(
              color: Colors.black,
            )),
        primaryColor: kHeaderColor,
        textTheme: Theme.of(context)
            .textTheme
            .apply(bodyColor: kTeXTColor, fontFamily: 'DayRoman'),
        primarySwatch: Colors.red,
      ),
      home: SplashScreenPage(),
    );
  }
}

class SplashScreenPage extends StatefulWidget {
  @override
  State<SplashScreenPage> createState() => _SplashScreenPageState();
}

class _SplashScreenPageState extends State<SplashScreenPage> {
  LatLng actue = new LatLng(4.0580037, 9.7260679);
  Future<LocationData> getCurentLocation() {
    Location location = Location();

    return location.getLocation().then((location) async {
      actue = LatLng(location.latitude, location.longitude);
      latActuel = location.latitude;
      lonActuel = location.longitude;
      setState(() {
        actue = LatLng(location.latitude, location.longitude);
        latActuel = location.latitude;
        lonActuel = location.longitude;
      });

      return location;
    });
  }

  @override
  void initState() {
    super.initState();
    getCurentLocation();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
  }

  @override
  Widget build(BuildContext context) {
    return SplashScreen(
      seconds: 1,
      navigateAfterSeconds: new IndiqueAcceuil(),
      backgroundColor: kPrimaryColor,
      title: new Text(
        'Bienvenue',
        style: TextStyle(
            fontSize: 10,
            color: Colors.white,
            fontFamily: 'DayRoman',
            fontWeight: FontWeight.w300),
        textScaleFactor: 2,
      ),

      image: Image.asset("assets/images/log1.jpeg"),
      // loadingText: Text("Loading"),
      photoSize: 100.0,
      loaderColor: Colors.white,
    );
  }
}

class IndiqueAcceuil extends StatelessWidget {
  const IndiqueAcceuil({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: jwtOrEmpty,
        builder: (context, snapshot) {
          if (!snapshot.hasData)
            return Container(
              // height: size.height,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Center(
                    child: LoadingBouncingLine.circle(
                      borderColor: kPrimaryColor,
                      borderSize: 3.0,
                      size: 120.0,
                      backgroundColor: kPrimaryColor,
                      duration: Duration(milliseconds: 500),
                    ),
                  ),
                ],
              ),
            );

          if (snapshot.data != "") {
            var str = snapshot.data;
            var jwt = str.split(".");
            if (jwt.length != 3) {
              return Connexion();
            } else {
              var payload = json.decode(
                  ascii.decode(base64.decode(base64.normalize(jwt[1]))));
              if (DateTime.fromMillisecondsSinceEpoch(payload["exp"] * 1000)
                  .isAfter(DateTime.now())) {
                storage.delete(key: "idUser");
                storage.write(key: "idUser", value: payload['sub'].toString());
                return HomePage();
                // return test2(str, payload);
              } else {
                return Acceuil();
              }
            }
          } else {
            return Acceuil();
          }
        });
  }
}
